{{-- @extends('layouts.admin._master-admin') --}}
@extends('layouts.admin._settings')
@section('content')
<div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-semibold pt-2 mb-1">Table Permission</h4>

        <p class="mb-4">
            Beri izin pada setiap role yang terdaftar pada kolom dibawah ini!
        </p>

        <!-- Permission Table -->
        <div class="card">
            <div class="card-datatable table-responsive">
                <table class="datatables-permissions table">
                    <thead class="table-light">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Nama URL</th>
                            <th>Tanggal</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--/ Permission Table -->

        <!-- Modal -->
        <!-- Add Permission Modal -->
        <div class="modal fade" id="addPermissionModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content p-3 p-md-5">
                    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body p-md-0">
                        <div class="text-center mb-4">
                            <h3 class="mb-2 pb-1">Tambah Permission</h3>
                            <p>Permissions you may use and assign to your admin/staff.</p>
                        </div>
                        <div class="col-12 mb-3">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="name_permission" name="name" class="form-control"
                                placeholder="Name Persmission" />
                                <label>Name Permission</label>
                            </div>
                            <div class="form-floating form-floating-outline mt-3">
                                <textarea type="text" id="description" name="description" class="form-control" placeholder="description">
                                </textarea>
                                <label>Description</label>
                            </div>

                            <div class="form-floating form-floating-outline mt-3">
                                <input type="text" id="url_path" name="url_path" class="form-control"
                                placeholder="dashboard-admin" />
                                <label>URL Path</label>
                            </div>

                            <div class="form-floating form-floating-outline mt-3">
                                <input type="text" id="url_name" name="url_name" class="form-control"
                                placeholder="admin.dashboard" />
                                <label>URL Name</label>
                            </div>

                            <div class="form-floating form-floating-outline mt-3">
                                <input type="text" id="icon" name="icon" class="form-control"
                                placeholder="Icon" />
                                <label>Icon</label>
                            </div>

                            <div class="form-floating form-floating-outline mt-3">
                                <input type="number" id="order_number" name="order_number" class="form-control"
                                placeholder="order number" />
                                <label>Order Number</label>
                            </div>

                            <div class="form-floating form-floating-outline mt-3">
                                <select class="form-control" id="parent_id" name="parent_id">
                                    <option id="parentName" value=""></option>
                                    @foreach ($list_permission as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <label>Parent Name</label>
                            </div>

                            <div class="col-12 text-center demo-vertical-spacing">
                                <button type="submit" class="btn btn-primary me-sm-3 me-1" id="btn-submit">Simpan</button>
                                <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Add Permission Modal -->
            <!-- /Modal -->
        </div>
        <!-- / Content -->

        <div class="content-backdrop fade"></div>

        <div class="modal fade" id="editPermissionModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content p-3 p-md-5">
                    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body p-md-0">
                        <div class="text-center mb-4">
                            <h3 class="mb-2 pb-1">Update Permission</h3>
                            <p>Update Permission</p>
                        </div>
                        <div class="col-12 mb-3">
                            <form action="{{route('permission.update')}}" method="POST">
                                @csrf
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="name" name="name" class="form-control"placeholder="Nama Persmission" />
                                    <input type="hidden" id="permissionId" name="permission_id" class="form-control"/>
                                    <label>Nama Permission</label>
                                </div>
                                <div class="form-floating form-floating-outline mt-3">
                                    <textarea type="text" id="desc" name="desc" class="form-control">
                                    </textarea>
                                    <label>Description</label>
                                </div>

                                <div class="form-floating form-floating-outline mt-3">
                                    <input type="text" id="urlPath" name="url_path" class="form-control"/>
                                    <label>URL Path</label>
                                </div>

                                <div class="form-floating form-floating-outline mt-3">
                                    <input type="text" id="urlName" name="url_name" class="form-control"/>
                                    <label>URL Name</label>
                                </div>

                                <div class="form-floating form-floating-outline mt-3">
                                    <input type="text" id="icons" name="icon" class="form-control"/>
                                    <label>Icon</label>
                                </div>

                                <div class="form-floating form-floating-outline mt-3">
                                    <input type="number" id="orderNumber" name="order_number" class="form-control"/>
                                    <label>Order Number</label>
                                </div>

                                <div class="form-floating form-floating-outline mt-3">
                                    <select class="form-control" name="parent_id">
                                        <option id="parentName" value=""></option>
                                        @foreach ($list_permission as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    <label>Parent Name</label>
                                </div>

                                <div class="col-12 text-center demo-vertical-spacing">
                                    <button type="submit" class="btn btn-primary me-sm-3 me-1" id="btn-submit">Simpan</button>
                                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">Tutup</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @push('custom-scripts')
    <script src="{{ URL::asset('resources/js/admin/permission.js') }}"></script>

    <script>
        $("#btn-submit").click(function() {
            var namePermission = $("#name_permission").val();
            var description = $("#description").val();
            var url_name = $("#url_name").val();
            var urlPath = $("#url_path").val();
            var icon = $("#icon").val();
            var parentId = $("#parent_id").val();
            var orderNumber = $("#order_number").val();

            $.ajax({
                url: "{{ route('permission.store') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "name": namePermission,
                    "description": description,
                    "url_name": url_name,
                    "url_path": urlPath,
                    "icon": icon,
                    "parent_id": parentId,
                    "order_number": orderNumber
                },
                success: function(response) {
                    if (response.type == "success") {
                        $("#addPermissionModal").modal('hide');
                        Swal.fire({
                            type: 'success',
                            title: 'Tambah Permission Berhasil!',
                            showConfirmButton: true
                        });
                    } else {
                        $("#addPermissionModal").modal('hide');
                        Swal.fire({
                            type: 'info',
                            title: 'Tambah Permission Gagal!',
                            text: "Silahkan coba lagi!",
                            showCancelButton: true,
                        });
                    }
                },
                error: function(jqXhr, json, errorThrown) {
                    $("#addPermissionModal").modal('hide');
                    Swal.fire({
                        type: 'error',
                        title: 'Tambah Permission Gagal!',
                        text: "Silahkan coba lagi!",
                        showCancelButton: true,
                        showConfirmButton: false
                    });
                }
            });
        });
    </script>
    @endpush
    @endsection

