<script>
    
    $(function () {
        var dataTable = $(".datatables").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: `/perubahan`,
            },
            columns: [
            { data: "" },
            { data: "nomor_pencatatan", name: "nomor_pencatatan" },
            { data: "no_surat", name: "no_surat" },
            { data: "bentuk_serikat", name: "bentuk_serikat" },
            { data: "nama_serikat", name: "nama_serikat" },
            { data: "tgl_pencatatan", name: "tgl_pencatatan" },
            { data: "status_serikat", name: "status_serikat" },
            { data: "afiliasi", name: "afiliasi" },
            { data: "perubahan_jenis", name: "perubahan_jenis" },
            { data: "dokument_url", name: "dokument_url" },
            { data: "action", name: "action", orderable: false, searchable: false},
            ],
            columnDefs: [
            {
                className: "center",
                orderable: true,
                searchable: false,
                responsivePriority: 2,
                targets: 0,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },
            {
                className: "center",
                targets: 9,
                render: function (data, type, full, meta) {
                    if (full['dokument_url']) {
                        var res = '<a href="'+full['dokument_url']+'" target="_blank"><i class="mdi mdi mdi-download"></i></a>'
                    }else{
                        var res = '';
                    }
                    return res;
                },
            },
            ],
            order: [[0, "asc"]],
            dom:
            '<"row mx-1"' +
            '<"col-sm-12 col-md-3" l>' +
            '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
            ">t" +
            '<"row mx-2"' +
            '<"col-sm-12 col-md-6"i>' +
            '<"col-sm-12 col-md-6"p>' +
            ">",
            language: {
                sLengthMenu: "Show _MENU_",
                search: "Search",
                searchPlaceholder: "Search..",
            },
            // Buttons with Dropdown
            buttons: [],
        });
        
        // $("body").on("click", ".edit-kategori", function () {
            //     var id = $(this).data("id");
            
            //     $.ajax({
                //         url: `master-kategori-affiliasi/edit/${id}`,
                //         type: "GET",
                //         cache: false,
                //         success: function (response) {
                    //             $('#modalContent').html(response);
                    //             $('#editKategoriModal').modal('toggle')
                    //         },
                    //         error: function(jqXhr, json, errorThrown) {
                        //             $("#editKategoriModal").modal('hide');
                        //             console.log(errorThrown);
                        //         }
                        
                        //     });
                        // });
                        
                        // $("body").on("click", ".delete-record", function() {
                            //     const id = $(this).data("id");
                            //     Swal.fire({
                                //         title: 'Are you sure?',
                                //         text: "You won't be able to revert this!",
                                //         icon: 'warning',
                                //         showCancelButton: true,
                                //         confirmButtonColor: '#3085d6',
                                //         cancelButtonColor: '#d33',
                                //         confirmButtonText: 'Yes, delete it!',
                                //     }).then(({
                                    //         dismiss,
                                    //         value
                                    //     }) => {
                                        //         if (value !== undefined) {
                                            //             $.ajax({
                                                //                 url: `master-kategori-affiliasi/destroy/${id}`,
                                                //                 type: "GET",
                                                //                 success: function(response) {
                                                    //                     if (response.type == "success") {
                                                        //                         Swal.fire({
                                                            //                             type: 'success',
                                                            //                             title: 'Deleted Success',
                                                            //                             showConfirmButton: true,
                                                            //                             timer: 3000,
                                                            //                         }).then(() => location.reload())
                                                            
                                                            //                     } else {
                                                                //                         Swal.fire({
                                                                    //                             type: 'error',
                                                                    //                             title: 'Failed!',
                                                                    //                             text: "Deleted Not Success",
                                                                    //                             showCancelButton: true,
                                                                    //                             timer: 3000,
                                                                    //                         }).then(() => location.reload())
                                                                    //                     }
                                                                    //                 },
                                                                    //             });
                                                                    //         }
                                                                    //     })
                                                                    // });
                                                                    
                                                                });
                                                                
                                                            </script>