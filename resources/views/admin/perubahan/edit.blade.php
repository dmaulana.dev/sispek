<div class="modal-content p-3 p-md-5" id="create">
    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal" aria-label="Close"></button>
    <div class="modal-body p-md-0">
        <div class="text-center mb-4">
            <h3 class="mb-2 pb-1">Update Jenis Instalasi</h3>
        </div>
        <form method="POST">
            @csrf
            <div class="col-12 mb-3">
                <div class="form-floating form-floating-outline">
                    <input type="text" id="nama_instalasi" name="nama_instalasi" value="{{ $data->nama_instalasi }}"
                        class="form-control" placeholder="Jenis Instalasi" required />
                    <label>Jenis Instalasi</label>
                </div>

                <div class="form-floating form-floating-outline mt-3">
                    <input type="text" id="kode" name="kode" value="{{ $data->kode }}"
                        class="form-control" placeholder="Kode" required />
                    <label>Kode</label>
                </div>

                <div class="form-floating form-floating-outline mt-3">
                    <div class="form-check form-switch">
                        <input class="form-check-input" name="manual" type="checkbox" role="switch"
                            {{ $data->manual === 1 ? 'checked="true"' : '' }} id="statusChecked">
                        <label class="form-check-label" for="statusChecked">Status</label>
                    </div>
                </div>

                <div class="form-floating form-floating-outline mt-3">
                    <div class="form-check form-switch">
                        <input class="form-check-input" name="visible" type="checkbox" role="switch"
                            {{ $data->visible === 1 ? 'checked="true"' : '' }} id="statusChecked">
                        <label class="form-check-label" for="statusChecked">Status</label>
                    </div>
                </div>
                
                <div class="form-floating form-floating-outline mt-3">
                    <div class="form-check form-switch">
                        <input class="form-check-input" name="active" type="checkbox" role="switch"
                            {{ $data->active === 1 ? 'checked="true"' : '' }} id="statusChecked">
                        <label class="form-check-label" for="statusChecked">Status</label>
                    </div>
                </div>

                <div class="col-12 text-center demo-vertical-spacing">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1" id="btn-submit">Simpan</button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                        aria-label="Close">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('document #create').ready(function() {

        $('#instalasiModal form').on('submit', function(e) {

            e.preventDefault();
            const data = $(this).serialize();

            $.ajax({
                url: "{{ route('dashboard.master-data.instalasi.update', ['instalasi' => MainSett::encodeId($data->id)]) }}",
                type: "PUT",
                dataType: "JSON",
                data,
                success: function(response) {
                    if (response.type == "success") {
                        $("#instalasiModal").modal('hide');
                        Swal.fire({
                            type: 'success',
                            title: 'Update Jenis Instalasi Berhasil!',
                            showConfirmButton: true,
                            timer: 3000,
                        }).then(() => $('#dataTables').DataTable().ajax.reload());
                    } else {
                        $("#instalasiModal").modal('hide');
                        Swal.fire({
                            type: 'info',
                            title: 'Update Jenis Instalasi Gagal!',
                            text: "Silahkan coba lagi!",
                            showCancelButton: true,
                            timer: 3000,
                        }).then(() => $('#dataTables').DataTable().ajax.reload());
                    }
                },
                error: function(jqXhr, json, errorThrown) {
                    $("#instalasiModal").modal('hide');
                    Swal.fire({
                        type: 'error',
                        title: 'Update Jenis Instalasi Gagal!',
                        text: "Silahkan coba lagi!",
                        showCancelButton: true,
                        showConfirmButton: false,
                        timer: 3000,
                    });
                }
            });
        })
    })
</script>
