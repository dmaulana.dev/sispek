<!DOCTYPE html>

<html>
<title>Bukti Pencatatan SP/SB</title>
<head>
    
    @php
    $year = Carbon\Carbon::now()->format('Y');
    $date = Carbon\Carbon::now()->format('d');
    $dateM = Carbon\Carbon::now()->format('m');
    $days = Carbon\Carbon::now()->format('l');
    $month = Carbon\Carbon::now()->format('F');
    @endphp
    
    <style>
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        .table-collaps {
            padding: 10px;
            text-align: left;
            border: 1px solid black; 
            border-collapse: collapse;
        },
        
        ol {
            /* background: #ff9999; */
            padding: 0px;
        }
        
        ul {
            /* background: #3399ff; */
            padding: 0px;
        }
        
        ol li {
            padding: 3px;
            margin-left: 15px;
        }
        
        ul li {
            /* background: #cce5ff; */
            /* color: darkblue; */
            margin: 5px;
        }
    </style>
    
    <body style="width: 95%">
        <style>
            hr {
                border: none;
                height: 1px;
                color: #333; 
                background-color: #333;
            }
        </style>
        <p class="font-arial font12" style="text-align: right; width:80%">{{ Carbon\Carbon::parse($log->tgl_surat)->translatedFormat('d F Y')}}</p>
        <div style="width: 65%; float: left;">
            <table class="font-arial font12" style="width: 65%;">
                <tr>
                    <td>Nomor</td>
                    <td>:</td>
                    <td>{{ $log['no_surat'] }}</td>
                </tr>
                <tr>
                    <td>Sifat</td>
                    <td>:</td>
                    <td>Biasa</td>
                </tr>
                <tr>
                    <td>Lamp</td>
                    <td>:</td>
                    <td> - </td>
                </tr>
                <tr>
                    <td>Perihal</td>
                    <td>:</td>
                    <td>
                        Tanda Terima Laporan
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        Pergantian Pengurus
                    </td>
                </tr>
            </table>
        </div>
        <div class="font13 font-arial" style="width: 35%; float: right; text-align: center;">
            <table class="font-arial font12">
                <tr>
                    <td></td>
                    <td>Kepada</td>
                </tr>
                <tr>
                    <td>Yth.</td>
                    <td>Ketua Serikat Pekerja</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{$result['nama_serikat']}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        {{ $result['alamat']}}
                        <br/>
                        {{' Kel. '.$result['kelurahan'].' '. 'Kec. '.$result['kecamatan']}}
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>di-</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $result['wilayah'] }}</td>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div style="width: 10cm; float: left;"></div>
        <div class="font13 font-arial" style="width: 90%; float: right;">
            <p class="font12 font-arial" align="justify">
                <span style="padding-left: 30px;">
                    Sehubungan dengan surat Saudara Nomor {{ $log->no_surat_permohonan }} tanggal {{ Carbon\Carbon::parse($log->tgl_surat_permohonan)->translatedFormat('d F Y')}}
                    perihal Pemberitahuan dan Permohonan Pencatatan Perubahan Pengurus dan Anggaran Dasar 
                    serta Anggaran Rumah Tangga Serikat Karyawan (Sekawan) BTN, 
                    dengan ini kami sampaikan hal-hal sebagai berikut :
                </span>
            </p>
            <ol type="1" class="font12 font-arial" align="justify">
                <li>
                    Suku Dinas Tenaga Kerja, Transmigrasi dan Energi Kota Administrasi Jakarta Pusat 
                    telah menerima laporan pergantian pengurus {{ $result['nama_serikat'] }}.
                </li>
                <li>
                    Tanda terima ini bukan sebagai bukti pengesahan pergantian pengurus {{ $result['nama_serikat'] }}
                    karena pengurus dipilih 
                    oleh anggota serikat pekerja/serikat buruh yang mekanismenya diatur AD/ART 
                    serikat pekerja/serikat buruh
                </li>
            </ol>
            
            <p class="font-arial font12" align="justify">
                Demikian kami sampaikan untuk diketahui
                <br/>
            </p>
            <br/>
            
            <div style="width: 40%; float: right; text-align: center;">
                <p class="font12 font-arial">
                    {{ $result['pilih_ttd'] != 'Kepala' ? $result['pilih_ttd'].'.' : '' }} Kepala Suku Dinas Tenaga Kerja,
                    Transmigrasi dan Energi
                    Kota Administrasi Jakarta Pusat,                        
                </p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <p class="font12 font-arial">
                    {{$result['nama_ttd']}}
                    <br/>
                    NIP. {{$result['nip_ttd']}}
                </p>
            </div>
        </div>
        
        
    </body>
</head>
</html>
