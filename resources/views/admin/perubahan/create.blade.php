@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Perubahan Pencatatan</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('perubahan.index')}}">Perubahan Pencatatan</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="row invoice-add">
        <!-- surat tugas Add-->
        <div class="col-lg-12 col-12 mb-lg-0 mb-4">
            <div class="card invoice-preview-card">
                <div class="card-body">
                    <h6>Pilih Tipe Perubahan Sebelum Mengisi Form Perubahan</h6>
                    <br/>
                    <form id="formPerubahan" action="{{ route('perubahan.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input
                        class="form-control"
                        type="hidden"
                        value="{{ $data->id }}"
                        name="pencatatan_id"
                        tabindex="0"
                        id="pencatatan_id" />
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="tipe_perubahan" tabindex="0" id="tipePerubahan">
                                        <option>Pilih Tipe Perubahan</option>
                                        <option value="Nama dan Lambang">Perubahan Nama Dan Lambang</option>
                                        <option value="Pengurus SP/SB">Pengurus SP/SB</option>
                                    </select>
                                    <label for="tipePerubahan">Pilih Tipe Perubahan</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-tline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $data->nomor_pencatatan }}"
                                    name="nomor_pencatatan"
                                    tabindex="0"
                                    disabled
                                    id="nama" />
                                    <label for="Bentuk Serikat">Nomor Pencatatan</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $data->bentuk_serikat }}"
                                    name="bentuk_serikat"
                                    tabindex="0"
                                    disabled
                                    id="nama" />
                                    <label for="Bentuk Serikat">Bentuk Serikat</label>
                                </div>
                            </div>
                            
                            <div class="resNamaLambang">
                                
                            </div>
                            
                            <div class="resPengurus">
                                
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="No Surat"
                                    type="text"
                                    value=""
                                    name="no_surat"
                                    tabindex="0"
                                    id="noSurat" />
                                    <label for="namaSerikat">No Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="No Surat"
                                    type="date"
                                    value=""
                                    name="tgl_surat"
                                    tabindex="0"
                                    id="tglSurat" />
                                    <label for="tglSurat">Tgl Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="No Surat Permohonan"
                                    type="text"
                                    value=""
                                    name="no_surat_permohonan"
                                    tabindex="0"
                                    id="noSuratPermohonan" />
                                    <label for="noSuratPermohonan">No Surat Permohonan</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Tgl Surat Permohonan"
                                    type="date"
                                    value=""
                                    name="tgl_surat_permohonan"
                                    tabindex="0"
                                    id="tglSuratPermohonan" />
                                    <label for="tglSuratPermohonan">Tgl Surat Permohonan</label>
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Dok Surat Permohonan"
                                    type="file"
                                    value=""
                                    name="dok_surat_permohonan"
                                    tabindex="0"
                                    id="dokSuratPermohonan" />
                                    <label for="dokSuratPermohonan">Dok Surat Permohonan</label>
                                </div>
                            </div>
                            
                            <div class="col-sm-2 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="pilih_ttd" tabindex="0" id="pilihKepala">
                                        <option {{ $data->pilih_ttd == 'Kepala' ? 'selected' : '' }} value="Kepala">Kepala</option>
                                        <option {{ $data->pilih_ttd == 'PLH' ? 'selected' : '' }} value="PLH">PLH</option>
                                        <option {{ $data->pilih_ttd == 'PLT' ? 'selected' : '' }} value="PLT">PLT</option>
                                    </select>
                                    <label for="pilihKepala">Pilih Kepala</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $data->nama_ttd }}"
                                    name="nama_ttd"
                                    tabindex="0"
                                    id="tglPencatatan" />
                                    <label for="tglPencatatan">Nama TTD</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="number"
                                    value="{{ $data['nip_ttd'] }}"
                                    name="nip_ttd"
                                    tabindex="0"
                                    id="logo" />
                                    <label for="logo">NIP TTD</label>
                                </div>
                            </div>
                            
                            
                            
                            <div class="col-sm-6 mt-2">
                                <button type="sumbit" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;  Simpan </button>
                                <button type="button" class="btn btn-danger"><i class="fa fa-minus-circle"></i>&nbsp;  Cancel </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="tambahPengurus" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Pengurus Baru</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form id="submit" method="POST">
                        <div class="modal-body">
                            <input
                            class="form-control"
                            type="hidden"
                            tabindex="0"
                            name="pencatatan_id"
                            id="pencatatan_id"
                            value="{{$data->id}}" />
                            <div class="col-sm-12">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    tabindex="0"
                                    name="nama_baru"
                                    id="nama_baru" />
                                    <label for="nama">Nama</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    tabindex="0"
                                    name="jabatan_baru"
                                    id="jabatan_baru" />
                                    <label for="Jabatan">Jabatan</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
    
    @endsection
    
    @push('custom-scripts')
    
    <script>
        $("#submit").submit(function(e) {
            e.preventDefault();
            var namaBaru = $("#nama_baru").val();
            var jabatanBaru = $("#jabatan_baru").val();
            var pencatatanId = $("#pencatatan_id").val();
            Swal.fire({
                title: 'loading !',
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false,
                // html: `<div><i class="animate-spin fas fa-spinner"></i></div>`,
                didOpen: () => {
                    Swal.showLoading()
                },
            });
            
            
            $.ajax({
                url: "{{ route('simpan-pengurus') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "nama_baru": namaBaru,
                    "jabatan_baru": jabatanBaru,
                    'pencatatan_id' : pencatatanId
                },
                success: function(response) {
                    if (response.type == "success") {
                        $('#tambahPengurus').modal('toggle');
                        Swal.fire({
                            type: 'success',
                            title: 'data berhasil',
                            text: '...',
                            timer: 3000,
                            showCancelButton: false,
                            showConfirmButton: false
                        }).then (function() {
                            $(".resPengurus").load(window.location.href + " .resPengurus" );
                        });
                    } else {
                        // Swal.fire({
                            //     type: 'info',
                            //     title: 'Username atau password salah!',
                            //     text: "Please check your email or password!",
                            //     showCancelButton: false,
                            //     showConfirmButton: false
                            // }).then (function() {
                                //     window.location.href = "{{ route('auth.login') }}";
                                // });
                            }
                        },
                        error: function(jqXhr, json, errorThrown) {
                            // Swal.fire({
                                //     type: 'error',
                                //     title: 'Login Gagal!',
                                //     text: "Please check your email or password!",
                                //     showCancelButton: false,
                                //     showConfirmButton: false
                                // }).then (function() {
                                    //     window.location.href = "{{ route('auth.login') }}";
                                    // });
                                }
                            });
                        });
                    </script>
                    
                    <script>
                        
                        $(document).ready(function() {
                            var max_fields = 10;
                            var $add_input_button = $('.add_input_button');
                            var $field_wrapper = $('.field_wrapper');
                            var new_field_html = `<div>
                                <div class="col-sm-6 mt-2">
                                    <div class="form-floating form-floating-outline mb-3">
                                        <input
                                        class="form-control"
                                        type="text"
                                        value=""
                                        name="perusahaan[]"
                                        tabindex="0"
                                        id="perusahaan" />
                                        <button style="float: right padding:20px" type="button" class="btn btn-danger remove_input_button"><i class="fa fa-minus-circle"></i></button>
                                    </div>
                                </div>
                            </div>`;
                            var input_count = 1;
                            
                            //add inputs
                            $add_input_button.click(function() {
                                if (input_count < max_fields) {
                                    input_count++;
                                    $field_wrapper.append(new_field_html);
                                }
                            });
                            
                            //remove_input
                            $field_wrapper.on('click', '.remove_input_button', function(e) {
                                e.preventDefault();
                                $(this).parent('div').remove();
                                input_count--;
                            });
                        });
                        
                    </script>
                    
                    
                    <script>
                        $(document).ready(function(){
                            
                            var count = 1;
                            
                            dynamic_field(count);
                            
                            function dynamic_field(number)
                            {
                                html = '<tr>';
                                    html += `<td>
                                        <div class="form-floating form-floating-outline mb-3">
                                            <input
                                            class="form-control"
                                            type="text"
                                            value=""
                                            name="nama[]"
                                            tabindex="0"
                                            id="nama" />
                                            <label for="nama">Nama</label>
                                        </div>
                                    </td>`;
                                    html += `<td>
                                        <div class="form-floating form-floating-outline mb-3">
                                            <input
                                            class="form-control"
                                            type="text"
                                            value=""
                                            name="jabatan[]"
                                            tabindex="0"
                                            id="jabatan" />
                                            <label for="jabatan">Jabatan</label>
                                        </div>
                                    </td>`;
                                    if(number > 1)
                                    {
                                        html += '<td><button id="remove" type="button" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></tr>';
                                            $('tbody').append(html);
                                        }
                                        else
                                        {   
                                            html += '<td><button id="add" type="button" class="btn btn-success"><i class="fa fa-plus-circle"></i></button></td></tr>';
                                            $('tbody').html(html);
                                        }
                                    }
                                    
                                    $(document).on('click', '#add', function(){
                                        count++;
                                        dynamic_field(count);
                                    });
                                    
                                    $(document).on('click', '#remove', function(){
                                        count--;
                                        $(this).closest("tr").remove();
                                    }); 
                                    
                                    
                                });
                            </script>
                            
                            <script>
                                $('#tipePerubahan').on('change', function (e) {
                                    var optionSelected = $("option:selected", this);
                                    var valueSelected = this.value;
                                    
                                    let _this = this;
                                    if (valueSelected == 'Nama dan Lambang') {
                                        console.log('lambang')
                                        // $('.resPengurus').hide();
                                        $('.resNamaLambang').html(
                                        `<div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-floating form-floating-outline mb-3">
                                                    <input
                                                    class="form-control"
                                                    placeholder="nama panjang..."
                                                    type="text"
                                                    value="{{ $data->nama_serikat }}"
                                                    name="nama_serikat"
                                                    tabindex="0"
                                                    id="namaSerikat" />
                                                    <label for="namaSerikat">Nama Panjang</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-floating form-floating-outline mb-3">
                                                    <input
                                                    class="form-control"
                                                    placeholder="nama singkat..."
                                                    type="text"
                                                    value="{{ $data->nama_singkat }}"
                                                    name="nama_singkatan"
                                                    tabindex="0"
                                                    id="namaSingkat" />
                                                    <label for="namaSingkat">Nama Singkatan</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-floating form-floating-outline mb-3">
                                                    <input
                                                    class="form-control"
                                                    placeholder="Lambang..."
                                                    type="file"
                                                    value=""
                                                    name="logo"
                                                    tabindex="0"
                                                    id="lambang" />
                                                    <label for="namaSerikat">Logo / Lambang</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-floating form-floating-outline mt-3">
                                                    <a href="{{ $data->logo_url }}" target="_blank">Click Lihat Logo Sebelumnya</a>
                                                </div>
                                            </div>
                                        </div>`
                                        );
                                    }
                                    else{
                                        console.log('pengurus')
                                        $('.resNamaLambang').hide();
                                        $('.resPengurus').html(`
                                        <div class="row col-sm-12">
                                            <div class="row mt-5 mb-5">
                                                <div class="col-md-6">
                                                    <button type="button" data-bs-toggle="modal" data-bs-target="#tambahPengurus" class="btn btn-sm btn-primary">Tambah Pengurus</button>
                                                </div>
                                            </div>
                                            @if (!empty($susunan))
                                            @foreach ($susunan as $item)
                                            <div class="col-sm-5">
                                                <div class="form-floating form-floating-outline mb-3">
                                                    <input
                                                    class="form-control"
                                                    type="text"
                                                    value="{{ $item->nama }}"
                                                    tabindex="0"
                                                    name="nama[]"
                                                    id="nama" />
                                                    <label for="nama">Nama</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-floating form-floating-outline mb-3">
                                                    <input
                                                    class="form-control"
                                                    type="text"
                                                    value="{{ $item->jabatan }}"
                                                    tabindex="0"
                                                    name="jabatan[]"
                                                    id="Jabatan" />
                                                    <label for="Jabatan">Jabatan</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-floating form-floating-outline mb-3">
                                                    <button type="button" class="btn btn-danger delete-record-pengurus" data-id="{{$item->id}}"><i class="fa fa-minus-circle"></i></button>
                                                </div>
                                            </div>
                                            @endforeach
                                            @else
                                            @endif
                                        </div>
                                        `);
                                    }
                                });
                            </script>
                            
                            <script>
                                $("body").on("click", ".delete-record-pengurus", function() {
                                    const id = $(this).data("id");
                                    Swal.fire({
                                        title: 'Are you sure?',
                                        text: "You won't be able to revert this!",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Yes, delete it!',
                                    }).then(({
                                        dismiss,
                                        value
                                    }) => {
                                        if (value !== undefined) {
                                            $.ajax({
                                                url: `/hapus-pengurus/${id}`,
                                                type: "GET",
                                                success: function(response) {
                                                    if (response.type == "success") {
                                                        Swal.fire({
                                                            type: 'success',
                                                            title: 'Deleted Success',
                                                            showConfirmButton: true,
                                                            timer: 3000,
                                                        }).then(() => $(".resPengurus").load(" .resPengurus"))
                                                        
                                                    } else {
                                                        Swal.fire({
                                                            type: 'error',
                                                            title: 'Failed!',
                                                            text: "Deleted Not Success",
                                                            showCancelButton: true,
                                                            timer: 3000,
                                                        }).then(() => $("#divid").load(" #divid"))
                                                    }
                                                },
                                            });
                                        }
                                    })
                                });
                            </script>
                            
                            @endpush