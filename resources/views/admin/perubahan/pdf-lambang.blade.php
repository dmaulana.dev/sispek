<!DOCTYPE html>

<html>
<title>Bukti Pencatatan SP/SB</title>
<head>
    
    @php
    $year = Carbon\Carbon::now()->format('Y');
    $date = Carbon\Carbon::now()->format('d');
    $dateM = Carbon\Carbon::now()->format('m');
    $days = Carbon\Carbon::now()->format('l');
    $month = Carbon\Carbon::now()->format('F');
    @endphp
    
    <style>
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        .table-collaps {
            padding: 10px;
            text-align: left;
            border: 1px solid black; 
            border-collapse: collapse;
        },
        
        ol {
            /* background: #ff9999; */
            padding: 0px;
        }
        
        ul {
            /* background: #3399ff; */
            padding: 0px;
        }
        
        ol li {
            padding: 3px;
            margin-left: 15px;
        }
        
        ul li {
            /* background: #cce5ff; */
            /* color: darkblue; */
            margin: 5px;
        }
    </style>
    
    <body style="width: 95%">
        <style>
            hr {
                border: none;
                height: 1px;
                color: #333; 
                background-color: #333;
            }
        </style>
        <p class="font-arial font12" style="text-align: right; width:80%">{{ Carbon\Carbon::parse($log['tgl_surat'])->translatedFormat('d F Y')}}</p>
        <div style="width: 65%; float: left;">
            <table class="font-arial font12" style="width: 65%;">
                <tr>
                    <td>Nomor</td>
                    <td>:</td>
                    <td>{{ $log['no_surat'] }}</td>
                </tr>
                <tr>
                    <td>Sifat</td>
                    <td>:</td>
                    <td>Biasa</td>
                </tr>
                <tr>
                    <td>Lamp</td>
                    <td>:</td>
                    <td> - </td>
                </tr>
                <tr>
                    <td>Perihal</td>
                    <td>:</td>
                    <td>
                        @if ($log->perubahan_jenis == 'Nama dan Lambang')
                        Perubahan Nama SP/FSB
                        @else
                        Bukti Perubahan Lambang 
                        SP/SB                            
                        @endif 
                    </td>
                </tr>
            </table>
        </div>
        <div class="font13 font-arial" style="width: 35%; float: right; text-align: center;">
            <table class="font-arial font12">
                <tr>
                    <td></td>
                    <td>Kepada</td>
                </tr>
                <tr>
                    <td>Yth.</td>
                    <td>Ketua Serikat Pekerja</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{$result['nama_serikat']}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        {{ $result['alamat']}}
                        <br/>
                        {{' Kel. '.$result['kelurahan'].' '. 'Kec. '.$result['kecamatan']}}
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>di-</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $result['wilayah'] }}</td>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div style="width: 10cm; float: left;"></div>
        <div class="font13 font-arial" style="width: 90%; float: right;">
            <p class="font12 font-arial" align="justify">
                <span style="padding-left: 30px;">
                    Sehubungan dengan surat Saudara Nomor {{ $log->no_surat_permohonan }} 
                    tanggal {{ Carbon\Carbon::parse($log->tgl_surat_permohonan)->translatedFormat('d F Y')}} perihal Surat pemberitahuan 
                    {{ $log->tipe_perubahan }} {{ $result['nama_serikat'] }}, 
                    mengingat pemberitahuan tersebut telah memenuhi syarat ketentuan Pasal 19 ayat (1) Peraturan Guberur Provinsi DKI Jakarta Nomor 10 Tahun 2007, maka sesuai ketentuan Pasal 20 Peraturan Gubernur Provinsi DKI Jakarta Nomor 10 Tahun 2007, bersama ini kami sampaikan bahwa perubahan nama tersebut telah kami catat sebagai berikut :
                </span>
            </p>
            <ol type="I" class="font12 font-arial" align="justify">
                <li>Identitas</li>
                <ol type="a" style="padding: 0px 0px 0px 20px;">
                    <li>Nama Dan Lambang yang Lama</li>
                    <dl style="padding: 5px 0px 0px 20px;">
                        <dt>- Nama SP/SB    : {{ $log['nama_serikat'] }}</dt>
                        <dt>- Lambang SP/SB : Terlampir</dt>
                    </dl>
                    <li style="padding-top: 10px;">Nama Dan Lambang yang Baru</li>
                    <dl style="padding: 5px 0px 0px 20px;">
                        <dt>- Nama    : {{ $result['nama_serikat'] }}</dt>
                        <dt>- Lambang SP/SB : Terlampir</dt>
                    </dl>
                </ol>
                <li style="padding-top: 20px;">
                    Dengan perubahan lambang sebagaimana angka I diatas, maka tidak merubah dan merupakan bagian 
                    tidak terpisahkan dengan
                    nomor bukti pencatatan federasi serikat pekerja/serikat buruh yang telah dikeluarkan sebelumnya, yakni
                    <ol type="a">
                        <li>
                            Nomor dan tanggal surat Kepala Suku Dinas Tenaga Kerja dan Transmigrasi Kota Administrasi Jakart Pusat
                        </li>
                        <li>Nomor dan taggal bukti pencatatan 
                            {{ $result['nomor_pencatatan'] }} tanggal {{ Carbon\Carbon::parse($result['tgl_pencatatan'])->translatedFormat('d F Y')}}.
                        </li>
                    </ol>
                </li>
            </ol>
            
            <p class="font-arial font12" align="justify">
                Demikian agar Saudara maklum.
                <br/>
            </p>
            <br/>
            
            <div style="width: 40%; float: right; text-align: center;">
                <p class="font12 font-arial">
                    {{ $result['pilih_ttd'] != 'Kepala' ? $result['pilih_ttd'].'.' : '' }} Kepala Suku Dinas Tenaga Kerja,
                    Transmigrasi dan Energi
                    Kota Administrasi Jakarta Pusat,                        
                </p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <p class="font12 font-arial">
                    {{$result['nama_ttd']}}
                    <br/>
                    NIP. {{$result['nip_ttd']}}
                </p>
            </div>
        </div>
        
        
    </body>
</head>
</html>
