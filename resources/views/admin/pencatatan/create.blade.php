@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Input Pencatatan SPSB</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pencatatan.index')}}">Pencatatan SPSB</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="row invoice-add">
        <!-- surat tugas Add-->
        <div class="col-lg-12 col-12 mb-lg-0 mb-4">
            <div class="card invoice-preview-card">
                <div class="card-body">
                    <h6>Lengkapi Form dibawah ini untuk input penctatan</h6>
                    <br/>
                    <form action="{{ route('pencatatan.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="bentuk_serikat" tabindex="0" id="bentukSerikat">
                                        <option></option>
                                        @foreach ($kategori as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                    <label for="Bentuk Serikat">Bentuk Serikat</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="jenis_serikat" tabindex="0" id="JenisSerikat">
                                        <option value="Serikat di Perusahaan">Serikat di Perusahaan</option>
                                        <option value="Serikat di luar Perusahaan">Serikat di luar Perusahaan</option>
                                    </select>
                                    <label for="Jenis Serikat">Jenis Serikat</label>
                                </div>
                            </div>
                            <div class="col-sm-12 selectOpt">
                                <div class="form-floating form-floating-outline mb-3">
                                    {{-- <select class="form-select nama_serikat" name="nama_serikat" tabindex="0" id="namaSerikat">
                                        
                                    </select> --}}
                                    <input
                                    class="form-control"
                                    placeholder="nama panjang..."
                                    type="text"
                                    value=""
                                    name="nama_panjang"
                                    tabindex="0"
                                    id="namaSerikat" />
                                    <label for="namaSerikat">Nama Panjang SP/SB</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="nama singkatan..."
                                    type="text"
                                    value=""
                                    name="nama_singkatan"
                                    tabindex="0"
                                    id="namaSingkatan" />
                                    <label for="namaSingkatan">Nama Singkatan SP/SB</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="status_serikat" tabindex="0" id="statusSerikat">
                                        <option>Pilih Afiliasi</option>
                                        <option value="Mandiri">Mandiri</option>
                                        <option value="Afiliasi">Afiliasi</option>
                                    </select>
                                    <label for="statusSerikat">Status Serikat</label>
                                </div>
                            </div>
                            <div class="col-sm-12 namaAfiliasi">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select targetAfiliasi" name="afiliasi" tabindex="0" id="afiliasi">
                                        
                                    </select>
                                    <label for="afiliasi">Nama Afiliasi</label>
                                </div>
                            </div>
                            <div class="row field_wrapper">
                                <div class="form-floating form-floating-outline mb-3 col-md-11 col-sm-11 col-11">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value=""
                                    name="perusahaan[]"
                                    tabindex="0"
                                    id="perusahaan" />
                                    <label for="perusahaan">Perusahaan</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-3 col-md-1 col-sm-1 col-1">
                                    <button type="button" class="btn btn-success add_input_button"><i class="fa fa-plus-circle"></i></button>
                                </div>
                            </div>
                            {{-- <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="DKI Jakarta"
                                    tabindex="0"
                                    id="provinsi" />
                                    <label for="provinsi">Provinsi</label>
                                </div>
                            </div> --}}
                            <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" tabindex="0" name="wilayah" id="kotaAdm">
                                        <option></option>
                                        @foreach ($wilayah as $row)
                                        <option value="{{$row['kabupaten']}}">{{$row['kabupaten']}}</option>
                                        @endforeach
                                    </select>
                                    <label for="kotaAdm">Kota Administrasi</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" tabindex="0" name="kecamatan" id="kecamatan">
                                        
                                    </select>
                                    <label for="kecamatan">Kecamatan</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" tabindex="0" name="kelurahan" id="kelurahan">
                                        
                                    </select>
                                    <label for="kelurahan">Kelurahan</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-floating form-floating-outline mb-3">
                                    <textarea class="form-control" rows="5" name="alamat"></textarea>
                                    <label for="alamat">Alamat Lengkap</label>
                                </div>
                            </div>
                            <p>Struktur Kepengurusan</p>
                            <table id="item_table" class="table">
                                <tbody>
                                    <tr class="border-bottom-primary">
                                        <td>
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="text"
                                                value=""
                                                name="nama[]"
                                                tabindex="0"
                                                id="nama" />
                                                <label for="nama">Nama</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="text"
                                                value=""
                                                name="jabatan[]"
                                                tabindex="0"
                                                id="jabatan" />
                                                <label for="jabatan">Jabatan</label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            {{-- <div class="col-sm-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <button id="add" type="button" class="btn btn-success"><i class="fa fa-plus-circle"></i></button>
                                    <button id="remove" type="button" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                </div>
                            </div> --}}
                            <div class="col-sm-6 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="date"
                                    value=""
                                    name="tgl_pencatatan"
                                    tabindex="0"
                                    id="tglPencatatan" />
                                    <label for="tglPencatatan">Tanggal Pencatatan</label>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="file"
                                    value=""
                                    name="logo_image"
                                    tabindex="0"
                                    id="logo" />
                                    <label for="logo">Upload Logo</label>
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value=""
                                    name="no_surat"
                                    tabindex="0"
                                    id="noSurat" />
                                    <label for="tglPencatatan">No Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-2 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="date"
                                    name="tgl_surat"
                                    tabindex="0"
                                    id="tglSurat" />
                                    <label for="tglSurat">Tanggal Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-7 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value=""
                                    name="judul_surat"
                                    tabindex="0"
                                    id="judulSurat" />
                                    <label for="judulSurat">Judul Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value=""
                                    name="no_surat_permohonan"
                                    tabindex="0"
                                    id="noPermohonan" />
                                    <label for="noPermohonan">No Permohonan</label>
                                </div>
                            </div>
                            <div class="col-sm-4 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="date"
                                    name="tgl_surat_permohonan"
                                    tabindex="0"
                                    id="tglPermohonan" />
                                    <label for="tglPermohonan">Tanggal Permohonan</label>
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="file"
                                    value=""
                                    name="permohonan_berkas_url"
                                    tabindex="0"
                                    id="uploadSurat" />
                                    <label for="judulSurat">Upload Surat Permohonan</label>
                                </div>
                            </div>
                            <div class="col-sm-2 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="pilih_ttd" tabindex="0" id="pilihKepala">
                                        <option>Pilih TTD</option>
                                        <option value="Kepala">Kepala</option>
                                        <option value="PLH">PLH</option>
                                        <option value="PLT">PLT</option>
                                    </select>
                                    <label for="pilihKepala">Pilih Kepala</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value=""
                                    name="nama_ttd"
                                    tabindex="0"
                                    id="tglPencatatan" />
                                    <label for="tglPencatatan">Nama TTD</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="number"
                                    value=""
                                    name="nip_ttd"
                                    tabindex="0"
                                    id="logo" />
                                    <label for="logo">NIP TTD</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 mt-2">
                            <button type="sumbit" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;  Simpan </button>
                            <button type="button" onclick="history.back()" class="btn btn-danger"><i class="fa fa-minus-circle"></i>&nbsp;  Cancel </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('custom-scripts')

<script>
    
    $(document).ready(function() {
        var max_fields = 10;
        var $add_input_button = $('.add_input_button');
        var $field_wrapper = $('.field_wrapper');
        var new_field_html = `<div>
            <div class="col-sm-6 mt-2">
                <div class="form-floating form-floating-outline mb-3">
                    <input
                    class="form-control"
                    type="text"
                    value=""
                    name="perusahaan[]"
                    tabindex="0"
                    id="perusahaan" />
                    <button style="float: right padding:20px" type="button" class="btn btn-danger remove_input_button"><i class="fa fa-minus-circle"></i></button>
                </div>
            </div>
        </div>`;
        var input_count = 1;
        
        //add inputs
        $add_input_button.click(function() {
            if (input_count < max_fields) {
                input_count++;
                $field_wrapper.append(new_field_html);
            }
        });
        
        //remove_input
        $field_wrapper.on('click', '.remove_input_button', function(e) {
            e.preventDefault();
            $(this).parent('div').remove();
            input_count--;
        });
    });
    
</script>


<script>
    $(document).ready(function(){
        
        var count = 1;
        
        dynamic_field(count);
        
        function dynamic_field(number)
        {
            html = '<tr>';
                html += `<td>
                    <div class="form-floating form-floating-outline mb-3">
                        <input
                        class="form-control"
                        type="text"
                        value=""
                        name="nama[]"
                        tabindex="0"
                        id="nama" />
                        <label for="nama">Nama</label>
                    </div>
                </td>`;
                html += `<td>
                    <div class="form-floating form-floating-outline mb-3">
                        <input
                        class="form-control"
                        type="text"
                        value=""
                        name="jabatan[]"
                        tabindex="0"
                        id="jabatan" />
                        <label for="jabatan">Jabatan</label>
                    </div>
                </td>`;
                if(number > 1)
                {
                    html += '<td><button id="remove" type="button" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></tr>';
                        $('tbody').append(html);
                    }
                    else
                    {   
                        html += '<td><button id="add" type="button" class="btn btn-success"><i class="fa fa-plus-circle"></i></button></td></tr>';
                        $('tbody').html(html);
                    }
                }
                
                $(document).on('click', '#add', function(){
                    count++;
                    dynamic_field(count);
                });
                
                $(document).on('click', '#remove', function(){
                    count--;
                    $(this).closest("tr").remove();
                }); 
                
                
            });
        </script>
        
        <script>
            $('#statusSerikat').on('change', function (e) {
                var optionSelected = $("option:selected", this);
                var valueSelected = this.value;
                
                let _this = this;
                if (valueSelected == 'Afiliasi') {
                    $('.namaAfiliasi').show();
                    $.ajax({
                        url: `{{ route('master-kategori-affiliasi.get-data') }}`,
                        type: "GET",
                        data : {
                            'ids' : valueSelected
                        },
                        cache: false,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            $('.targetAfiliasi').html(response.option);
                            // let data = response.data
                            // const Item = $(_this).closest('select').find('.targetAfiliasi').html();
                            // data.map(d => {
                                //     $(Item).append(`<option value="${d.nama}">${d.nama}</option>`)
                                
                                // });
                            },
                            error: function(jqXhr, json, errorThrown) {
                                console.log(errorThrown);
                            }
                            
                        });
                    }
                    else{
                        $('.namaAfiliasi').hide();
                    }
                });
            </script>
            <script>
                $('#kotaAdm').on('change', function (e) {
                    var optionSelected = $("option:selected", this);
                    var valueSelected = this.value;
                    
                    let _this = this;
                    $.ajax({
                        url: `{{ route('wilayah.kabupaten') }}`,
                        type: "GET",
                        data : {
                            'wilayah' : valueSelected
                        },
                        cache: false,
                        success: function (response) {
                            response.data.map(d => {
                                $('#kecamatan').append(`<option value="${d.kecamatan}">${d.kecamatan}</option>`)
                            });
                        },
                        error: function(jqXhr, json, errorThrown) {
                            console.log(errorThrown);
                        }
                        
                    });
                    
                });
                
                $('#kecamatan').on('change', function (e) {
                    var optionSelected = $("option:selected", this);
                    var valueSelected = this.value;
                    
                    let _this = this;
                    $.ajax({
                        url: `{{ route('wilayah.kecamatan') }}`,
                        type: "GET",
                        data : {
                            'kecamatan' : valueSelected
                        },
                        cache: false,
                        success: function (response) {
                            response.data.map(d => {
                                $('#kelurahan').append(`<option value="${d.kelurahan}">${d.kelurahan}</option>`)
                            });
                        },
                        error: function(jqXhr, json, errorThrown) {
                            console.log(errorThrown);
                        }
                        
                    });
                    
                });
            </script>
            
            @endpush