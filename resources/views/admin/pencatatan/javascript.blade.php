<script>
    
    $(function () {
        var dataTable = $(".datatables").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: `/pencatatan`,
            },
            columns: [
            { data: "" },
            { data: "nomor_pencatatan", name: "nomor_pencatatan" },
            { data: "bentuk_serikat", name: "bentuk_serikat" },
            { data: "nama_serikat", name: "nama_serikat" },
            { data: "tgl_pencatatan", name: "tgl_pencatatan" },
            { data: "status_serikat", name: "status_serikat" },
            { data: "afiliasi", name: "afiliasi" },
            { data: "status", name: "status" },
            { data: "visible", name: "visible" },
            { data: "dokument_url", name: "dokument_url" },
            { data: "action", name: "action", orderable: false, searchable: false},
            ],
            columnDefs: [
            {
                className: "center",
                orderable: true,
                searchable: false,
                responsivePriority: 2,
                targets: 0,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },
            {
                // Label
                targets: 7,
                render: function (data, type, full, meta) {
                    var $status_number = full['status'];
                    var $status = {
                        0: { title: 'Proses', class: ' bg-label-warning' },
                        1: { title: 'Selesai', class: ' bg-label-primary' },
                        2: { title: 'Tolak', class: ' bg-label-danger' },
                    };
                    
                    return (
                    '<span class="badge rounded-pill ' +
                    $status[$status_number].class +
                    '">' +
                    $status[$status_number].title +
                    '</span>'
                    );
                }
            },
            {
                // Label
                targets: 8,
                render: function (data, type, full, meta) {
                    var $status_number = full['visible'];
                    var $status = {
                        0: { title: 'Non Aktif', class: ' bg-label-danger' },
                        1: { title: 'Aktif', class: ' bg-label-success' },
                    };
                    
                    return (
                    '<span class="badge rounded-pill ' +
                    $status[$status_number].class +
                    '">' +
                    $status[$status_number].title +
                    '</span>'
                    );
                }
            },
            {
                className: "center",
                targets: 9,
                render: function (data, type, full, meta) {
                    if (full['dokument_url']) {
                        var res = '<a href="'+full['dokument_url']+'" target="_blank"><i class="mdi mdi mdi-download"></i></a>'
                    }else{
                        var res = '';
                    }
                    return res;
                },
            },
            ],
            order: [[0, "asc"]],
            dom:
            '<"row mx-1"' +
            '<"col-sm-12 col-md-3" l>' +
            '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
            ">t" +
            '<"row mx-2"' +
            '<"col-sm-12 col-md-6"i>' +
            '<"col-sm-12 col-md-6"p>' +
            ">",
            language: {
                sLengthMenu: "Show _MENU_",
                search: "Search",
                searchPlaceholder: "Search..",
            },
            // Buttons with Dropdown
            buttons: [],
        });
        
        $("body").on("click", ".edit-kategori", function () {
            var id = $(this).data("id");
            
            $.ajax({
                url: `/pencatatan/edit/${id}`,
                type: "GET",
                cache: false,
                success: function (response) {
                    $('#modalContent').html(response);
                    $('#editKategoriModal').modal('toggle')
                },
                error: function(jqXhr, json, errorThrown) {
                    $("#editKategoriModal").modal('hide');
                    // console.log(errorThrown);
                }
                
            });
        });
        
        $("body").on("click", ".delete-record", function() {
            const id = $(this).data("id");
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
            }).then(({
                dismiss,
                value
            }) => {
                if (value !== undefined) {
                    $.ajax({
                        url: `/pencatatan/destroy/${id}`,
                        type: "GET",
                        success: function(response) {
                            if (response.type == "success") {
                                Swal.fire({
                                    type: 'success',
                                    title: 'Deleted Success',
                                    showConfirmButton: true,
                                    timer: 3000,
                                }).then(() => location.reload())
                                
                            } else {
                                Swal.fire({
                                    type: 'error',
                                    title: 'Failed!',
                                    text: "Deleted Not Success",
                                    showCancelButton: true,
                                    timer: 3000,
                                }).then(() => location.reload())
                            }
                        },
                    });
                }
            })
        });
        
    });
    
</script>