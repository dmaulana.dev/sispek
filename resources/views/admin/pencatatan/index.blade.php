@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Pencatatan SPSB</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pencatatan.index')}}">Data Pencatatan SPSB</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8">
                    <h5 class="card-title">Data Pencatatan</h5>
                </div>
                <div class="col-md-2">
                    <a style="float:right" class="btn-sm btn btn-dark" data-bs-toggle="modal", data-bs-target="#addImportPencatatanModal">
                        <i class="mdi mdi-file-import me-0 me-sm-1" style="color: white"></i>
                        <span class="d-none d-sm-inline-block" style="color: white">IMPORT</span>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('pencatatan.create') }}" class="btn-sm btn btn-primary add-surat-tugas">
                        <i class="mdi mdi-plus me-0 me-sm-1"></i>
                        <span class="d-none d-sm-inline-block">PENCATATAN</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card-datatable table-responsive">
            <table class="datatables table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No Pencatatan</th>
                        <th>Bentuk Serikat</th>
                        <th>Nama Serikat</th>
                        <th>Tgl Pencatatan</th>
                        <th>Status SPSB</th>
                        <th>Afiliasi</th>
                        <th>Status</th>
                        <th>Visible</th>
                        <th>Dok</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    
    <div class="modal-onboarding modal fade animate__animated" id="addImportPencatatanModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content p-3 p-md-5">
                <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
                aria-label="Close"></button>
                <div class="modal-body p-md-0">
                    <div class="text-center mb-4">
                        <h3 class="mb-2 pb-1">Import Data</h3>
                        <p>Silahkan Download Template dulu untuk import data <a href="{{ route('pencatatan.template') }}" style="color: blue"><b>Download Template</b></a></p>
                    </div>
                    <form action="{{route('pencatatan.import')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-12 mb-3">
                            <div class="form-floating form-floating-outline">
                                <input type="file" id="import" name="import"
                                class="form-control" placeholder="Import Pencatatan"/>
                                <label>Import Pencatatan</label>
                            </div>
                        </div>
                        
                        <div class="col-12 text-center demo-vertical-spacing">
                            <button type="submit" class="btn btn-primary btn-submit me-sm-3 me-1">Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Tutup
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-onboarding modal fade animate__animated" id="uploadDokument" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content p-3 p-md-5">
                <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
                aria-label="Close"></button>
                <div class="modal-body p-md-0">
                    <div class="text-center mb-4">
                        <h3 class="mb-2 pb-1">Upload Dokument Pencatatan</h3>
                        <p></p>
                    </div>
                    <form action="{{route('pencatatan.upload')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-12 mb-3">
                            <div class="form-floating form-floating-outline">
                                <input type="hidden" id="idPencatatan" name="pencatatan_id" class="form-control" value="" />
                                <input type="file" id="upload" name="dokument" class="form-control" placeholder="Upload Pencatatan"/>
                                <label>Upload Dok Pencatatan</label>
                            </div>
                        </div>
                        
                        <div class="col-12 text-center demo-vertical-spacing">
                            <button type="submit" class="btn btn-primary btn-submit me-sm-3 me-1">Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Tutup
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    @endsection
    
    @push('custom-scripts')
    @include('admin.pencatatan.javascript')
    
    <script>
        // console.log(`{{ hex2bin('303835323833343830373838') }}`)
        $(document).ready(function() {
            $("body").on("click", ".upload-record", function() {
                const id = $(this).data("id");
                $("#idPencatatan").val(id);
                $("#uploadDokument").modal('show');
            });
        })
    </script>
    @endpush