<!DOCTYPE html>

<html>
<title>Bukti Pencatatan SP/SB</title>
<head>
    
    @php
    $year = Carbon\Carbon::now()->translatedFormat('Y');
    $date = Carbon\Carbon::now()->translatedFormat('d');
    $dateM = Carbon\Carbon::now()->translatedFormat('m');
    $days = Carbon\Carbon::now()->translatedFormat('l');
    $month = Carbon\Carbon::now()->translatedFormat('F');
    @endphp
    
    <style>
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        
        ol {
            /* background: #ff9999; */
            padding: 0px;
        }
        
        ul {
            /* background: #3399ff; */
            padding: 0px;
        }
        
        ol li {
            padding: 3px;
            margin-left: 15px;
        }
        
        ul li {
            /* background: #cce5ff; */
            /* color: darkblue; */
            margin: 5px;
        }
    </style>
    
    <body style="width: 95%">
        <p class="font-arial font12" style="text-align: right; width:80%">{{ Carbon\Carbon::parse($result['tgl_surat'])->translatedFormat('d F Y')}}</p>
        
        <div style="width: 65%; float: left;">
            <table class="table" class="font-arial font12">
                <tr>
                    <td>Nomor</td>
                    <td>:</td>
                    <td>{{ $result['no_surat'] }}</td>
                </tr>
                <tr>
                    <td>Sifat</td>
                    <td>:</td>
                    <td>Penting</td>
                </tr>
                <tr>
                    <td>Lampiran</td>
                    <td>:</td>
                    <td>-</td>
                </tr>
            </table>
            <span style="width: 65%; padding:3px;" class="font-arial font12">Perihal &nbsp; &nbsp; : 
                Pencatatan dan Pemberian
                <br/>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp;&nbsp;
                Nomor Bukti Pencatatan SP/SB 
                <br/>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp;&nbsp;
                di Perusahaan
            </span>
        </div>
        <div class="font13 font-arial" style="width: 35%; float: right; text-align: center;">
            <table class="font-arial font12">
                <tr>
                    <td></td>
                    <td>Kepada</td>
                </tr>
                <tr>
                    <td>Yth.</td>
                    <td>Pengurus Serikat Pekerja</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $result['nama_serikat'] }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        {{ $result['alamat']}}
                        <br/>
                        {{' Kel. '.$result['kelurahan'].' '. 'Kec. '.$result['kecamatan']}}
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>di-</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $result['wilayah'] }}</td>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div style="width: 10cm; float: left;"></div>
        <div class="font13 font-arial" style="width: 90%; float: right;">
            <p class="font12 font-arial" align="justify">
                <span style="padding-left: 30px;">
                    Sehubungan dengan surat {{ $result['judul_surat'] }}
                    Nomor {{ $result['no_surat_permohonan'] }} tanggal {{ Carbon\Carbon::parse($result['tgl_surat_permohonan'])->translatedFormat('d F Y') }}
                    perihal Pemberitahuan dan Permohonan Pencatatan Serikat Pekerja {{ $result['nama_serikat'] }},
                    dengan ini disampaikan hal-hal sebagai berikut :
                </span>
            </p>
            <ol class="font12 font-arial" align="justify">
                <li>Bahwa pemberitahuan dan permohonan pencatatan Serikat Pekerja {{ $result['nama_serikat'] }} telah memenuhi ketentuan Pasal 9 Peraturan Gubernur Provinsi DKI Jakarta No. 10 Tahun 2007 tentang Tata Cara Pembentukan, Pencatatan, Perubahan Organisasi dan Pembubaran Serikat Pekerja / Serikat Buruh</li>
                <li>Sehubungan dengan angka 1 (satu) di atas, maka pemberitahuan dan permohonan pencatatan serikat pekerja tersebut telah kami catat dan diberikan nomor bukti pencatatan, sebagai berikut :</li>
                <ol type="A" style="padding: 20px;">
                    <li>Identitas</li>
                    <ol type="a" style="padding: 20px;">
                        <li>
                            Nama Serikat Pekerja / Serikat Buruh
                            <br>
                            {{ $result['nama_serikat'] }}
                        </li>
                        <li>
                            Jenis Serikat Pekerja / Serikat Buruh 
                            <br/>
                            {{ $result['jenis_serikat'] }}
                        </li>
                        <li>
                            Alamat / Kedudukan
                            <br/>
                            {{ $result['alamat'].' Kel. '.$result['kelurahan'].' Kec. '.$result['kecamatan'].' '.$result['wilayah'] }}
                        </li>
                        <li>
                            Status
                            <br/>
                            <span>
                                -    Mandiri / Berafiliasi		: {{ $result['status_serikat'] }}
                                <br/>
                                -	 Berafiliasi ke            	: -
                            </span> 
                        </li>
                    </ol>
                    <li>Nomor dan Tanggal Pencatatan</li>
                    <ol type="a" style="padding: 20px;">
                        <li>Nomor bukti pencatatan		: {{ $result['nomor_pencatatan'] }}</li>
                        <li>Tanggal pencatatan			: {{ Carbon\Carbon::parse($result['tgl_pencatatan'])->translatedFormat('d F Y') }}</li>
                    </ol>
                </ol>
                <span class="font-arial font12" align="justify">
                    Demikian agar Saudara maklum.
                </span>
            </ol>
            
            <br/>
            
            <div style="width: 40%; float: right; text-align: center;">
                <p class="font12 font-arial">
                    {{ $result['pilih_ttd'] != 'Kepala' ? $result['pilih_ttd'].'.' : '' }} Kepala Suku Dinas Tenaga Kerja,
                    Transmigrasi dan Energi
                    Kota Administrasi Jakarta Pusat,
                </p>
                <br/>
                <br/>
                <br/>
                <br/>
                <p class="font12 font-arial">
                    {{ $result['nama_ttd'] }}
                    <br/>
                    NIP. {{ $result['nip_ttd'] }}
                </p>
            </div>
        </div>
        
        
    </body>
</head>
</html>
