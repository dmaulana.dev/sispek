<!DOCTYPE html>

<html>
<title>Bukti Pencatatan Federasi</title>
<head>
    
    @php
    $year = Carbon\Carbon::now()->translatedFormat('Y');
    $date = Carbon\Carbon::now()->translatedFormat('d');
    $dateM = Carbon\Carbon::now()->translatedFormat('m');
    $days = Carbon\Carbon::now()->translatedFormat('l');
    $month = Carbon\Carbon::now()->translatedFormat('F');
    @endphp
    
    <style>
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        .table-collaps {
            padding: 10px;
            text-align: left;
            border: 1px solid black; 
            border-collapse: collapse;
        }
    </style>
    
    <body style="width: 95%">
        <style>
            hr {
                border: none;
                height: 1px;
                color: #333; 
                background-color: #333;
            }
        </style>
        <p class="font-arial font12" style="text-align: right; width:80%">16 Juni 2024</p>
        <div style="width: 65%; float: left;">
            <table class="font-arial font12" style="width: 65%;">
                <tr>
                    <td>Nomor</td>
                    <td>:</td>
                    <td>{{ $result['no_surat'] }}</td>
                </tr>
                <tr>
                    <td>Sifat</td>
                    <td>:</td>
                    <td>Penting</td>
                </tr>
                <tr>
                    <td>Lampiran</td>
                    <td>:</td>
                    <td> - </td>
                </tr>
                <tr>
                    <td>Perihal</td>
                    <td>:</td>
                    <td>
                        Pencatatan dan Pemberian Nomor Bukti Pencatatan SP/SB di Perusahaan
                    </td>
                </tr>
            </table>
        </div>
        <div class="font13 font-arial" style="width: 35%; float: right; text-align: center;">
            <table class="font-arial font12">
                <tr>
                    <td></td>
                    <td>Kepada</td>
                </tr>
                <tr>
                    <td>Yth.</td>
                    <td>Pengurus Serikat Pekerja</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $result['nama_serikat'] }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        {{ $result['alamat']}}
                        <br/>
                        {{' Kel. '.$result['kelurahan'].' '. 'Kec. '.$result['kecamatan']}}
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>di-</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{$result['wilayah']}}</td>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div style="width: 10cm; float: left;"></div>
        <div class="font13 font-arial" style="width: 90%; float: right;">
            <p class="font12 font-arial">
                <span style="padding-left: 30px;">
                    Sehubungan dengan surat {{ $result['judul_surat'] }} Nomor {{ $result['no_surat_permohonan'] }} tanggal {{ Carbon\Carbon::parse($result['tgl_surat_permohonan'])->translatedFormat('d F Y') }} perihal Pemberitauan dan Permohonan Pencatatan Federasi Serikat Pekerja Perkeretaapian, dengan ini disampaikan hal-hal sebagai berikut :
                </span>
            </p>
            <ol class="font12 font-arial">
                <li align="justify">
                    Bahwa pemberitahuan dan permohonan pencatatan Federasi Serikat Pekerja Perkeretaapian telah memenuhi ketentuan Pasal 9 Peraturan Gubernur Provinsi DKI Jakarta No. 10 Tahun 2007 tentang Tata Cara Pembentukan, Pencatatan, Perubahan Organisasi dan Pembubaran Serikat Pekerja / Serikat Buruh.
                </li>
                <li align="justify">Sehubungan dengan angka 1 (satu) di atas, maka pemberitahuan dan permohonan pencatatan federasi serikat pekerja tersebut telah kami catat dan diberikan nomor bukti pencatatan, sebagai berikut :</li>
                <ol type="A" style="padding: 20px;">
                    <li>Identitas</li>
                    <ol type="a" style="padding: 20px;">
                        <li>
                            Nama Federasi Serikat Pekerja / Federasi Serikat Buruh
                            <br>
                            {{ $result['nama_serikat'] }}
                        </li>
                        <li>
                            Jenis Serikat Pekerja / Serikat Buruh
                            <br/>
                            {{ $result['jenis_serikat'] }}
                        </li>
                        <li>
                            Alamat / Kedudukan
                            <br/>
                            {{ $result['alamat'].' Kel. '.$result['kelurahan'].' Kec. '.$result['kecamatan'].' '.$result['wilayah'] }}
                        </li>
                        <li>
                            Status
                            <br/>
                            <span>
                                -    Mandiri / Berafiliasi		: {{ $result['status_serikat'] }}
                                <br/>
                                -	 Berafiliasi ke            	: -
                            </span> 
                        </li>
                    </ol>
                    <li>Nomor dan Tanggal Pencatatan</li>
                    <ol type="a" style="padding: 20px;">
                        <li>Nomor bukti pencatatan		: {{ $result['nomor_pencatatan'] }}</li>
                        <li>Tanggal pencatatan			: {{ Carbon\Carbon::parse($result['tgl_pencatatan'])->translatedFormat('d F Y') }}</li>
                    </ol>
                </ol>
                <span class="font-arial font12" align="justify">
                    Demikian agar Saudara maklum.
                </span>
            </ol>
            <br/>
            
            <div style="width: 40%; float: right; text-align: center;">
                <p class="font12 font-arial">
                    {{ $result['pilih_ttd'] != 'Kepala' ? $result['pilih_ttd'].'.' : '' }} Kepala Suku Dinas Tenaga Kerja,
                    Transmigrasi dan Energi
                    Kota Administrasi Jakarta Pusat,
                </p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <p class="font12 font-arial">
                    {{ $result['nama_ttd'] }}
                    <br/>
                    NIP. {{ $result['nip_ttd'] }}
                </p>
            </div>
        </div>
        
        
    </body>
</head>
</html>
