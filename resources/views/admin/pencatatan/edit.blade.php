@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Edit Pencatatan SPSB</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pencatatan.index')}}">Edit Pencatatan SPSB</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="row invoice-add">
        <!-- surat tugas Add-->
        <div class="col-lg-12 col-12 mb-lg-0 mb-4">
            <div class="card invoice-preview-card">
                <div class="card-body">
                    <h6>Lengkapi Form dibawah ini untuk Edit penctatan</h6>
                    <br/>
                    <form action="{{ route('pencatatan.update', $data->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="bentuk_serikat" tabindex="0" id="bentukSerikat">
                                        <option></option>
                                        @foreach ($kategori as $item)
                                        <option {{ $data->bentuk_serikat === $item->nama ? 'selected' : '' }}  value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                    <label for="Bentuk Serikat">Bentuk Serikat</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="jenis_serikat" tabindex="0" id="JenisSerikat">
                                        <option {{ $data->jenis_serikat === 'Serikat di Perushaan' ? 'selected' : '' }} value="Serikat di Perushaan">Serikat di Perushaan</option>
                                        <option {{ $data->jenis_serikat === 'Serikat di luar Perushaan' ? 'selected' : '' }} value="Serikat di luar Perushaan">Serikat di luar Perushaan</option>
                                    </select>
                                    <label for="Jenis Serikat">Jenis Serikat</label>
                                </div>
                            </div>
                            <div class="col-sm-12 selectOpt">
                                <div class="form-floating form-floating-outline mb-3">
                                    {{-- <select class="form-select nama_serikat" name="nama_serikat" tabindex="0" id="namaSerikat">
                                        
                                    </select> --}}
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $data->nama_serikat }}"
                                    name="nama_panjang"
                                    tabindex="0"
                                    id="namaSerikat" />
                                    <label for="namaSerikat">Nama Panjang SP/SB</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $data->nama_singkat }}"
                                    name="nama_singkatan"
                                    tabindex="0"
                                    id="namaSingkatan" />
                                    <label for="namaSingkatan">Nama Singkatan SP/SB</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="status_serikat" tabindex="0" id="statusSerikat">
                                        <option {{ $data->status_serikat === 'Mandiri' ? 'selected' : ''}} value="Mandiri">Mandiri</option>
                                        <option {{ $data->status_serikat === 'Afiliasi' ? 'selected' : ''}} value="Afiliasi">Afiliasi</option>
                                    </select>
                                    <label for="statusSerikat">Status Serikat</label>
                                </div>
                            </div>
                            @if ($data->status_serikat === 'Afiliasi')
                            <div class="col-sm-12 namaAfiliasi">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select targetAfiliasi" name="afiliasi" tabindex="0" id="afiliasi">
                                        @foreach ($list_afiliasi as $item)
                                        <option {{ $data->afiliasi === $item->nama ? 'selected' : '' }}  value="{{ $item->nama }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                    <label for="afiliasi">Nama Afiliasi</label>
                                </div>
                            </div>
                            @else
                            @endif
                            <div class="col-sm-12">
                                <div class="form-floating form-floating-outline mb-3">
                                    <textarea class="form-control" rows="5" name="alamat">{{ $data->alamat }}</textarea>
                                    <label for="alamat">Alamat Lengkap</label>
                                </div>
                            </div>
                            <div class="mt-4">
                                <p><b>Nama Perusahaan</b></p>
                            </div>
                            <div class="row field_wrapper">
                                <div class="form-floating form-floating-outline mb-3 col-md-11 col-sm-11 col-11">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value=""
                                    name="perusahaan[]"
                                    tabindex="0"
                                    id="perusahaan" />
                                    <label for="perusahaan">Perusahaan</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-3 col-md-1 col-sm-1 col-1">
                                    <button type="button" class="btn btn-success add_input_button"><i class="fa fa-plus-circle"></i></button>
                                </div>
                            </div>
                            @foreach ($perusahaan as $item)
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $item->nama_perusahaan }}"
                                    tabindex="0"
                                    disabled
                                    id="perusahaan" />
                                    <label for="perusahaan">Perusahaan</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <button type="button" class="btn btn-danger delete-record-perusahaan"><i class="fa fa-minus-circle"></i></button>
                                </div>
                            </div>
                            @endforeach
                            {{-- <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="DKI Jakarta"
                                    tabindex="0"
                                    id="provinsi" />
                                    <label for="provinsi">Provinsi</label>
                                </div>
                            </div> --}}
                            {{-- <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" tabindex="0" id="kotaAdm">
                                        <option value="Jakarta Pusat">Jakarta Pusat</option>
                                        <option value="Jakarta Selatan">Jakarta Selatan</option>
                                        <option value="Jakarta Timur">Jakarta Timur</option>
                                        <option value="Jakarta Utara">Jakarta Utara</option>
                                        <option value="Jakarta Barat">Jakarta Barat</option>
                                    </select>
                                    <label for="kotaAdm">Kota Administrasi</label>
                                </div>
                            </div> --}}
                            {{-- <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" tabindex="0" id="kecamatan">
                                        <option value="Jakarta Pusat">Jakarta Pusat</option>
                                        <option value="Jakarta Selatan">Jakarta Selatan</option>
                                        <option value="Jakarta Timur">Jakarta Timur</option>
                                        <option value="Jakarta Utara">Jakarta Utara</option>
                                        <option value="Jakarta Barat">Jakarta Barat</option>
                                    </select>
                                    <label for="kecamatan">Kecamatan</label>
                                </div>
                            </div> --}}
                            {{-- <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" tabindex="0" id="kelurahan">
                                        <option value="Rawamangun">Rawamangun</option>
                                        <option value="Jakarta Selatan">Jakarta Selatan</option>
                                        <option value="Jakarta Timur">Jakarta Timur</option>
                                        <option value="Jakarta Utara">Jakarta Utara</option>
                                        <option value="Jakarta Barat">Jakarta Barat</option>
                                    </select>
                                    <label for="kelurahan">Kelurahan</label>
                                </div>
                            </div> --}}
                            <div class="mt-4">
                                <p><b>Struktur Kepengurusan</b></p>
                            </div>
                            <div class="col-sm-12">
                                <table id="item_table" class="table">
                                    <tbody>
                                        <tr class="border-bottom-primary">
                                            <td>
                                                <div class="form-floating form-floating-outline mb-3">
                                                    <input
                                                    class="form-control"
                                                    type="text"
                                                    value=""
                                                    name="nama[]"
                                                    tabindex="0"
                                                    id="nama" />
                                                    <label for="nama">Nama</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-floating form-floating-outline mb-3">
                                                    <input
                                                    class="form-control"
                                                    type="text"
                                                    value=""
                                                    name="jabatan[]"
                                                    tabindex="0"
                                                    id="jabatan" />
                                                    <label for="jabatan">Jabatan</label>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            @if (!empty($susunan))
                            @foreach ($susunan as $item)
                            <div class="col-sm-3">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $item->nama }}"
                                    tabindex="0"
                                    disabled
                                    id="nama" />
                                    <label for="nama">Nama</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $item->jabatan }}"
                                    tabindex="0"
                                    disabled
                                    id="Jabatan" />
                                    <label for="Jabatan">Jabatan</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <button type="button" class="btn btn-danger delete-record-perusahaan"><i class="fa fa-minus-circle"></i></button>
                                </div>
                            </div>
                            @endforeach
                            @else
                            @endif
                            
                            <div class="mt-4">
                            </div>
                            <div class="col-sm-6 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="date"
                                    value="{{ $data->tgl_pencatatan }}"
                                    name="tgl_pencatatan"
                                    tabindex="0"
                                    id="tglPencatatan" />
                                    <label for="tglPencatatan">Tanggal Pencatatan</label>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="file"
                                    value=""
                                    name="logo_image"
                                    tabindex="0"
                                    id="logo" />
                                    <label for="logo">Upload Logo</label>
                                    <a href="{{ $data->logo_url }}" target="_blank" class="">Lihat Logo</a>
                                </div>
                            </div>
                            <hr/>
                            <div class="col-sm-3 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $data->no_surat }}"
                                    name="no_surat"
                                    tabindex="0"
                                    id="noSurat" />
                                    <label for="tglPencatatan">No Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-2 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="date"
                                    name="tgl_surat"
                                    value="{{ $data->tgl_surat }}"
                                    tabindex="0"
                                    id="tglSurat" />
                                    <label for="tglSurat">Tgl Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-7 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{$data->judul_surat}}"
                                    name="judul_surat"
                                    tabindex="0"
                                    id="judulSurat" />
                                    <label for="judulSurat">Judul Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $data->no_surat_permohonan }}"
                                    name="no_permohonan"
                                    tabindex="0"
                                    id="noPermohonan" />
                                    <label for="noPermohonan">No Permohonan</label>
                                </div>
                            </div>
                            <div class="col-sm-4 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="date"
                                    name="tgl_permohonan"
                                    tabindex="0"
                                    value="{{$data->tgl_surat_permohonan}}"
                                    id="tglPermohonan" />
                                    <label for="tglPermohonan">Tgl Permohonan</label>
                                </div>
                            </div>
                            <div class="col-sm-3 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="file"
                                    name="permohonan_berkas_url"
                                    tabindex="0"
                                    id="uploadSurat" />
                                    <label for="judulSurat">Upload Surat Permohonan</label>
                                    <small>
                                        <a href="{{ $data->permohonan_berkas_url }}">Dokument Sebelumnya</a>
                                    </small>
                                </div>
                            </div>
                            <div class="col-sm-2 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="pilih_ttd" tabindex="0" id="pilihKepala">
                                        <option {{ $data->pilih_ttd == 'Kepala' ? 'selected' : '' }} value="Kepala">Kepala</option>
                                        <option {{ $data->pilih_ttd == 'PLH' ? 'selected' : '' }} value="PLH">PLH</option>
                                        <option {{ $data->pilih_ttd == 'PLT' ? 'selected' : '' }} value="PLT">PLT</option>
                                    </select>
                                    <label for="pilihKepala">Pilih Kepala</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $data->nama_ttd }}"
                                    name="nama_ttd"
                                    tabindex="0"
                                    id="tglPencatatan" />
                                    <label for="tglPencatatan">Nama TTD</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="number"
                                    value="{{ $data['nip_ttd'] }}"
                                    name="nip_ttd"
                                    tabindex="0"
                                    id="logo" />
                                    <label for="logo">NIP TTD</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 mt-2">
                            <button type="sumbit" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;  Simpan & Cetak </button>
                            <button type="button" onclick="history.back()" class="btn btn-danger"><i class="fa fa-minus-circle"></i>&nbsp;  Cancel </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('custom-scripts')

<script>
    
    $(document).ready(function() {
        var max_fields = 10;
        var $add_input_button = $('.add_input_button');
        var $field_wrapper = $('.field_wrapper');
        var new_field_html = `<div>
            <div class="col-sm-6 mt-2">
                <div class="form-floating form-floating-outline mb-3">
                    <input
                    class="form-control"
                    type="text"
                    value=""
                    name="perusahaan[]"
                    tabindex="0"
                    id="perusahaan" />
                    <button style="float: right padding:20px" type="button" class="btn btn-danger remove_input_button"><i class="fa fa-minus-circle"></i></button>
                </div>
            </div>
        </div>`;
        var input_count = 1;
        
        //add inputs
        $add_input_button.click(function() {
            if (input_count < max_fields) {
                input_count++;
                $field_wrapper.append(new_field_html);
            }
        });
        
        //remove_input
        $field_wrapper.on('click', '.remove_input_button', function(e) {
            e.preventDefault();
            $(this).parent('div').remove();
            input_count--;
        });
    });
    
</script>


<script>
    $(document).ready(function(){
        
        var count = 1;
        
        dynamic_field(count);
        
        function dynamic_field(number)
        {
            html = '<tr>';
                html += `<td>
                    <div class="form-floating form-floating-outline mb-3">
                        <input
                        class="form-control"
                        type="text"
                        value=""
                        name="nama[]"
                        tabindex="0"
                        id="nama" />
                        <label for="nama">Nama</label>
                    </div>
                </td>`;
                html += `<td>
                    <div class="form-floating form-floating-outline mb-3">
                        <input
                        class="form-control"
                        type="text"
                        value=""
                        name="jabatan[]"
                        tabindex="0"
                        id="jabatan" />
                        <label for="jabatan">Jabatan</label>
                    </div>
                </td>`;
                if(number > 1)
                {
                    html += '<td><button id="remove" type="button" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></tr>';
                        $('tbody').append(html);
                    }
                    else
                    {   
                        html += '<td><button id="add" type="button" class="btn btn-success"><i class="fa fa-plus-circle"></i></button></td></tr>';
                        $('tbody').html(html);
                    }
                }
                
                $(document).on('click', '#add', function(){
                    count++;
                    dynamic_field(count);
                });
                
                $(document).on('click', '#remove', function(){
                    count--;
                    $(this).closest("tr").remove();
                }); 
                
                
            });
        </script>
        
        <script>
            $('#statusSerikat').on('change', function (e) {
                var optionSelected = $("option:selected", this);
                var valueSelected = this.value;
                
                let _this = this;
                if (valueSelected == 'Afiliasi') {
                    $('.namaAfiliasi').show();
                    $.ajax({
                        url: `{{ route('master-kategori-affiliasi.get-data') }}`,
                        type: "GET",
                        data : {
                            'ids' : valueSelected
                        },
                        cache: false,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            $('.targetAfiliasi').html(response.option);
                            // let data = response.data
                            // const Item = $(_this).closest('select').find('.targetAfiliasi').html();
                            // data.map(d => {
                                //     $(Item).append(`<option value="${d.nama}">${d.nama}</option>`)
                                
                                // });
                            },
                            error: function(jqXhr, json, errorThrown) {
                                console.log(errorThrown);
                            }
                            
                        });
                    }
                    else{
                        $('.namaAfiliasi').hide();
                    }
                });
            </script>
            
            <script>
                $("body").on("click", ".delete-record-perusahaan", function() {
                    const id = $(this).data("id");
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                    }).then(({
                        dismiss,
                        value
                    }) => {
                        if (value !== undefined) {
                            $.ajax({
                                url: `/perusahaan/destroy/${id}`,
                                type: "GET",
                                success: function(response) {
                                    if (response.type == "success") {
                                        Swal.fire({
                                            type: 'success',
                                            title: 'Deleted Success',
                                            showConfirmButton: true,
                                            timer: 3000,
                                        }).then(() => location.reload())
                                        
                                    } else {
                                        Swal.fire({
                                            type: 'error',
                                            title: 'Failed!',
                                            text: "Deleted Not Success",
                                            showCancelButton: true,
                                            timer: 3000,
                                        }).then(() => location.reload())
                                    }
                                },
                            });
                        }
                    })
                });
            </script>
            
            @endpush