@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>History Pencatatan</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pencatatan.index')}}">History Pencatatan</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="row invoice-add">
        <!-- surat tugas Add-->
        <div class="col-lg-12 col-12 mb-lg-0 mb-4">
            <div class="card mb-4">
                <h4 class="card-header">{{ $data->nama_serikat }}</h4>
                <!-- Account -->
                <div class="card-body">
                    <div class="d-flex align-items-start align-items-sm-center gap-4">
                        <img
                        src="{{ $data->logo_url }}"
                        alt="logo"
                        class="d-block w-px-120 h-px-120 rounded"
                        id="uploadedAvatar" />
                        <div class="button-wrapper">
                            <h5>{{ !empty($data->nama_singkat) ? $data->nama_singkat : 'Nama Singkat' }}</h5>
                            <div class="text-muted small mt-3">
                                @if ($data->visible === 1)
                                <span class="badge bg-label-primary rounded-pill">Aktif</span>
                                @else
                                <span class="badge bg-label-danger rounded-pill">Non Aktif</span>
                                @endif
                            </div>
                            <br/>
                            <a href="{{ route('pencatatan.pdf', $data->id) }}">Cetak Berkas Pencatatan</a>
                        </div>
                    </div>
                </div>
                <!-- Current Plan -->
                <h5 class="card-header">{{ $data->nomor_pencatatan }}
                    <br/>
                    <small>
                        @if (!empty($dokument))
                        <a href="{{ $data->dokument_url }}" target="_blank">Dokument After Pencatatan</a>
                        @else
                        -
                        @endif
                    </small>
                </h5>
                
                <div class="card-body pt-1">
                    <div class="row">
                        <div class="col-md-6 mb-1">
                            <div class="mb-4">
                                <h6 class="mb-1 fw-semibold">Bentuk Serikat</h6>
                                <p>{{ $data->bentuk_serikat }}</p>
                            </div>
                            <div class="mb-4">
                                <h6 class="mb-1 fw-semibold">Jenis Serikat</h6>
                                <p>{{ $data->jenis_serikat }}</p>
                            </div>
                            <div class="mb-4">
                                <h6 class="mb-1 fw-semibold">Afiliasi</h6>
                                <p>{{ $data->afiliasi }}</p>
                            </div>
                            <div>
                                <h6 class="mb-1 fw-semibold">Alamat Lengkap</h6>
                                <p class="mb-0">
                                    {{ $data->alamat }}, Kelurahan {{ $data->kelurahan }}, Kecamatan {{ $data->kecamatan }},
                                    <br/>
                                    {{ $data->wilayah }}
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 mb-1">
                            <div class="mb-4">
                                <h6 class="mb-1 fw-semibold">Surat Pencatatan</h6>
                                <p>{{ $data->no_surat }} - {{ $data->tgl_surat }}</p>
                            </div>
                            <div class="mb-4">
                                <h6 class="mb-1 fw-semibold">Judul Surat Pencatatan</h6>
                                <p>{{ !empty($data->judul_surat) ? $data->judul_surat : '-' }}</p>
                            </div>
                            <div class="mb-4">
                                <h6 class="mb-1 fw-semibold">Surat Permohonan</h6>
                                <p>{{ $data->no_surat_permohonan }} - {{ $data->tgl_surat_permohonan }}</p>
                                @if (!empty($data->permohonan_berkas_url))
                                <a href="{{ $data->permohonan_berkas_url }}" target="_blank">Dokument Permohonan</a>
                                @else
                                -
                                @endif
                            </div>
                        </div>
                        <div class="col-4 mt-4">
                            <h6 class="text-body fw-semibold">Nama Perusahaan</h6>
                            @foreach ($perusahaan as $item)
                            <li>{{$item->nama_perusahaan}}</li>
                            @endforeach
                        </div>
                        <div class="col-4 mt-4">
                            <h6 class="text-body fw-semibold">Susunan Pengurus</h6>
                            <div class="col-8 d-flex gap-2 flex-wrap">
                                @foreach ($susunan as $item)
                                <h6 class="text-body">
                                    <span style="padding: 5px;"><b>{{ $item->jabatan }}</b>  - {{ $item->nama }}</span>
                                    &nbsp; &nbsp;
                                </h6>
                                @endforeach
                            </div>
                        </div>
                        
                        
                    </div>
                    <!-- /Account -->
                </div>
            </div>
            <div class="card mb-4">
                <h5 class="card-header pb-4">History Pencatatan <b>{{ $data->nama_serikat }}</b></h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach ($perubahan as $item)
                            <div class="bg-lighter rounded p-3 mb-3 position-relative">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="me-2 fw-semibold">Perubahan KE-{{$item->perubahan_ke}}</span>
                                </div>
                                <div class="align-items-center mb-3">
                                    <h4 class="mb-0 me-3">{{ $item->perubahan_jenis }}</h4>
                                    <span class="badge bg-label-primary rounded-pill mt-2">
                                        Dirubah Pada Tanggal <b>{{ Carbon\Carbon::parse($item->tgl_perubahan)->translatedFormat('d F Y') }}</b>
                                    </span>
                                </div>
                                {{-- <span class="text-muted">comming soon</span> --}}
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table">
                                            <tr>
                                                <td>No Surat</td>
                                                <th>:</th>
                                                <th>{{ $item->no_surat }}</th>
                                            </tr>
                                            <tr>
                                                <td>Tgl Surat</td>
                                                <th>:</th>
                                                <th>{{ Carbon\Carbon::parse($item->tgl_surat)->translatedFormat('d F Y') }}</th>
                                            </tr>
                                            <tr>
                                                <td>No Surat Permohonan</td>
                                                <th>:</th>
                                                <th>{{ $item->no_surat_permohonan }}</th>
                                            </tr>
                                            <tr>
                                                <td>Tgl Surat Permohonan</td>
                                                <th>:</th>
                                                <th>{{ Carbon\Carbon::parse($item->tgl_surat_permohonan)->translatedFormat('d F Y') }}</th>
                                            </tr>
                                            <tr>
                                                <td>Dok Surat Permohonan</td>
                                                <th>:</th>
                                                <th>
                                                    <a href="{{ $item->dok_surat_permohonan }}" target="_blank">Lihat Dokument Permohonan</a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>Nama Serikat</td>
                                                <th>:</th>
                                                <th>{{ $item->nama_serikat }}</th>
                                            </tr>
                                            <tr>
                                                <td>Nama Singkat</td>
                                                <th>:</th>
                                                <th>{{ $item->nama_singkat }}</th>
                                            </tr>
                                            <tr>
                                                <td>Logo / Lambang</td>
                                                <th>:</th>
                                                <th>
                                                    <a href="{{ $item->logo_url }}" target="_blank">Lihat Logo / Lambang</a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>Cetak / Unduh Berkas</td>
                                                <th>:</th>
                                                <th>
                                                    @if (!empty($item->dokument_url))
                                                    <a href="{{ $item->dokument_url }}" target="_blank">Cetak Berkas</a>
                                                    @else
                                                    -
                                                    @endif
                                                </th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @foreach ($pengganti as $key => $item)
                            <div class="bg-lighter rounded p-3 mb-3 position-relative">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="me-2 fw-semibold">Pengganti KE-{{$key+1}}</span>
                                </div>
                                <div class="align-items-center mb-3">
                                    <h4 class="mb-0 me-3">Pengganti Kehilangan</h4>
                                    <span class="badge bg-label-primary rounded-pill mt-2">
                                        Diganti Pada Tanggal <b>{{ Carbon\Carbon::parse($item->created_at)->translatedFormat('d F Y') }}</b>
                                    </span>
                                </div>
                                {{-- <span class="text-muted">comming soon</span> --}}
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table">
                                            <tr>
                                                <td>No Surat</td>
                                                <th>:</th>
                                                <th>{{ $item->no_surat }}</th>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Surat</td>
                                                <th>:</th>
                                                <th>{{ Carbon\Carbon::parse($item->tgl_surat)->translatedFormat('d F Y') }}</th>
                                            </tr>
                                            <tr>
                                                <td>Perihal Surat</td>
                                                <th>:</th>
                                                <th>{{ $item->perihal }}</th>
                                            </tr>
                                            <tr>
                                                <td>No Surat Permohonan</td>
                                                <th>:</th>
                                                <th>{{ $item->no_surat_permohonan }}</th>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Surat Permohonan</td>
                                                <th>:</th>
                                                <th>{{ Carbon\Carbon::parse($item->tgl_surat_permohonan)->translatedFormat('d F Y') }}</th>
                                            </tr>
                                            <tr>
                                                <td>Dok Surat Permohonan</td>
                                                <th>:</th>
                                                <th>
                                                    <a href="{{ $item->dok_surat_permohonan }}" target="_blank">Lihat Dokument Permohonan</a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>No Surat Keterangan Hilang</td>
                                                <th>:</th>
                                                <th>{{ $item->no_surat_ket_hilang }}</th>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Laporan Kehilangan</td>
                                                <th>:</th>
                                                <th>{{ Carbon\Carbon::parse($item->tgl_kehilangan)->translatedFormat('d F Y') }}</th>
                                            </tr>
                                            <tr>
                                                <td>Surat Keterangan Hilang Dari</td>
                                                <th>:</th>
                                                <th>{{ $item->surat_ket_hilang }}</th>
                                            </tr>
                                            <tr>
                                                <td>Keterangan Hilang</td>
                                                <th>:</th>
                                                <th>{{ $item->keterangan_hilang }}</th>
                                            </tr>
                                            <tr>
                                                <td>Dokument Kehilangan</td>
                                                <th>:</th>
                                                <th>
                                                    <a href="{{ $item->dokument_kehilangan }}" target="_blank">Dokument Kehilangan</a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>Cetak / Unduh Berkas</td>
                                                <th>:</th>
                                                <th>
                                                    @if (!empty($item->dokument_url))
                                                    <a href="{{ $item->dokument_url }}" target="_blank">Cetak Berkas</a>
                                                    @else
                                                    -
                                                    @endif
                                                </th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @foreach ($penonaktifan as $key => $item)
                            <div class="bg-lighter rounded p-3 mb-3 position-relative">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="me-2 fw-semibold">Penonaktifan KE-{{$key+1}}</span>
                                </div>
                                <div class="align-items-center mb-3">
                                    <h4 class="mb-0 me-3">Penonaktifan {{$item->status_alasan}}</h4>
                                    <span class="badge bg-label-primary rounded-pill mt-2">
                                        Di Non Aktifkan Oleh
                                        <b>
                                            {{$item->created_by}}
                                        </b> 
                                        Pada Tanggal 
                                        <b>
                                            {{ Carbon\Carbon::parse($item->tgl_kehilangan)->translatedFormat('d F Y') }}
                                        </b>
                                    </span>
                                </div>
                                {{-- <span class="text-muted">comming soon</span> --}}
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table">
                                            <tr>
                                                <td>No Surat</td>
                                                <th>:</th>
                                                <th>{{ $item->no_surat }}</th>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Surat</td>
                                                <th>:</th>
                                                <th>{{ Carbon\Carbon::parse($item->tgl_surat)->translatedFormat('d F Y') }}</th>
                                            </tr>
                                            <tr>
                                                <td>No Surat Permohonan</td>
                                                <th>:</th>
                                                <th>{{ $item->no_surat_permohonan }}</th>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Surat Permohonan</td>
                                                <th>:</th>
                                                <th>{{ Carbon\Carbon::parse($item->tgl_surat_permohonan)->translatedFormat('d F Y') }}</th>
                                            </tr>
                                            <tr>
                                                <td>Dok Surat Permohonan</td>
                                                <th>:</th>
                                                <th>
                                                    <a href="{{ $item->dok_surat_permohonan }}" target="_blank">Lihat Dokument Permohonan</a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>Pindah KE</td>
                                                <th>:</th>
                                                <th>
                                                    {{ $item->pindah_ke }}
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>Keterangan</td>
                                                <th>:</th>
                                                <th>
                                                    {{ $item->keterangan }}
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>Cetak / Unduh Berkas</td>
                                                <th>:</th>
                                                <th>
                                                    @if (!empty($item->dokument_url))
                                                    <a href="{{ $item->dokument_url }}" target="_blank">Cetak Berkas</a>
                                                    @else
                                                    -
                                                    @endif
                                                </th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            
            @endsection
            
            @push('custom-scripts')
            
            
            @endpush