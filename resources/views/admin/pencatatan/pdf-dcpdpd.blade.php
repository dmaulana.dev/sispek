<!DOCTYPE html>

<html>
<title>Bukti Pencatatan SP/SB</title>
<head>
    
    @php
    $year = Carbon\Carbon::now()->format('Y');
    $date = Carbon\Carbon::now()->format('d');
    $dateM = Carbon\Carbon::now()->format('m');
    $days = Carbon\Carbon::now()->format('l');
    $month = Carbon\Carbon::now()->format('F');
    @endphp
    
    <style>
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        .table-collaps {
            padding: 10px;
            text-align: left;
            border: 1px solid black; 
            border-collapse: collapse;
        }
    </style>
    
    <body style="width: 95%">
        {{-- <div class="font12 font-arial-narrow" style="width: 100%; float: right; text-align: center;">
            <span>
                <b>PEMERINTAH PROVINSI DAERAH KHUSUS IBUKOTA JAKARTA</b>
                <br/>
                <b>DINAS PERINDUSTRIAN, PERDAGANGAN, KOPERASI, USAHA KECIL DAN MENENGAH</b>
                <br/>
                <span class="font-arial-narrow font12">
                    Jl. Perintis Kemerdekaan/ BGR I No. 3, Jakarta Utara
                    <br/>
                    Telp. 458 48014 – 458 48055 – 458 48011 – 453 4313 – 458 76685  Fax. 021-7814415
                    <br/>
                    Website http://disppkukm.jakarta.go.id/, Email : disppkukm@jakarta.go.id
                    <br/>
                    J A K A R T A
                </span>
            </span>
        </div> --}}
        {{-- <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div style="text-align: right; width:100%;" class="font11">
                <hr/>
            </div> --}}
            <style>
                hr {
                    border: none;
                    height: 1px;
                    color: #333; 
                    background-color: #333;
                }
            </style>
            <p class="font-arial font12" style="text-align: right; width:80%">16 Juni 2024</p>
            <div style="width: 65%; float: left;">
                <table class="font-arial font12" style="width: 65%;">
                    <tr>
                        <td>Nomor</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Sifat</td>
                        <td>:</td>
                        <td>Penting</td>
                    </tr>
                    <tr>
                        <td>Lampiran</td>
                        <td>:</td>
                        <td> - </td>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td>:</td>
                        <td>
                            Pencatatan dan Pemberian Nomor Bukti Pencatatan SP/SB di Perusahaan
                        </td>
                    </tr>
                </table>
            </div>
            <div class="font13 font-arial" style="width: 35%; float: right; text-align: center;">
                <table class="font-arial font12">
                    <tr>
                        <td></td>
                        <td>Kepada</td>
                    </tr>
                    <tr>
                        <td>Yth.</td>
                        <td>Pengurus Serikat Pekerja</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>PT. Kereta Api Properti Manajemen</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Jl. KH. Samanhudi Kel. Pasar Baru, Kec. Sawah Besar</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>di-</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Jakarta Pusat 10710</td>
                    </tr>
                </table>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div style="width: 10cm; float: left;"></div>
            <div class="font13 font-arial" style="width: 90%; float: right; text-align: center;">
                <p class="font12 font-arial" align="justify">1. Bahwa pemberitahuan dan permohonan pencatatan Serikat Pekerja PT. Kereta Api Properti Manajemen telah memenuhi ketentuan Pasal 9 Peraturan Gubernur Provinsi DKI Jakarta No. 10 Tahun 2007 tentang Tata Cara Pembentukan, Pencatatan, Perubahan Organisasi dan Pembubaran Serikat Pekerja / Serikat Buruh</p>
                <p class="font12 font-arial" align="justify">2. Sehubungan dengan angka 1 (satu) di atas, maka pemberitahuan dan permohonan pencatatan serikat pekerja tersebut telah kami catat dan diberikan nomor bukti pencatatan, sebagai berikut :</p>
                <p class="font12 font-arial" align="justify" style="margin-left: 3%;">A. Identitas</p>
                <p class="font12 font-arial" align="justify" style="margin-left: 5%;">
                    a. Nama Serikat Pekerja / Serikat Buruh
                    <br/>
                    <span>Serikat Pekerja PT. Kereta Api Properti Manajemen</span>
                </p>
                <p class="font12 font-arial" align="justify" style="margin-left: 5%;">
                    b.	Jenis Serikat Pekerja / Serikat Buruh 
                    <br/>
                    <span>Serikat Pekerja di Perusahaan</span>
                </p>
                <p class="font12 font-arial" align="justify" style="margin-left: 5%;">
                    c.	Alamat / Kedudukan
                    <br/>
                    <span>
                        Jl. KH Samanhudi, Kelurahan Pasar Baru,
                        Kecamatan Sawah Besar, Kota Administrasi Jakarta Pusat 10710
                    </span>                    
                </p>
                <p class="font12 font-arial" align="justify" style="margin-left: 5%;">
                    d.	Status
                    <br/>
                    <span>
                        -    Mandiri / Berafiliasi		: Mandiri
                        <br/>
                        -	 Berafiliasi ke            	: -
                        
                    </span>                    
                </p>
                <p class="font12 font-arial" align="justify" style="margin-left: 3%;">
                    B.	Nomor dan Tanggal Pencatatan
                </p>
                <p class="font12 font-arial" align="justify" style="margin-left: 5%;">
                    <span>
                        a.	Nomor bukti pencatatan		: 788/SP/JP/VI/2023 
                        <br/>
                        b.	Tanggal pencatatan			: 16 Juni 2023
                        
                    </span>                    
                </p>
                <p class="font-arial font12" align="justify">
                    Demikian agar Saudara maklum.
                    <br/>
                </p>
                <br/>
                
                <div style="width: 40%; float: right; text-align: center;">
                    <p class="font12 font-arial">
                        Kepala Suku Dinas Tenaga Kerja,
                        Transmigrasi dan Energi
                        Kota Administrasi Jakarta Pusat,
                        
                    </p>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <p class="font12 font-arial">
                        Sudrajad, S.E., M.M.
                        <br/>
                        NIP. 196603261986031005
                    </p>
                </div>
            </div>
            
            
        </body>
    </head>
    </html>
    