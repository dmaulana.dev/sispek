<!DOCTYPE html>

<html>
<title>Rekapitulasi Anggota Federasi dan Konfederasi</title>
<head>
    
    @php
    $year = Carbon\Carbon::now()->format('Y');
    $date = Carbon\Carbon::now()->format('d');
    $dateM = Carbon\Carbon::now()->format('m');
    $days = Carbon\Carbon::now()->format('l');
    $month = Carbon\Carbon::now()->format('F');
    @endphp
    
    <style>
        #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        
        #customers td, #customers th {
            border: 1px solid #adacac;
            padding: 8px;
            text-align: center;
        }
        
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        
        #customers tr:hover {background-color: #ddd;}
        
        #customers th {
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: left;
            background-color: #999999;
            color: white;
            text-align: center;
            font-size: 13px;
            border: 1px solid #767575
        }
        
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        .table-collaps {
            padding: 10px;
            text-align: left;
            border: 1px solid black; 
            border-collapse: collapse;
        }
    </style>
    
    <body style="width: 100%">
        <div class="font12 font-arial-narrow" style="width: 100%; float: right; text-align: center;">
            <span>
                <b>SUKU DINAS TENAGA KERJA, TRANSMIGRASI DAN ENERGI</b>
                <br/>
                <b>KOTA ADMINISTRASI JAKARTA PUSAT</b>
                <br/>
                <b>
                    DAFTAR SERIKAT PEKERJA/SERIKAT BURUH, FEDERASI DAN KONFEDERASI
                </b>
            </span>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div>
            <table id="customers">
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Jenis SP/SB</th>
                    <th rowspan="2">S.D. TAHUN 2023</th>
                    <th rowspan="2">TAHUN 2024</th>
                    <th rowspan="2">JUMLAH</th>
                    <th colspan="3">JUMLAH SERIKAT DI  PERUSAHAAN</th>
                    <th colspan="3">JUMLAH SERIKAT DI DILUAR PERUSAHAAN</th>
                    <th colspan="3">JUMLAH AKTIF</th>
                    <th colspan="3">JUMLAH NON-AKTIF</th>
                </tr>
                <tr>
                    <th>Mandiri</th>
                    <th>Afiliasi</th>
                    <th>Total</th>
                    
                    <th>Mandiri</th>
                    <th>Afiliasi</th>
                    <th>Total</th>
                    
                    <th>Mandiri</th>
                    <th>Afiliasi</th>
                    <th>Total</th>
                    
                    <th>Mandiri</th>
                    <th>Afiliasi</th>
                    <th>Total</th>
                </tr>
                @foreach ($data as $key => $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item['bentuk_serikat'] }}</td>
                    <td>{{ $item['tahun_2023'] }}</td>
                    <td>{{ $item['tahun_2024'] }}</td>
                    <td>{{ $item['jumlah'] }}</td>
                    
                    <td>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['mandiri'] : 0 }}</td>
                    <td>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['afiliasi'] : 0 }}</td>
                    <td>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['total'] : 0 }}</td>
                    
                    <td>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['mandiri'] : 0 }}</td>
                    <td>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['afiliasi'] : 0 }}</td>
                    <td>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['total'] : 0 }}</td>
                    
                    <td>{{ $item['mandiri_aktif'] }}</td>
                    <td>{{ $item['afiliasi_aktif'] }}</td>
                    <td>{{ $item['jumlah_aktif'] }}</td>
                    
                    <td>{{ $item['mandiri_nonaktif'] }}</td>
                    <td>{{ $item['afiliasi_nonaktif'] }}</td>
                    <td>{{ $item['jumlah_nonaktif'] }}</td>
                </tr>
                @endforeach
            </table>
        </div>
        
        
    </body>
</head>
</html>
