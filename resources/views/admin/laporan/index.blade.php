@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Daftar Laporan SISPEK</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pencatatan.index')}}">Daftar Laporan SISPEK</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header">
            <form action="{{route('daftar.laporan.export')}}" method="get">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label><strong>Kategori :</strong></label>
                            <select name="bentuk_serikat" id='bentuk_serikat' class="form-control" style="width: 200px">
                                <option value="">--Pilih Bentuk Serikat--</option>
                                <option value="SP/SB">SP/SB</option>
                                <option value="Federasi">Federasi</option>
                                <option value="Konfederasi">Konfederasi</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label><strong>Afiliasi :</strong></label>
                            <select name="status_serikat" id='status_serikat' class="form-control" style="width: 200px">
                                <option value="">--Pilih Status Serikat--</option>
                                <option value="Afiliasi">Afiliasi</option>
                                <option value="Mandiri">Mandiri</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label><strong>Status Active :</strong></label>
                            <select name="visible" id='visible' class="form-control" style="width: 200px">
                                <option value="">--Pilih Status--</option>
                                <option value="1">Active</option>
                                <option value="0">Deactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label><strong>Status Progress :</strong></label>
                            <select name="status" id='status' class="form-control" style="width: 200px">
                                <option value="">--Pilih Progres--</option>
                                <option value="0">Proses</option>
                                <option value="1">Selesai</option>
                                <option value="2">Ditolak</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label><strong>Pilih Export :</strong></label>
                            <select name="pilih_export" class="form-control" style="width: 200px">
                                <option value="">--Pilih Export--</option>
                                <option value="excell">Excell</option>
                                <option value="pdf">PDF</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {{-- <label><strong>Export :</strong></label> --}}
                            <br/>
                            <button type="submit" class="btn-sm btn btn-dark">
                                <i class="mdi mdi-file-import me-0 me-sm-1" style="color: white"></i>
                                <span class="d-none d-sm-inline-block" style="color: white">EXPORT</span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-datatable table-responsive">
            <table class="data-table table table-bordered">
                <thead>
                    <tr>
                        <th>No Pencatatan</th>
                        <th>Bentuk Serikat</th>
                        <th>Nama Serikat</th>
                        <th>Tgl Pencatatan</th>
                        <th>Status SPSB</th>
                        <th>Afiliasi</th>
                        <th>Status</th>
                        <th>Visible</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    @endsection
    
    @push('custom-scripts')
    {{-- @include('admin.laporan.javascript') --}}
    
    <script type="text/javascript">
        $(function () {
            
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('daftar.laporan.index') }}",
                    data: function (d) {
                        d.status = $('#status').val(),
                        d.visible = $('#visible').val(),
                        d.bentuk_serikat = $('#bentuk_serikat').val(),
                        d.status_serikat = $('#status_serikat').val(),
                        d.search = $('input[type="search"]').val()
                    }
                },
                columns: [
                { data: "nomor_pencatatan", name: "nomor_pencatatan" },
                { data: "bentuk_serikat", name: "bentuk_serikat" },
                { data: "nama_serikat", name: "nama_serikat" },
                { data: "tgl_pencatatan", name: "tgl_pencatatan" },
                { data: "status_serikat", name: "status_serikat" },
                { data: "afiliasi", name: "afiliasi" },
                { data: "status", name: "status" },
                { data: "visible", name: "visible" },
                ],
                columnDefs: [
                {
                    // Label
                    targets: 6,
                    render: function (data, type, full, meta) {
                        var $status_number = full['status'];
                        var $status = {
                            0: { title: 'Proses', class: ' bg-label-warning' },
                            1: { title: 'Selesai', class: ' bg-label-primary' },
                            2: { title: 'Tolak', class: ' bg-label-danger' },
                        };
                        
                        return (
                        '<span class="badge rounded-pill ' +
                        $status[$status_number].class +
                        '">' +
                        $status[$status_number].title +
                        '</span>'
                        );
                    }
                },
                {
                    // Label
                    targets: 7,
                    render: function (data, type, full, meta) {
                        var $status_number = full['visible'];
                        var $status = {
                            0: { title: 'Non Aktif', class: ' bg-label-danger' },
                            1: { title: 'Aktif', class: ' bg-label-success' },
                        };
                        
                        return (
                        '<span class="badge rounded-pill ' +
                        $status[$status_number].class +
                        '">' +
                        $status[$status_number].title +
                        '</span>'
                        );
                    }
                },
                ]
            });
            
            $('#bentuk_serikat').change(function(){
                table.draw();
            });
            $('#status_serikat').change(function(){
                table.draw();
            });
            $('#visible').change(function(){
                table.draw();
            });
            $('#status').change(function(){
                table.draw();
            });
            
        });
    </script>
    @endpush