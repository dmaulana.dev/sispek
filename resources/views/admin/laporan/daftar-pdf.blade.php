<!DOCTYPE html>

<html>
<title>Bukti Pencatatan SP/SB</title>
<head>
    
    @php
    $year = Carbon\Carbon::now()->format('Y');
    $date = Carbon\Carbon::now()->format('d');
    $dateM = Carbon\Carbon::now()->format('m');
    $days = Carbon\Carbon::now()->format('l');
    $month = Carbon\Carbon::now()->format('F');
    @endphp
    
    <style>
        #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        
        #customers td, #customers th {
            border: 1px solid #ecebeb;
            padding: 8px;
        }
        
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        
        #customers tr:hover {background-color: #ddd;}
        
        #customers th {
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: left;
            background-color: #717171;
            color: white;
            text-align: center;
            font-size: 13px;
        }
        
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        .table-collaps {
            padding: 10px;
            text-align: left;
            border: 1px solid black; 
            border-collapse: collapse;
        }
    </style>
    
    <body style="width: 100%">
        <div class="font12 font-arial-narrow" style="width: 100%; float: right; text-align: center;">
            <span>
                <b>SUKU DINAS TENAGA KERJA, TRANSMIGRASI DAN ENERGI</b>
                <br/>
                <b>KOTA ADMINISTRASI JAKARTA PUSAT</b>
                <br/>
                <b>DAFTAR SERIKAT PEKERJA/SERIKAT BURUH 
                    @if ($request == 'Konfederasi')
                    KONFEDERASI
                    @elseif ($request == 'Federasi')
                    FEDERASI
                    @else
                    @endif
                </b>
            </span>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div>
            <table id="customers">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No Pencatatan</th>
                        <th>Bentuk Serikat</th>
                        <th>Nama Serikat</th>
                        <th>Tgl Pencatatan</th>
                        <th>Status SPSB</th>
                        <th>Afiliasi</th>
                        <th>Status</th>
                        <th>Visible</th>
                    </tr>
                    @foreach ($data as $key => $row)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$row['nomor_pencatatan'] }}</td>
                        <td>{{$row['bentuk_serikat'] }}</td>
                        <td>{{$row['nama_serikat'] }}</td>
                        <td>{{$row['tgl_pencatatan'] }}</td>
                        <td>{{$row['status_serikat'] }}</td>
                        <td>{{$row['afiliasi'] }}</td>
                        <td>{{$row['status'] }}</td>
                        <td>{{$row['visible'] }}</td>
                    </tr>
                    @endforeach
                </thead>
            </table>
        </div>
        {{-- <div style="width: 10cm; float: left;"></div> --}}
        {{-- <div class="font13 font-arial" style="width: 100%; float: right; text-align: center;">
            <br/>
            
            <div style="width: 40%; float: right; text-align: center;">
                <p class="font12 font-arial">
                    Kepala Suku Dinas Tenaga Kerja,
                    Transmigrasi dan Energi
                    Kota Administrasi Jakarta Pusat,
                    
                </p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <p class="font12 font-arial">
                    Sudrajad, S.E., M.M.
                    <br/>
                    NIP. 196603261986031005
                </p>
            </div>
        </div> --}}
        
        
    </body>
</head>
</html>
