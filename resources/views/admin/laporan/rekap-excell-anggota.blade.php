    <style>
        #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        
        #customers td, #customers th {
            border: 1px solid #b0b0b0;
            padding: 8px;
            text-align: center;
        }
        
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        
        #customers tr:hover {background-color: #ddd;}
        
        #customers th {
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: left;
            background-color: #999999;
            color: white;
            text-align: center;
            font-size: 13px;
            border: 1px solid #838282
        }
        
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        .table-collaps {
            padding: 10px;
            text-align: left;
            border: 1px solid black; 
            border-collapse: collapse;
        }
    </style>
    
    
    <table id="customers">
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Jenis SP/SB</th>
            <th colspan="3">Jumlah Anggota</th>
            <th colspan="2">JUMLAH SERIKAT DI  PERUSAHAAN</th>
            <th colspan="2">JUMLAH SERIKAT DI DILUAR PERUSAHAAN</th>
        </tr>
        <tr>
            <th>S.D. TAHUN 2023</th>
            <th>Tahun 2024</th>
            <th>Total</th>
            
            <th>Aktif</th>
            <th>Non Aktif</th>
            
            <th>Aktif</th>
            <th>Non Aktif</th>
        </tr>
        @foreach ($data_rekapitulasi as $key => $item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td style="text-align:left">{{ $item['afiliasi'] }}</td>
            <td>{{ $item['tahun_2023'] }}</td>
            <td>{{ $item['tahun_2024'] }}</td>
            <td>{{ $item['jumlah'] }}</td>
            
            <td>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['aktif'] : 0 }}</td>
            <td>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['non_aktif'] : 0 }}</td>
            
            <td>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['aktif'] : 0 }}</td>
            <td>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['non_aktif'] : 0 }}</td>
        </tr>
        @endforeach
    </table>
    