@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Rekap Laporan SISPEK</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pencatatan.index')}}">Rekap Laporan Rekapitulasi SISPEK</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <br/>
                        <a href="{{ route('rekap.laporan.bentuk-serikat.excell') }}" class="btn-sm btn btn-dark">
                            <i class="mdi mdi-file-import me-0 me-sm-1" style="color: white"></i>
                            <span class="d-none d-sm-inline-block" style="color: white">Export Excell</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <br/>
                        <a href="{{ route('rekap.laporan.bentuk-serikat.pdf') }}" class="btn-sm btn btn-secondary">
                            <i class="mdi mdi-file-import me-0 me-sm-1" style="color: white"></i>
                            <span class="d-none d-sm-inline-block" style="color: white">Export PDF</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-datatable table-responsive">
            <table class="table table-bordered" style="text-align: center">
                <thead>
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">Jenis SP/SB</th>
                        <th rowspan="2">S.D. TAHUN 2023</th>
                        <th rowspan="2">TAHUN 2024</th>
                        <th rowspan="2">JUMLAH</th>
                        <th colspan="3">JUMLAH SERIKAT DI  PERUSAHAAN</th>
                        <th colspan="3">JUMLAH SERIKAT DI DILUAR PERUSAHAAN</th>
                        <th colspan="3">JUMLAH AKTIF</th>
                        <th colspan="3">JUMLAH NON-AKTIF</th>
                    </tr>
                    <tr>
                        <th>Mandiri</th>
                        <th>Afiliasi</th>
                        <th>Total</th>
                        
                        <th>Mandiri</th>
                        <th>Afiliasi</th>
                        <th>Total</th>
                        
                        <th>Mandiri</th>
                        <th>Afiliasi</th>
                        <th>Total</th>
                        
                        <th>Mandiri</th>
                        <th>Afiliasi</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $item)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <th>{{ $item['bentuk_serikat'] }}</th>
                        <th>{{ $item['tahun_2023'] }}</th>
                        <th>{{ $item['tahun_2024'] }}</th>
                        <th>{{ $item['jumlah'] }}</th>
                        
                        <th>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['mandiri'] : 0 }}</th>
                        <th>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['afiliasi'] : 0 }}</th>
                        <th>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['total'] : 0 }}</th>
                        
                        <th>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['mandiri'] : 0 }}</th>
                        <th>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['afiliasi'] : 0 }}</th>
                        <th>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['total'] : 0 }}</th>
                        
                        <th>{{ $item['mandiri_aktif'] }}</th>
                        <th>{{ $item['afiliasi_aktif'] }}</th>
                        <th>{{ $item['jumlah_aktif'] }}</th>
                        
                        <th>{{ $item['mandiri_nonaktif'] }}</th>
                        <th>{{ $item['afiliasi_nonaktif'] }}</th>
                        <th>{{ $item['jumlah_nonaktif'] }}</th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
    <br/>   
    <div class="card">
        <div class="card-header">
            <h3>Rekapitulasi Federasi dan Konfederasi</h3>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <br/>
                        <a href="{{ route('rekap.laporan.anggota.excell')}}" class="btn-sm btn btn-dark">
                            <i class="mdi mdi-file-import me-0 me-sm-1" style="color: white"></i>
                            <span class="d-none d-sm-inline-block" style="color: white">Export Excell</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <br/>
                        <a href="{{ route('rekap.laporan.anggota.pdf')}}" class="btn-sm btn btn-secondary">
                            <i class="mdi mdi-file-import me-0 me-sm-1" style="color: white"></i>
                            <span class="d-none d-sm-inline-block" style="color: white">Export PDF</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-datatable table-responsive">
            <table class="table table-bordered" style="text-align: center">
                <thead>
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">Jenis SP/SB</th>
                        <th colspan="3">Jumlah Anggota</th>
                        <th colspan="2">JUMLAH SERIKAT DI  PERUSAHAAN</th>
                        <th colspan="2">JUMLAH SERIKAT DI DILUAR PERUSAHAAN</th>
                    </tr>
                    <tr>
                        <th>S.D. TAHUN 2023</th>
                        <th>Tahun 2024</th>
                        <th>Total</th>
                        
                        <th>Aktif</th>
                        <th>Non Aktif</th>
                        
                        <th>Aktif</th>
                        <th>Non Aktif</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data_rekapitulasi as $key => $item)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <th style="text-align:left">{{ $item['afiliasi'] }}</th>
                        <th>{{ $item['tahun_2023'] }}</th>
                        <th>{{ $item['tahun_2024'] }}</th>
                        <th>{{ $item['jumlah'] }}</th>
                        
                        <th>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['aktif'] : 0 }}</th>
                        <th>{{ !empty($item['jenis_serikat_diperusaaan'][0]) ? $item['jenis_serikat_diperusaaan'][0]['non_aktif'] : 0 }}</th>
                        
                        <th>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['aktif'] : 0 }}</th>
                        <th>{{ !empty($item['jenis_serikat_diluar_perusaaan'][0]) ? $item['jenis_serikat_diluar_perusaaan'][0]['non_aktif'] : 0 }}</th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
    @endsection
    
    @push('custom-scripts')
    {{-- @include('admin.laporan.javascript') --}}
    
    @endpush