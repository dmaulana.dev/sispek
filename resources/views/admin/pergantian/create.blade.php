@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Pengganti Pencatatan SPSB</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pengganti.index')}}">Pengganti SPSB</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="row invoice-add">
        <!-- surat tugas Add-->
        <div class="col-lg-12 col-12 mb-lg-0 mb-4">
            <div class="card invoice-preview-card">
                <div class="card-body">
                    <h6>SURAT KETERANGAN PENGGANTI BUKTI PENCATATAN SP/SB</h6>
                    <br/>
                    <form action="{{ route('pengganti.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <input
                            class="form-control"
                            type="hidden"
                            value="{{ $data->id }}"
                            tabindex="0"
                            name="pencatatan_id"/>
                            
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="bentuk_serikat" tabindex="0" id="bentukSerikat" readonly>
                                        <option value="{{ $data->bentuk_serikat }}">{{ $data->bentuk_serikat }}</option>
                                    </select>
                                    <label for="Bentuk Serikat">Bentuk Serikat</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="nama panjang..."
                                    type="text"
                                    value="{{ $data->nomor_pencatatan }}"
                                    tabindex="0"
                                    name="nomor_pencatatan"
                                    readonly
                                    id="namaSerikat" />
                                    <label for="Jenis Serikat">Nomor Pencatatan</label>
                                </div>
                            </div>
                            <div class="col-sm-12 selectOpt">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="nama serikat..."
                                    type="text"
                                    value="{{ $data->nama_serikat }}"
                                    tabindex="0"
                                    name="nama_serikat"
                                    readonly />
                                    <label for="namaSerikat">Nama Serikat SP/SB</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Nomor Surat..."
                                    type="text"
                                    tabindex="0"
                                    name="no_surat"
                                    id="namaSerikat" />
                                    <label for="">Nomor Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Tanggal Surat..."
                                    type="date"
                                    tabindex="0"
                                    name="tgl_surat"
                                    id="tglSurat" />
                                    <label for="">Tanggal Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Perihal Surat..."
                                    type="text"
                                    tabindex="0"
                                    name="perihal"
                                    id="perihalSurat" />
                                    <label for="">Perihal Surat</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="No Surat Permohonan..."
                                    type="text"
                                    tabindex="0"
                                    name="no_surat_permohonan"
                                    id="noSuratPermohonan" />
                                    <label for="">No Surat Permohonan</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Tgl Surat Permohonan..."
                                    type="date"
                                    tabindex="0"
                                    name="tgl_surat_permohonan"
                                    id="tglSuratPermohonan" />
                                    <label for="">Tgl Surat Permohonan</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Surat Keterangan Hilang Dari..."
                                    type="text"
                                    name="no_surat_ket_hilang"
                                    tabindex="0"
                                    required/>
                                    <label for="">Nomor Surat Keterangan Hilang</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Surat Keterangan Hilang Dari..."
                                    type="text"
                                    name="surat_kehilangan_dari"
                                    tabindex="0"
                                    required/>
                                    <label for="Jenis Serikat">Surat Keterangan Hilang Dari</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Tgl Surat Kehilangan..."
                                    type="date"
                                    name="tgl_kehilangan"
                                    tabindex="0"
                                    required/>
                                    <label for="TglKehilangan">Tanggal Laporan Kehilangan</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    placeholder="Dokument Surat Kehilangan..."
                                    type="file"
                                    tabindex="0"
                                    name="dokument_kehilangan"
                                    id="dokKehilangan" />
                                    <label for="">Dokument Surat Kehilangan</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-floating form-floating-outline mb-3">
                                    <textarea
                                    class="form-control h-px-100"
                                    id="exampleFormControlTextarea1"
                                    name="keterangan_hilang"
                                    required
                                    placeholder="ketik disini..."></textarea>
                                    <label for="">Keterangan Hilang</label>
                                </div>
                            </div>
                            <div class="col-sm-2 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <select class="form-select" name="pilih_ttd" tabindex="0" id="pilihKepala">
                                        <option {{ $data->pilih_ttd == 'Kepala' ? 'selected' : '' }} value="Kepala">Kepala</option>
                                        <option {{ $data->pilih_ttd == 'PLH' ? 'selected' : '' }} value="PLH">PLH</option>
                                        <option {{ $data->pilih_ttd == 'PLT' ? 'selected' : '' }} value="PLT">PLT</option>
                                    </select>
                                    <label for="pilihKepala">Pilih Kepala</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="text"
                                    value="{{ $data->nama_ttd }}"
                                    name="nama_ttd"
                                    tabindex="0"
                                    id="tglPencatatan" />
                                    <label for="tglPencatatan">Nama TTD</label>
                                </div>
                            </div>
                            <div class="col-sm-5 mt-2">
                                <div class="form-floating form-floating-outline mb-3">
                                    <input
                                    class="form-control"
                                    type="number"
                                    value="{{ $data['nip_ttd'] }}"
                                    name="nip_ttd"
                                    tabindex="0"
                                    id="logo" />
                                    <label for="logo">NIP TTD</label>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-2">
                                <button type="sumbit" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;  Simpan </button>
                                <button type="button" onclick="history.back()" class="btn btn-danger"><i class="fa fa-minus-circle"></i>&nbsp;  Cancel </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @endsection
    
    @push('custom-scripts')
    
    @endpush