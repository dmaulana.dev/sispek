<script>
    
    $(function () {
        var dataTable = $(".datatables").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: `/pengganti`,
            },
            columns: [
            { data: "" },
            { data: "bentuk_serikat", name: "bentuk_serikat" },
            { data: "nomor_pencatatan", name: "nomor_pencatatan" },
            { data: "no_surat", name: "no_surat" },
            { data: "nama_serikat", name: "nama_serikat" },
            { data: "tgl_kehilangan", name: "tgl_kehilangan" },
            { data: "surat_ket_hilang", name: "surat_ket_hilang" },
            { data: "no_surat_ket_hilang", name: "no_surat_ket_hilang" },
            { data: "dokument_url", name: "dokument_url" },
            { data: "action", name: "action", orderable: false, searchable: false},
            ],
            columnDefs: [
            {
                className: "center",
                orderable: true,
                searchable: false,
                responsivePriority: 2,
                targets: 0,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },
            {
                className: "center",
                targets: 8,
                render: function (data, type, full, meta) {
                    if (full['dokument_url']) {
                        var res = '<a href="'+full['dokument_url']+'" target="_blank"><i class="mdi mdi mdi-download"></i></a>'
                    }else{
                        var res = '';
                    }
                    return res;
                },
            },
            ],
            order: [[0, "asc"]],
            dom:
            '<"row mx-1"' +
            '<"col-sm-12 col-md-3" l>' +
            '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
            ">t" +
            '<"row mx-2"' +
            '<"col-sm-12 col-md-6"i>' +
            '<"col-sm-12 col-md-6"p>' +
            ">",
            language: {
                sLengthMenu: "Show _MENU_",
                search: "Search",
                searchPlaceholder: "Search..",
            },
            // Buttons with Dropdown
            buttons: [],
        });
        
        
    });
    
</script>