<!DOCTYPE html>

<html>
<title>Bukti Pencatatan SP/SB</title>
<head>
    
    @php
    $year = Carbon\Carbon::now()->translatedFormat('Y');
    $date = Carbon\Carbon::now()->translatedFormat('d');
    $dateM = Carbon\Carbon::now()->translatedFormat('m');
    $days = Carbon\Carbon::now()->translatedFormat('l');
    $month = Carbon\Carbon::now()->translatedFormat('F');
    @endphp
    
    <style>
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        .table-collaps {
            padding: 10px;
            text-align: left;
            border: 1px solid black; 
            border-collapse: collapse;
        }
    </style>
    
    <body style="width: 95%">
        {{-- <div class="font12 font-arial-narrow" style="width: 100%; float: right; text-align: center;">
            <span>
                <b>PEMERINTAH PROVINSI DAERAH KHUSUS IBUKOTA JAKARTA</b>
                <br/>
                <b>DINAS PERINDUSTRIAN, PERDAGANGAN, KOPERASI, USAHA KECIL DAN MENENGAH</b>
                <br/>
                <span class="font-arial-narrow font12">
                    Jl. Perintis Kemerdekaan/ BGR I No. 3, Jakarta Utara
                    <br/>
                    Telp. 458 48014 – 458 48055 – 458 48011 – 453 4313 – 458 76685  Fax. 021-7814415
                    <br/>
                    Website http://disppkukm.jakarta.go.id/, Email : disppkukm@jakarta.go.id
                    <br/>
                    J A K A R T A
                </span>
            </span>
        </div> --}}
        {{-- <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div style="text-align: right; width:100%;" class="font11">
                <hr/>
            </div> --}}
            <style>
                hr {
                    border: none;
                    height: 1px;
                    color: #333; 
                    background-color: #333;
                }
            </style>
            <p class="font-arial font12" style="text-align: right; width:80%">{{ Carbon\Carbon::parse($log->tgl_surat)->translatedFormat('d F Y') }}</p>
            <div style="width: 65%; float: left;">
                <table class="font-arial font12" style="width: 65%;">
                    <tr>
                        <td>Nomor</td>
                        <td>:</td>
                        <td>{{ $log->no_surat }}</td>
                    </tr>
                    <tr>
                        <td>Sifat</td>
                        <td>:</td>
                        <td>Biasa</td>
                    </tr>
                    <tr>
                        <td>Lamp</td>
                        <td>:</td>
                        <td> - </td>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td>:</td>
                        <td>Surat Keterangan Pengganti</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td> </td>
                        <td>
                            Tanda Bukti Pencatatan
                            Serikat Pekerja/Serikat Buruh.
                        </td>
                    </tr>
                </table>
            </div>
            <div class="font13 font-arial" style="width: 35%; float: right; text-align: center;">
                <table class="font-arial font12">
                    <tr>
                        <td></td>
                        <td>Kepada</td>
                    </tr>
                    <tr>
                        <td>Yth.</td>
                        <td>Ketua Serikat Pekerja</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>{{$result['nama_serikat']}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            {{ $result['alamat']}}
                            <br/>
                            {{' Kel. '.$result['kelurahan'].' '. 'Kec. '.$result['kecamatan']}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>di-</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>{{ $result['wilayah'] }}</td>
                    </tr>
                </table>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div style="width: 10cm; float: left;"></div>
            <div class="font13 font-arial" style="width: 90%; float: right; text-align: center;">
                <p class="font12 font-arial" align="justify">
                    <span style="padding-left: 30px;">
                        Sehubungan dengan surat Saudara Nomor {{ $log->no_surat_permohonan }} tanggal {{ Carbon\Carbon::parse($log->tgl_surat_permohonan)->translatedFormat('d F Y') }} 
                        perihal {{ $log->perihal }}, 
                        disampaikan bahwa {{ $result['nama_serikat'] }} telah tercatat dengan 
                        Nomor Bukti Pencatatan {{ $result['nomor_pencatatan'] }} tanggal {{ Carbon\Carbon::parse($result['tgl_pencatatan'])->translatedFormat('d F Y') }}, 
                        dimana Nomor Bukti Pencatatan tersebut telah hilang dan telah dilaporkan ke {{ $log->surat_ket_hilang }}, dengan Surat Tanda Laporan Kehilangan/Kerusakan Barang/Surat-Surat 
                        Nomor {{ $log->no_surat_ket_hilang }} tanggal {{ Carbon\Carbon::parse($log->tgl_kehilangan)->translatedFormat('d F Y') }}. 
                        Bahwa sesuai Buku Pencatatan Serikat Pekerja / Serikat Buruh 
                        pada Suku Dinas Tenaga Kerja, Transmigrasi dan Energi Kota Administrasi Jakarta Pusat, 
                        telah tercatat {{ $result['nama_serikat'] }} dengan data sebagai berikut:
                    </span>
                </p>
                <table class="table">
                    <tr>
                        <td>a.</td>
                        <td>Nama Serikat Pekerja</td>
                        <td>:</td>
                        <td>{{ $result['nama_serikat'] }}</td>
                    </tr>
                    <tr>
                        <td>b.</td>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{$result['alamat'].', Kel. '.$result['kelurahan'].' '. 'Kec. '.$result['kecamatan']}} </td>
                    </tr>
                    <tr>
                        <td>c.</td>
                        <td>Nomor Bukti Pencatatan</td>
                        <td>:</td>
                        <td>{{$result['nomor_pencatatan']}}</td>
                    </tr>
                    <tr>
                        <td>d.</td>
                        <td>Tanggal Pencatatan</td>
                        <td>:</td>
                        <td>{{ Carbon\Carbon::parse($result['tgl_pencatatan'])->translatedFormat('d F Y') }}</td>
                    </tr>
                </table>
                <p class="font-arial font12" align="justify">
                    <span style="padding-left: 30px;">
                        Surat Keterangan ini sebagai pengganti tanda bukti pencatatan 
                        {{$result['nama_serikat']}} yang hilang. Namun apabila bukti pencatatan serikat pekerja/serikat buruh yang hilang ditemukan kembali, maka yang digunakan adalah bukti pencatatan tersebut.    
                    </span>
                </p>
                <p class="font-arial font12" align="justify">
                    Demikian agar Saudara maklum.
                    <br/>
                </p>
                <br/>
                
                <div style="width: 40%; float: right; text-align: center;">
                    <p class="font12 font-arial">
                        {{ $log->pilih_ttd != 'Kepala' ? $log->pilih_ttd.'.' : '' }} Kepala Suku Dinas Tenaga Kerja,
                        Transmigrasi dan Energi
                        Kota Administrasi Jakarta Pusat,
                        
                    </p>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <p class="font12 font-arial">
                        {{$log->nama_ttd}}
                        <br/>
                        NIP. {{$log->nip_ttd}}
                    </p>
                </div>
            </div>
            
            
        </body>
    </head>
    </html>
    