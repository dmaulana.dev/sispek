@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Penolakan Pencatatan SPSB</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pencatatan.penolakan.index')}}">Penolakan SPSB</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="row invoice-add">
        <!-- surat tugas Add-->
        <div class="row invoice-preview">
            <!-- Invoice -->
            <div class="col-xl-12 col-md-12 col-12 mb-md-0 mb-4">
                <div class="card invoice-preview-card">
                    <hr class="my-0" />
                    <div class="card-body">
                        <h5>Form Penolakan</h5>
                        <div class="row">
                            <div class="col-12">
                                <form action="{{ route('pencatatan.penolakan.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <select class="form-select" name="status" tabindex="0" id="status" disabled>
                                                    <option value="tolak">tolak pencatatan</option>
                                                </select>
                                                <label for="Penolakan">Penolakan</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="text"
                                                value=""
                                                name="no_surat"
                                                tabindex="0"
                                                id="NoSurat" />
                                                <label for="noSurat">No Surat</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="date"
                                                value=""
                                                name="tgl_surat"
                                                tabindex="0"
                                                id="tglSurat" />
                                                <label for="tglSurat">Tanggal Surat</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-4 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <select class="form-select" name="bentuk_serikat" tabindex="0" id="bentuk_serikat">
                                                    <option value="SP/SB">SP/SB</option>
                                                    <option value="Federasi">Federasi</option>
                                                    <option value="Konfederasi">Konfederasi</option>
                                                </select>
                                                <label for="bentuk_serikat">Bentuk Serikat</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-8 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="text"
                                                value=""
                                                name="nama_serikat"
                                                tabindex="0"
                                                id="namaSerikat" />
                                                <label for="namaSerikat">Nama Serikat</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <textarea
                                                class="form-control h-px-100"
                                                id="exampleFormControlTextarea1"
                                                name="alasan_penolakan"
                                                placeholder="ketik disini..."></textarea>
                                                <label for="alsaanPenolakan">Alasan Penolakan</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mt-2">
                                            <button type="sumbit" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;  Simpan </button>
                                            <button type="button" onclick="history.back()" class="btn btn-danger"><i class="fa fa-minus-circle"></i>&nbsp;  Cancel </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
            <!-- /Invoice -->
            
            
            <!-- /Invoice Actions -->
        </div>
    </div>
    
    @endsection
    
    @push('custom-scripts')
    
    
    @endpush