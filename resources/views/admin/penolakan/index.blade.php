@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Penolakan Pencatatan SPSB</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pencatatan.index')}}">Data Penolakan Pencatatan SPSB</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-10">
                    <h5 class="card-title">Data Penolakan</h5>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('pencatatan.penolakan.create') }}" class="btn btn-secondary btn-sm">
                        <i class="mdi mdi-plus me-0 me-sm-1"></i>
                        <span class="d-none d-sm-inline-block">Penolakan</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card-datatable table-responsive">
            <table class="datatables table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Bentuk Serikat</th>
                        <th>Nama Serikat</th>
                        <th>Tgl Penolakan</th>
                        <th>Alasan Penolakan</th>
                        <th>Dok</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    
    
    <div class="modal-onboarding modal fade animate__animated" id="uploadDokument" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content p-3 p-md-5">
                <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
                aria-label="Close"></button>
                <div class="modal-body p-md-0">
                    <div class="text-center mb-4">
                        <h3 class="mb-2 pb-1">Upload Dokument Penolakan</h3>
                        <p></p>
                    </div>
                    <form action="{{route('pencatatan.penolakan.upload')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-12 mb-3">
                            <div class="form-floating form-floating-outline">
                                <input type="hidden" id="idPencatatan" name="penolakan_id" class="form-control" value="" />
                                <input type="file" id="upload" name="dokument" class="form-control" placeholder="Upload Penolakan"/>
                                <label>Upload Dok Penolakan</label>
                            </div>
                        </div>
                        
                        <div class="col-12 text-center demo-vertical-spacing">
                            <button type="submit" class="btn btn-primary btn-submit me-sm-3 me-1">Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Tutup
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    @endsection
    
    @push('custom-scripts')
    @include('admin.penolakan.javascript')
    
    <script>
        $(document).ready(function() {
            $("body").on("click", ".upload-record", function() {
                const id = $(this).data("id");
                $("#idPencatatan").val(id);
                $("#uploadDokument").modal('show');
            });
        })
    </script>
    
    
    @endpush