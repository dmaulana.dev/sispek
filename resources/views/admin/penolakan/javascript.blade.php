<script>
    
    $(function () {
        var dataTable = $(".datatables").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: `/pencatatan/penolakan`,
            },
            columns: [
            { data: "" },
            // { data: "nomor_pencatatan", name: "nomor_pencatatan" },
            { data: "bentuk_serikat", name: "bentuk_serikat" },
            { data: "nama_serikat", name: "nama_serikat" },
            { data: "tgl_penolakan", name: "tgl_penolakan" },
            { data: "alasan_penolakan", name: "alasan_penolakan" },
            { data: "dokument_url", name: "dokument_url" },
            { data: "action", name: "action", orderable: false, searchable: false},
            ],
            columnDefs: [
            {
                className: "center",
                orderable: true,
                searchable: false,
                responsivePriority: 2,
                targets: 0,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },
            {
                className: "center",
                targets: 5,
                render: function (data, type, full, meta) {
                    if (full['dokument_url']) {
                        var res = '<a href="'+full['dokument_url']+'" target="_blank"><i class="mdi mdi mdi-download"></i></a>'
                    }else{
                        var res = '';
                    }
                    return res;
                },
            },
            ],
            order: [[0, "asc"]],
            dom:
            '<"row mx-1"' +
            '<"col-sm-12 col-md-3" l>' +
            '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
            ">t" +
            '<"row mx-2"' +
            '<"col-sm-12 col-md-6"i>' +
            '<"col-sm-12 col-md-6"p>' +
            ">",
            language: {
                sLengthMenu: "Show _MENU_",
                search: "Search",
                searchPlaceholder: "Search..",
            },
            // Buttons with Dropdown
            buttons: [],
        });
        
    });
    
</script>