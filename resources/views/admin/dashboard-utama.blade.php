<!DOCTYPE html>

<html lang="en" class="light-style layout-navbar-fixed layout-menu-fixed" dir="ltr" data-theme="theme-default"
data-assets-path="{{URL::asset('public/admin/assets')}}" data-template="vertical-menu-template">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
    content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    
    <title>Dashboard - Administrator</title>
    
    <meta name="description" content="" />
    
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="https://jshk-disnakertransgidkijakarta.com/assets_fe/images/logo-dki-small.png" />
    
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap"
    rel="stylesheet" />
    
    <!-- Icons -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/fonts/materialdesignicons.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/fonts/fontawesome.css')}}" />
    <!-- Menu waves for no-customizer fix -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/node-waves/node-waves.css')}}" />
    
    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/rtl/core.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/rtl/theme-default.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/demo.css')}}" />
    
    <!-- Vendors CSS -->
    <link rel="stylesheet"
    href="{{ URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/typeahead-js/typeahead.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/apex-charts/apex-charts.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/swiper/swiper.css')}}" />
    
    <!-- Page CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/pages/cards-statistics.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/pages/cards-analytics.css')}}" />
    <!-- Helpers -->
    <script src="{{ URL::asset('public/admin/assets/vendor/js/helpers.js')}}"></script>
    
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
        <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
        <script src="{{ URL::asset('public/admin/assets/vendor/js/template-customizer.js')}}"></script>
        <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
        <script src="{{ URL::asset('public/admin/assets/js/config.js')}}"></script>
    </head>
    
    <body>
        <!-- Layout wrapper -->
        <div class="layout-wrapper layout-content-navbar">
            <div class="layout-container">
                <!-- Menu -->
                @include('layouts.admin._menu-admin')
                <!-- / Menu -->
                
                <!-- Layout container -->
                <div class="layout-page">
                    <!-- Navbar -->
                    @include('layouts.admin._navbar-admin')
                    <!-- / Navbar -->
                    
                    <!-- Content wrapper -->
                    <div class="content-wrapper">
                        <style>
                            #spsb {
                                background: rgb(217,206,42);
                                background: linear-gradient(0deg, rgba(217,206,42,1) 19%, rgba(255,150,1,1) 100%);
                            }
                            #federasi {
                                background: rgb(195,34,34);
                                background: linear-gradient(0deg, rgba(195,34,34,1) 0%, rgba(253,142,45,1) 100%);
                            }
                            #konfederasi {
                                background: rgb(217,42,124);
                                background: linear-gradient(0deg, rgba(217,42,124,1) 19%, rgba(33,3,19,1) 100%);
                            }
                        </style>
                        
                        <div class="container-xxl flex-grow-1 container-p-y">
                            <div class="row gy-4 mb-4">
                                <!-- Sales Overview-->
                                <div class="col-lg-4">
                                    <div class="card h-100" id="spsb">
                                        <div class="card-header">
                                            <div class="d-flex justify-content-between">
                                                <h4 class="mb-2" style="color: white;">SP/SB</h4>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <small class="me-2" style="color: white;">Total dan status Pencatatan SP/SB</small>
                                                <div class="d-flex align-items-center text-success">
                                                    <p class="mb-0" style="color: white;">{{ $card_total['total_spsb'] }}</p>
                                                    <i class="mdi mdi-chevron-up"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body d-flex justify-content-between flex-wrap gap-3">
                                            <div class="d-flex gap-3">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-primary rounded">
                                                        <i class="mdi mdi-recycle mdi-24px"></i>
                                                    </div>
                                                </div>
                                                <div class="card-info">
                                                    <h4 class="mb-0" style="color: white;">{{ $card_total['total_spsb_proses'] }}</h4>
                                                    <small class="" style="color: rgb(28, 28, 28);">Proses</small>
                                                </div>
                                            </div>
                                            <div class="d-flex gap-3">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-warning rounded">
                                                        <i class="mdi mdi-checkbox-marked-circle-outline mdi-24px"></i>
                                                    </div>
                                                </div>
                                                <div class="card-info">
                                                    <h4 class="mb-0" style="color: white;">{{ $card_total['total_spsb_selesai'] }}</h4>
                                                    <small class="" style="color: rgb(28, 28, 28)">Selesai</small>
                                                </div>
                                            </div>
                                            <div class="d-flex gap-3">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-danger rounded">
                                                        <i class="mdi mdi-backspace mdi-24px"></i>
                                                    </div>
                                                </div>
                                                <div class="card-info">
                                                    <h4 class="mb-0" style="color: white;">0</h4>
                                                    <small class="" style="color: rgb(28, 28, 28)">Di Tolak</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card h-100" id="federasi">
                                        <div class="card-header">
                                            <div class="d-flex justify-content-between">
                                                <h4 class="mb-2" style="color: white;">Federasi</h4>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <small class="me-2" style="color: white;">Total dan status Pencatatan Federasi</small>
                                                <div class="d-flex align-items-center text-success">
                                                    <p class="mb-0" style="color: white;">{{ $card_total['total_federasi'] }}</p>
                                                    <i class="mdi mdi-chevron-up"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body d-flex justify-content-between flex-wrap gap-3">
                                            <div class="d-flex gap-3">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-primary rounded">
                                                        <i class="mdi mdi-recycle mdi-24px"></i>
                                                    </div>
                                                </div>
                                                <div class="card-info">
                                                    <h4 class="mb-0" style="color: white;">{{ $card_total['total_federasi_proses'] }}</h4>
                                                    <small class="" style="color: rgb(28, 28, 28);">Proses</small>
                                                </div>
                                            </div>
                                            <div class="d-flex gap-3">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-warning rounded">
                                                        <i class="mdi mdi-checkbox-marked-circle-outline mdi-24px"></i>
                                                    </div>
                                                </div>
                                                <div class="card-info">
                                                    <h4 class="mb-0" style="color: white;">{{ $card_total['total_federasi_selesai'] }}</h4>
                                                    <small class="" style="color: rgb(28, 28, 28)">Selesai</small>
                                                </div>
                                            </div>
                                            <div class="d-flex gap-3">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-danger rounded">
                                                        <i class="mdi mdi-backspace mdi-24px"></i>
                                                    </div>
                                                </div>
                                                <div class="card-info">
                                                    <h4 class="mb-0" style="color: white;">{{ $card_total['total_federasi_tolak'] }}</h4>
                                                    <small class="" style="color: rgb(28, 28, 28)">Di Tolak</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card h-100" id="konfederasi">
                                        <div class="card-header">
                                            <div class="d-flex justify-content-between">
                                                <h4 class="mb-2" style="color: white;">Konfederasi</h4>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <small class="me-2" style="color: white;">Total dan status Pencatatan Konfederasi</small>
                                                <div class="d-flex align-items-center text-success">
                                                    <p class="mb-0" style="color: white;">{{ $card_total['total_konfederasi'] }}</p>
                                                    <i class="mdi mdi-chevron-up"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body d-flex justify-content-between flex-wrap gap-3">
                                            <div class="d-flex gap-3">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-primary rounded">
                                                        <i class="mdi mdi-recycle mdi-24px"></i>
                                                    </div>
                                                </div>
                                                <div class="card-info">
                                                    <h4 class="mb-0" style="color: white;">{{ $card_total['total_konfederasi_proses'] }}</h4>
                                                    <small class="" style="color: rgb(28, 28, 28);">Proses</small>
                                                </div>
                                            </div>
                                            <div class="d-flex gap-3">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-warning rounded">
                                                        <i class="mdi mdi-checkbox-marked-circle-outline mdi-24px"></i>
                                                    </div>
                                                </div>
                                                <div class="card-info">
                                                    <h4 class="mb-0" style="color: white;">{{ $card_total['total_konfederasi_selesai'] }}</h4>
                                                    <small class="" style="color: rgb(28, 28, 28)">Selesai</small>
                                                </div>
                                            </div>
                                            <div class="d-flex gap-3">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-danger rounded">
                                                        <i class="mdi mdi-backspace mdi-24px"></i>
                                                    </div>
                                                </div>
                                                <div class="card-info">
                                                    <h4 class="mb-0" style="color: white;">{{ $card_total['total_konfederasi_tolak'] }}</h4>
                                                    <small class="" style="color: rgb(28, 28, 28)">Di Tolak</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-xl-12 col-12 mb-4">
                                    <div class="card">
                                        <div class="card-header header-elements">
                                            <h5 class="card-title mb-0">Laporan Pencatatan Serikat Pekerja / Serikat Buruh</h5>
                                        </div>
                                        <div class="card-body">
                                            <div id="barTahun" data-height="400"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-12 mb-4">
                                    <div class="card">
                                        <div class="card-header header-elements">
                                            <h5 class="card-title mb-0">Laporan Pencatatan SP/SB Per Kecamatan</h5>
                                        </div>
                                        <div class="card-body">
                                            <canvas id="barKecamatan" data-height="400"></canvas>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-12 col-lg-12">
                                    <div class="card h-100">
                                        <div class="card-header d-flex justify-content-between">
                                            <div class="card-title m-0">
                                                <h5 class="mb-0">10 Data Terbaru Pencatatan SP/SB</h5>
                                                {{-- <small class="text-muted">82% Activity Growth</small> --}}
                                            </div>
                                        </div>
                                        <div class="card-body pb-1">
                                            <ul class="nav nav-tabs nav-tabs-widget pb-3 gap-4 mx-1 d-flex flex-nowrap" role="tablist">
                                                <li class="nav-item">
                                                    <a
                                                    href="javascript:void(0);"
                                                    class="nav-link btn active d-flex flex-column align-items-center justify-content-center"
                                                    role="tab"
                                                    data-bs-toggle="tab"
                                                    data-bs-target="#navs-orders-id"
                                                    aria-controls="navs-orders-id"
                                                    aria-selected="true">
                                                    <div class="avatar">
                                                        <div class="avatar-initial bg-label-secondary rounded">
                                                            <i class="mdi mdi-cellphone"></i>
                                                        </div>
                                                    </div>
                                                    <small>SP/SB</small>
                                                </a>
                                                
                                            </li>
                                            <li class="nav-item">
                                                <a
                                                href="javascript:void(0);"
                                                class="nav-link btn d-flex flex-column align-items-center justify-content-center"
                                                role="tab"
                                                data-bs-toggle="tab"
                                                data-bs-target="#navs-sales-id"
                                                aria-controls="navs-sales-id"
                                                aria-selected="false">
                                                <div class="avatar">
                                                    <div class="avatar-initial bg-label-secondary rounded">
                                                        <i class="mdi mdi-television"></i>
                                                    </div>
                                                </div>
                                                <small>Federasi</small>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a
                                            href="javascript:void(0);"
                                            class="nav-link btn d-flex flex-column align-items-center justify-content-center"
                                            role="tab"
                                            data-bs-toggle="tab"
                                            data-bs-target="#navs-profit-id"
                                            aria-controls="navs-profit-id"
                                            aria-selected="false">
                                            <div class="avatar">
                                                <div class="avatar-initial bg-label-secondary rounded">
                                                    <i class="mdi mdi-gamepad-circle-outline"></i>
                                                </div>
                                            </div>
                                            <small>Konfederasi</small>
                                        </a>
                                    </li>
                                </ul>
                                
                                <div class="tab-content p-0 ms-0 ms-sm-2">
                                    <div class="tab-pane fade show active" id="navs-orders-id" role="tabpanel">
                                        <div class="table-responsive text-nowrap">
                                            <table class="table table-border">
                                                <thead>
                                                    <tr>
                                                        <th class="ps-0 fw-medium text-heading">No Pencatatan</th>
                                                        <th class="fw-medium ps-0 text-heading">Nama Serikat</th>
                                                        <th class="pe-0 text-end fw-medium text-heading">Jenis Serikat</th>
                                                        <th class="pe-0 fw-medium text-end text-heading">Afiliasi</th>
                                                        <th class="pe-0 fw-medium text-end text-heading">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($spsb as $row)
                                                    <tr>
                                                        <td class="ps-0">{{ $row->nomor_pencatatan }}</td>
                                                        <td class="text-heading fw-semibold ps-0">{{ $row->nama_serikat }}</td>
                                                        <td class="text-heading text-end">{{ $row->jenis_serikat }}</td>
                                                        <td class="pe-0 text-end fw-semibold h6">{{ $row->afiliasi }}</td>
                                                        <td class="text-heading text-end pe-0">
                                                            @if ($row->status == 0)
                                                            <span class="badge rounded-pill bg-label-warning">Proses</span>
                                                            @elseif ($row->status == 1)
                                                            <span class="badge rounded-pill bg-label-success">Selesai</span>
                                                            @elseif ($row->status == 2)
                                                            <span class="badge rounded-pill bg-label-danger">Ditolak</span>
                                                            @else
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                                    <div class="tab-pane fade" id="navs-sales-id" role="tabpanel">
                                        <div class="table-responsive text-nowrap">
                                            <table class="table table-borderless">
                                                <thead>
                                                    <tr>
                                                        <th class="ps-0 fw-medium text-heading">No Pencatatan</th>
                                                        <th class="fw-medium ps-0 text-heading">Nama Serikat</th>
                                                        <th class="pe-0 text-end fw-medium text-heading">Jenis Serikat</th>
                                                        <th class="pe-0 fw-medium text-end text-heading">Afiliasi</th>
                                                        <th class="pe-0 fw-medium text-end text-heading">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($federasi as $row)
                                                    <tr>
                                                        <td class="ps-0">{{ $row->nomor_pencatatan }}</td>
                                                        <td class="text-heading fw-semibold ps-0">{{ $row->nama_serikat }}</td>
                                                        <td class="text-heading text-end">{{ $row->jenis_serikat }}</td>
                                                        <td class="pe-0 text-end fw-semibold h6">{{ $row->afiliasi }}</td>
                                                        <td class="text-heading text-end pe-0">
                                                            @if ($row->status == 0)
                                                            <span class="badge rounded-pill bg-label-warning">Proses</span>
                                                            @elseif ($row->status == 1)
                                                            <span class="badge rounded-pill bg-label-success">Selesai</span>
                                                            @elseif ($row->status == 2)
                                                            <span class="badge rounded-pill bg-label-danger">Ditolak</span>
                                                            @else
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="navs-profit-id" role="tabpanel">
                                        <div class="table-responsive text-nowrap">
                                            <table class="table table-borderless">
                                                <thead>
                                                    <tr>
                                                        <th class="ps-0 fw-medium text-heading">No Pencatatan</th>
                                                        <th class="fw-medium ps-0 text-heading">Nama Serikat</th>
                                                        <th class="pe-0 text-end fw-medium text-heading">Jenis Serikat</th>
                                                        <th class="pe-0 fw-medium text-end text-heading">Afiliasi</th>
                                                        <th class="pe-0 fw-medium text-end text-heading">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($konfederasi as $row)
                                                    <tr>
                                                        <td class="ps-0">{{ $row->nomor_pencatatan }}</td>
                                                        <td class="text-heading fw-semibold ps-0">{{ $row->nama_serikat }}</td>
                                                        <td class="text-heading text-end">{{ $row->jenis_serikat }}</td>
                                                        <td class="pe-0 text-end fw-semibold h6">{{ $row->afiliasi }}</td>
                                                        <td class="text-heading text-end pe-0">
                                                            @if ($row->status == 0)
                                                            <span class="badge rounded-pill bg-label-warning">Proses</span>
                                                            @elseif ($row->status == 1)
                                                            <span class="badge rounded-pill bg-label-success">Selesai</span>
                                                            @elseif ($row->status == 2)
                                                            <span class="badge rounded-pill bg-label-danger">Ditolak</span>
                                                            @else
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                
            </div>
            
            
        </div>
    </div>
</div>

</div>

<!--/ Live Visitors-->
</div>
<!--/ visits By Day Chart-->
</div>
</div>

</div>
<!--/ Project Statistics -->
</div>
</div>
<!-- / Content -->

<!-- Footer -->
@include('layouts.admin._footer-admin')
<!-- / Footer -->

<div class="content-backdrop fade"></div>
</div>
<!-- Content wrapper -->
</div>

<!-- / Layout page -->
</div>

<!-- Overlay -->
<div class="layout-overlay layout-menu-toggle"></div>

<!-- Drag Target Area To SlideIn Menu On Small Screens -->
<div class="drag-target"></div>
</div>
<!-- / Layout wrapper -->

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script>
    var ctx = document.getElementById('barKecamatan').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: {!! json_encode($bar_kecamatan['kecamatan'] )!!},
            datasets: [
            {
                label: 'Data',
                data: {!! json_encode($bar_kecamatan['jumlah'] )!!},
                backgroundColor: 'rgba(229, 184, 4, 0.8)',
                borderColor: 'rgba(188, 151, 0, 0.8)',
                borderWidth: 1
            }
            ]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>
    Highcharts.chart('barTahun', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Diagram SP/SB Per Tahun',
            align: 'center'
        },
        xAxis: {
            categories: {!! json_encode($result['tahun'] )!!},
            crosshair: true,
            accessibility: {
                description: 'Countries'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Data'
            }
        },
        tooltip: {
            valueSuffix: ' (Data)'
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
        {
            name: 'SP/SB',
            data: {!! json_encode($result['spsb'] )!!}
        },
        {
            name: 'Federasi',
            data: {!! json_encode($result['federasi'] )!!}
        },
        {
            name: 'Konfederasi',
            data: {!! json_encode($result['konfederasi'] )!!}
        }
        ]
    });
</script>
<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="{{ URL::asset('public/admin/assets/vendor/libs/jquery/jquery.js')}}"></script>
<script src="{{ URL::asset('public/admin/assets/vendor/libs/popper/popper.js')}}"></script>
<script src="{{ URL::asset('public/admin/assets/vendor/js/bootstrap.js')}}"></script>
<script src="{{ URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
<script src="{{ URL::asset('public/admin/assets/vendor/libs/node-waves/node-waves.js')}}"></script>

<script src="{{ URL::asset('public/admin/assets/vendor/libs/hammer/hammer.js')}}"></script>
<script src="{{ URL::asset('public/admin/assets/vendor/libs/i18n/i18n.js')}}"></script>
<script src="{{ URL::asset('public/admin/assets/vendor/libs/typeahead-js/typeahead.js')}}"></script>

<script src="{{ URL::asset('public/admin/assets/vendor/js/menu.js')}}"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="{{ URL::asset('public/admin/assets/vendor/libs/apex-charts/apexcharts.js')}}"></script>
<script src="{{ URL::asset('public/admin/assets/vendor/libs/swiper/swiper.js')}}"></script>

<!-- Main JS -->
<script src="{{ URL::asset('public/admin/assets/js/main.js')}}"></script>

<!-- Page JS -->
<script src="{{ URL::asset('public/admin/assets/js/dashboards-analytics.js')}}"></script>

<!-- Vendors JS -->
<script src="{{ URL::asset('public/admin/assets/vendor/libs/chartjs/chartjs.js')}}"></script>
<!-- Page JS -->
<script src="{{ URL::asset('public/admin/assets/js/charts-chartjs.js')}}"></script>

</body>

</html>
