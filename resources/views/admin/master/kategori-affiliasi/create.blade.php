<div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content p-3 p-md-5">
        <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
        aria-label="Close"></button>
        <div class="modal-body p-md-0">
            <div class="text-center mb-4">
                <h3 class="mb-2 pb-1">Tambah Kategori Affiliasi</h3>
                <p>Tambah Data Kategori Affiliasi</p>
            </div>
            <form action="{{route('master-kategori-affiliasi.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                
                <div class="col-12 mb-3">
                    <div class="form-floating form-floating-outline">
                        <input type="text" id="nama" name="nama"
                        class="form-control" placeholder="Nama Affiliasi"/>
                        <label>Nama Affiliasi</label>
                    </div>
                </div>
                
                <div class="col-12 mb-3">
                    <div class="form-floating form-floating-outline mt-3">
                        <select class="form-control" id="status" name="status">
                            <option>Pilih</option>
                            <option value="1">Active</option>
                            <option value="2">Non Active</option>
                        </select>
                        <label>Status</label>
                    </div>
                </div>
                
                <div class="col-12 mb-3">
                    <div class="form-floating form-floating-outline mt-3">
                        <textarea rows="4" type="text" id="keterangan" name="keterangan" class="form-control" placeholder=""></textarea>
                        <label>Keterangan</label>
                    </div>
                </div>
                
                <div class="col-12 text-center demo-vertical-spacing">
                    <button type="submit" class="btn btn-primary btn-submit me-sm-3 me-1">Simpan</button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                        Tutup
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>