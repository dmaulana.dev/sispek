@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Master Kategori Affiliasi</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('master-kategori-affiliasi.index')}}">Master Data Kategori Affiliasi</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="card">
        <div class="card-datatable table-responsive">
            <table class="datatables table">
                <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Keterangan</th>
                        <th>Status</th>
                        <th>Created By</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    <div class="modal fade" id="addKategoriAffiliasiModal" tabindex="-1" aria-hidden="true">
        @include('admin.master.kategori-affiliasi.create')
    </div>
    
    <div class="modal fade" id="editKategoriAffiliasiModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" id="modalContent">
        </div>
    </div>
    
    @endsection
    
    @push('custom-scripts')
    @include('admin.master.kategori-affiliasi.javascript')
    
    
    @endpush