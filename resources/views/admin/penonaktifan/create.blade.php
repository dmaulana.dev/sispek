@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Penonaktifan Pencatatan SPSB</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('penonaktifan.index')}}">Penonaktifan SPSB</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="row invoice-add">
        <!-- surat tugas Add-->
        <div class="row invoice-preview">
            <!-- Invoice -->
            <div class="col-xl-12 col-md-12 col-12 mb-md-0 mb-4">
                <div class="card invoice-preview-card">
                    <hr class="my-0" />
                    <div class="card-body">
                        <h3>{{ $data->nama_serikat }}</h3>
                        <span>{{ $data->nama_singkat }}</span>
                        <div class="d-flex justify-content-between flex-wrap">
                            <div class="my-3">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="pe-3 fw-medium">No Pencatatan</td>
                                            <td>{{ $data->nomor_pencatatan }}</td>
                                        </tr>
                                        <tr>
                                            <td class="pe-3 fw-medium">Tgl Pencatatan</td>
                                            <td>{{ $data->tgl_pencatatan }}</td>
                                        </tr>
                                        <tr>
                                            <td class="pe-3 fw-medium">Bentuk Serikat</td>
                                            <td>{{ $data->bentuk_serikat }}</td>
                                        </tr>
                                        <tr>
                                            <td class="pe-3 fw-medium">Jenis Serikat</td>
                                            <td>{{ $data->jenis_serikat }}</td>
                                        </tr>
                                        <tr>
                                            <td class="pe-3 fw-medium">Status Serikat</td>
                                            <td>{{$data->status_serikat}}</td>
                                        </tr>
                                        <tr>
                                            <td class="pe-3 fw-medium">Afiliasi</td>
                                            <td>{{ $data->afiliasi}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="my-3">
                                <h6 class="pb-2">Perusahaan</h6>
                                @foreach ($perusahaan as $key => $item)
                                <p class="mb-1"><span> {{$key+1}}. </span>  {{ $item->nama_perusahaan }}</p>
                                @endforeach
                            </div>
                            <div class="my-3">
                                <tr>
                                    <td class="pe-3 fw-medium">
                                        {{-- <img src="{{ $data->logo_url }}" width="20%"> --}}
                                    </td>
                                </tr>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table m-0">
                            <p style="padding: 15px;"><b>Susunan Pengurus</b></p>
                            <thead class="table-light border-top">
                                <tr>
                                    <th>#</th>
                                    <th>Jabatan</th>
                                    <th>Nama</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($susunan as $item)
                                <tr>
                                    <td class="text-nowrap">{{ $key + 1 }}</td>
                                    <td class="text-nowrap">{{ $item->jabatan }}</td>
                                    <td class="text-nowrap">{{ $item->nama }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br/>
                    <div class="card-body">
                        <h5>Form Penonaktifan</h5>
                        <div class="row">
                            <div class="col-12">
                                <form action="{{ route('penonaktifan.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="pencatatan_id" value="{{ $data->id }}">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <select class="form-select" name="status_alasan" tabindex="0" id="pilihPenonakifan">
                                                    <option value="Pindah Domisili">Pindah Domisili</option>
                                                    <option value="Perusahaan Tutup">Perusahaan Tutup</option>
                                                    <option value="Dibubarkan Oleh Anggota">Dibubarkan Oleh Anggota</option>
                                                    <option value="Putusan Pengadilan">Putusan Pengadilan</option>
                                                    <option value="Lain Lain">Lain Lain</option>
                                                </select>
                                                <label for="Penonaktifan">Penonaktifan</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-6 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="text"
                                                value=""
                                                name="no_surat"
                                                tabindex="0"
                                                id="NoSurat" />
                                                <label for="noSurat">No Surat</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-6 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="date"
                                                value=""
                                                name="tgl_surat"
                                                tabindex="0"
                                                id="TglSurat" />
                                                <label for="noSurat">Tanggal Surat</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-3 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <select class="form-select" name="status_alasan" tabindex="0" id="pilihPenonakifan">
                                                    @foreach ($wilayah as $item)
                                                    <option value="{{ $item['wilayah'] }}">{{ $item['wilayah'] }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="pindahKE">Pindah KE</label>
                                            </div>
                                        </div>

                                        <div class="col-sm-3 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="text"
                                                value=""
                                                name="no_surat_permohonan"
                                                tabindex="0"
                                                id="NoSuratPermohonan" />
                                                <label for="noSuratPermohonan">No Surat Permohonan</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-3 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="date"
                                                value=""
                                                name="tgl_surat_permohonan"
                                                tabindex="0"
                                                id="tglSuratPermohonan" />
                                                <label for="tglSuratPermohonan">Tgl Surat Permohonan</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-3 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="file"
                                                value=""
                                                name="dok_surat_permohonan"
                                                tabindex="0"
                                                id="dokSuratPermohonan" />
                                                <label for="dokSuratPermohonan">Dok Surat Permohonan</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <textarea
                                                class="form-control h-px-100"
                                                id="exampleFormControlTextarea1"
                                                name="keterangan"
                                                placeholder="ketik disini..."></textarea>
                                                <label for="alsaanPenolakan">Keterangan Penonaktifan</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <select class="form-select" name="pilih_ttd" tabindex="0" id="pilihKepala">
                                                    <option {{ $data->pilih_ttd == 'Kepala' ? 'selected' : '' }} value="Kepala">Kepala</option>
                                                    <option {{ $data->pilih_ttd == 'PLH' ? 'selected' : '' }} value="PLH">PLH</option>
                                                    <option {{ $data->pilih_ttd == 'PLT' ? 'selected' : '' }} value="PLT">PLT</option>
                                                </select>
                                                <label for="pilihKepala">Pilih Kepala</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="text"
                                                value="{{ $data->nama_ttd }}"
                                                name="nama_ttd"
                                                tabindex="0"
                                                id="tglPencatatan" />
                                                <label for="tglPencatatan">Nama TTD</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 mt-2">
                                            <div class="form-floating form-floating-outline mb-3">
                                                <input
                                                class="form-control"
                                                type="number"
                                                value="{{ $data['nip_ttd'] }}"
                                                name="nip_ttd"
                                                tabindex="0"
                                                id="logo" />
                                                <label for="logo">NIP TTD</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mt-2">
                                            <button type="sumbit" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;  Simpan </button>
                                            <button type="button" onclick="history.back()" class="btn btn-danger"><i class="fa fa-minus-circle"></i>&nbsp;  Cancel </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
            <!-- /Invoice -->
            
            
            <!-- /Invoice Actions -->
        </div>
    </div>
    
    @endsection
    
    @push('custom-scripts')
    
    
    @endpush