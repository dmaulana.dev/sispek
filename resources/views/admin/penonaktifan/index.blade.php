@extends('layouts.admin._master-admin')
@section('content')

<div class="d-grid gap-3">
    <div class="card shadow-sm">
        <div class="card-body">
            <h4>Penonaktifan Pencatatan SPSB</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">
                        <a href="{{ route('dashboard.admin')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="{{ route('pencatatan.index')}}">Data Penonaktifan Pencatatan SPSB</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-10">
                    <h5 class="card-title">Data Penonaktifan</h5>
                </div>
                <div class="col-md-2">
                    <a type="button" class="btn-sm btn btn-light" id="tambahPenonaktifan">
                        <i class="mdi mdi-plus me-0 me-sm-1"></i>
                        <span class="d-none d-sm-inline-block">Penonaktifan</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card-datatable table-responsive">
            <table class="datatables table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomor Pencatatan</th>
                        <th>Nomor Surat</th>
                        <th>Tgl Penonaktifan</th>
                        <th>Alasan</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    <div class="modal fade" id="modalDataPencatatan" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered modal-simple modal-upgrade-plan">
            <div class="modal-content p-3 p-md-5">
                <div class="modal-body p-1">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h3 class="mb-2 pb-1">Data Pencatatan</h3>
                        <p>Cari Data Pencatatan yang mau di rubah..</p>
                    </div>
                    <div class="card-datatable table-responsive pt-0">
                        <table class="datatables-data-pencatatan table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No Pencatatan</th>
                                    <th>Bentuk Serikat</th>
                                    <th>Nama Serikat</th>
                                    <th>Tgl Pencatatan</th>
                                    <th>Status SPSB</th>
                                    <th>Afiliasi</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal-onboarding modal fade animate__animated" id="uploadDokument" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content p-3 p-md-5">
                <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
                aria-label="Close"></button>
                <div class="modal-body p-md-0">
                    <div class="text-center mb-4">
                        <h3 class="mb-2 pb-1">Upload Dokument Penolakan</h3>
                        <p></p>
                    </div>
                    <form action="{{route('pencatatan.penolakan.upload')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-12 mb-3">
                            <div class="form-floating form-floating-outline">
                                <input type="hidden" id="idPencatatan" name="penolakan_id" class="form-control" value="" />
                                <input type="file" id="upload" name="dokument" class="form-control" placeholder="Upload Penolakan"/>
                                <label>Upload Dok Penolakan</label>
                            </div>
                        </div>
                        
                        <div class="col-12 text-center demo-vertical-spacing">
                            <button type="submit" class="btn btn-primary btn-submit me-sm-3 me-1">Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Tutup
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    @endsection
    
    @push('custom-scripts')
    @include('admin.penonaktifan.javascript')
    
    <script>
        $(document).ready(function() {
            $("body").on("click", ".upload-record", function() {
                const id = $(this).data("id");
                $("#idPencatatan").val(id);
                $("#uploadDokument").modal('show');
            });
        })
    </script>
    
    <script>
        $('body').on('click', '#tambahPenonaktifan', function () {
            var id = $(this).data('id');
            $("#modalDataPencatatan").modal('show');
            var table = $('.datatables-data-pencatatan').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: `/pencatatan/penonaktifan/show`,
                },
                columns: [
                { data: "" },
                { data: "nomor_pencatatan", name: "nomor_pencatatan" },
                { data: "bentuk_serikat", name: "bentuk_serikat" },
                { data: "nama_serikat", name: "nama_serikat" },
                { data: "tgl_pencatatan", name: "tgl_pencatatan" },
                { data: "status_serikat", name: "status_serikat" },
                { data: "afiliasi", name: "afiliasi" },
                { data: "status", name: "status" },
                { data: "action", name: "action", orderable: false, searchable: false},
                ],
                columnDefs: [
                {
                    className: "center",
                    orderable: true,
                    searchable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    // Label
                    targets: 7,
                    render: function (data, type, full, meta) {
                        var $status_number = full['status'];
                        var $status = {
                            0: { title: 'Proses', class: ' bg-label-warning' },
                            1: { title: 'Selesai', class: ' bg-label-primary' },
                            2: { title: 'Tolak', class: ' bg-label-danger' },
                        };
                        
                        return (
                        '<span class="badge rounded-pill ' +
                        $status[$status_number].class +
                        '">' +
                        $status[$status_number].title +
                        '</span>'
                        );
                    }
                },
                ],
                order: [[0, "asc"]],
            });
        });
        
        $('#modalDataPencatatan').on('hidden.bs.modal', function (e) {
            location.reload('#modalDataPencatatan');
        });
    </script>
    
    @endpush