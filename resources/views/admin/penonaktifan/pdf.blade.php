<!DOCTYPE html>

<html>
<title>Bukti Pencatatan SP/SB</title>
<head>
    
    @php
    $year = Carbon\Carbon::now()->translatedFormat('Y');
    $date = Carbon\Carbon::now()->translatedFormat('d');
    $dateM = Carbon\Carbon::now()->translatedFormat('m');
    $days = Carbon\Carbon::now()->translatedFormat('l');
    $month = Carbon\Carbon::now()->translatedFormat('F');
    @endphp
    
    <style>
        .font13 {
            font-size:14px;
        }
        
        .font11 {
            text-align: center; 
            font-size:12px;
            font-weight: normal;
        }
        .font12 {
            font-size:13px;
            font-weight: normal;
        }
        .font-arial-narrow {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-arial {
            font-family: 'Arial Narrow', Arial, sans-serif;
        }
        
        .font-times {
            font-family: "Times New Roman", Times, serif;
        }
        
        .table-collaps {
            padding: 10px;
            text-align: left;
            border: 1px solid black; 
            border-collapse: collapse;
        },
        ol {
            /* background: #ff9999; */
            padding: 0px;
        }
        
        ul {
            /* background: #3399ff; */
            padding: 0px;
        }
        
        ol li {
            padding: 3px;
            margin-left: 15px;
        }
        
        ul li {
            /* background: #cce5ff; */
            /* color: darkblue; */
            margin: 5px;
        }
    </style>
    
    <body style="width: 95%">
        <style>
            hr {
                border: none;
                height: 1px;
                color: #333; 
                background-color: #333;
            }
        </style>
        <p class="font-arial font12" style="text-align: right; width:80%">{{ Carbon\Carbon::parse($log->tgl_surat)->translatedFormat('d F Y') }}</p>
        <div style="width: 65%; float: left;">
            <table class="font-arial font12" style="width: 65%;">
                <tr>
                    <td>Nomor</td>
                    <td>:</td>
                    <td>{{ $log->no_surat }}</td>
                </tr>
                <tr>
                    <td>Sifat</td>
                    <td>:</td>
                    <td>Biasa</td>
                </tr>
                <tr>
                    <td>Lamp</td>
                    <td>:</td>
                    <td> - </td>
                </tr>
                <tr>
                    <td>Perihal</td>
                    <td>:</td>
                    <td>Pencabutan Nomor Bukti</td>
                </tr>
                <tr>
                    <td></td>
                    <td> </td>
                    <td>
                        Pencatatan Serikat Pekerja/Serikat Buruh 
                        di Perusahaan Karena Pindah Domisili/Alamat
                    </td>
                </tr>
            </table>
        </div>
        <div class="font13 font-arial" style="width: 35%; float: right; text-align: center;">
            <table class="font-arial font12">
                <tr>
                    <td></td>
                    <td>Kepada</td>
                </tr>
                <tr>
                    <td>Yth.</td>
                    <td>Ketua Serikat Pekerja</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{$result['nama_serikat']}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        {{ $result['alamat']}}
                        <br/>
                        {{' Kel. '.$result['kelurahan'].' '. 'Kec. '.$result['kecamatan']}}
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>di-</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $result['wilayah'] }}</td>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div style="width: 10cm; float: left;"></div>
        <div class="font13 font-arial" style="width: 90%; float: right;">
            <p class="font12 font-arial" align="justify">
                <span style="padding-left: 30px;">
                    Sehubungan dengan surat Pengurus Serikat Pekerja 
                    {{ $result['nama_serikat'] }} Nomor {{ $log['no_surat_permohonan'] }} tanggal {{ Carbon\Carbon::parse($log->tgl_surat_permohonan)->translatedFormat('d F Y') }}
                    perihal Pemberitahuan {{$log->status_alasan}} @if ($log->status_alasan == 'Pindah Domisili') / Alamat @else @endif dan Permohonan Pencabutan Nomor Bukti Pencatatan Serikat Pekerja, 
                    dengan ini kami sampaikan hal-hal sebagai berikut :
                </span>
            </p>
            <ol type="I" class="font12 font-arial" align="justify">
                <li>Bahwa pemberitahuan {{$log->status_alasan}} @if ($log->status_alasan == 'Pindah Domisili') / Alamat @else @endif dan permohonan pencabutan nomor bukti 
                    pencatatan {{ $result['nama_serikat'] }} telah memenuhi ketentuan Pasal 17 ayat (1) huruf a 
                    Peraturan Gubernur Provinsi DKI Jakarta Nomor 10 Tahun 2007, 
                    @if ($log->status_alasan == 'Pindah Domisili') yaitu pindah ke wilayah Kota Administrasi {{ $log->pindah_ke }} @else @endif.
                </li>
                <li>
                    Mengingat perpindahan domisili tersebut ke wilayah kota/kabupaten lain, 
                    maka sesuai ketentuan Pasal 17 ayat (2) Peraturan Gubernur Provinsi DKI Jakarta 
                    Nomor 10 Tahun 2007, dengan ini mencabut nomor bukti pencatatan 
                    Serikat pekerja/serikat buruh di perusahaan
                </li>
                <ol type="a" style="padding: 0px 0px 0px 20px;">
                    <li>Nama serikat pekerja/serikat buruh</li>
                    <dl style="padding: 5px 0px 0px 20px;">
                        <dt>{{ $result['nama_serikat'] }}</dt>
                    </dl>
                    <li style="padding-top: 10px;">Alamat/kedudukan</li>
                    <dl style="padding: 5px 0px 0px 20px;">
                        <dt>
                            {{ $result['alamat']}}
                            <br/>
                            {{' Kel. '.$result['kelurahan']. ', ' .'Kec. '.$result['kecamatan']}}
                            <br/>
                            {{ $result['wilayah']}}
                        </dt>
                    </dl>
                    <li style="padding-top: 10px;">Status</li>
                    <dl style="padding: 5px 0px 0px 20px;">
                        <dt>{{ $result['status_serikat'] }}</dt>
                    </dl>
                    <li style="padding-top: 10px;">Nomor bukti pencacatan yang dicabut Nomor {{ $result['nomor_pencatatan'] }}</li>
                    <dl style="padding: 5px 0px 0px 20px;">
                        <dt>tanggal {{ Carbon\Carbon::parse($result['tgl_pencatatan'])->translatedFormat('d F Y') }}</dt>
                    </dl>
                </ol>
            </ol>
            <p class="font-arial font12" align="justify">
                Demikian kami sampaikan untuk diketahui.
                <br/>
            </p>
            <br/>
            
            <div style="width: 40%; float: right; text-align: center;">
                <p class="font12 font-arial">
                    {{ $log->pilih_ttd != 'Kepala' ? $log->pilih_ttd.'.' : '' }} Kepala Suku Dinas Tenaga Kerja,
                    Transmigrasi dan Energi
                    Kota Administrasi Jakarta Pusat,
                    
                </p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <p class="font12 font-arial">
                    {{$log->nama_ttd}}
                    <br/>
                    NIP. {{$log->nip_ttd}}
                </p>
            </div>
        </div>
        
        
    </body>
</head>
</html>
