$(function () {
    var dataTableRole = $(".datatables-role").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: `/role`,
        },
        columns: [
            { data: "" },
            { data: "name", name: "name" },
            { data: "description", name: "description" },
            { data: "created_by", name: "created_by" },
            { data: "updated_by", render: (data) => {
                return data || '-';
            } },
            { data: "action", name: "action", orderable: false, searchable: false},
        ],
        columnDefs: [
            {
                className: "center",
                orderable: true,
                searchable: false,
                responsivePriority: 2,
                targets: 0,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },
        ],
        order: [[0, "asc"]],
        dom:
            '<"row mx-1"' +
            '<"col-sm-12 col-md-3" l>' +
            '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
            ">t" +
            '<"row mx-2"' +
            '<"col-sm-12 col-md-6"i>' +
            '<"col-sm-12 col-md-6"p>' +
            ">",
        language: {
            sLengthMenu: "Show _MENU_",
            search: "Search",
            searchPlaceholder: "Search..",
        },
        // Buttons with Dropdown
        buttons: [
            {
                text: "TAMBAH ROLE",
                className: "add-new btn btn-primary mb-3 mb-md-0",
                attr: {
                    "data-bs-toggle": "modal",
                    "data-bs-target": "#addRoleModal",
                },
                init: function (api, node, config) {
                    $(node).removeClass("btn-secondary");
                },
            },
        ],
    });

    $("body").on("click", ".editRole", function () {
        var id = $(this).data("id");

        $.ajax({
            url: `/role/show-edit/${id}`,
            type: "GET",
            cache: false,
            success: function (response) {

                $('#editRoleModal').html(response)
                // //open modal
                $("#editRoleModal").modal("show");
            },
            error: function(jqXhr, json, errorThrown) {
                $("#addRoleModal").modal('hide');
                console.log(errorThrown);
            }

        });
    });
});
