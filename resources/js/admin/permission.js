/**
* App user list (jquery)
*/

"use strict";

$(function () {
    var dataTablePermissions = $(".datatables-permissions"),
    dt_permission;
    // userList = 'app-user-list.html';

    // Users List datatable
    if (dataTablePermissions.length) {
        dt_permission = dataTablePermissions.DataTable({
            ajax: {
                url: `/permission/get-data`,
            },
            columns: [
                // columns according to JSON
                // { data: '1' },
                { data: "" },
                { data: "name" },
                { data: "url_path" },
                { data: "created_at" },
                { data: "" },
            ],
            columnDefs: [
                {
                    // For Responsive
                    className: "center",
                    orderable: true,
                    searchable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    // Name
                    targets: 2,
                    render: function (data, type, full, meta) {
                        var $url = full["url_path"];
                        return (
                            '<a href="' +
                            full["url_path"] +
                            '" class="text-nowrap" style"color:teal">' +
                            $url +
                            "</a>"
                            );
                        },
                    },
                    {
                        // For Responsive
                        className: "align-top",
                        orderable: true,
                        searchable: false,
                        responsivePriority: 2,
                        targets: 3,
                        render: function (data, type, row, meta) {
                            return moment(data).format("MM-DD-YYYY");
                        },
                    },
                    // {
                    //   targets: 1,
                    //   searchable: false,
                    //   visible: false
                    // },
                    // {
                    //   // User Role
                    //   targets: 3,
                    //   orderable: false,
                    //   render: function (data, type, full, meta) {
                    //     var $assignedTo = full['url'],
                    //       $output = '';
                    //     var roleBadgeObj = {
                    //       AdminSistem:
                    //         '<a href="' +
                    //         userList +
                    //         '"><span class="badge rounded-pill bg-label-primary m-1">your/path/admin</span></a>',
                    //       Head:
                    //         '<a href="' + userList + '"><span class="badge rounded-pill bg-label-warning m-1">your/path/head</span></a>',
                    //       Staff:
                    //         '<a href="' + userList + '"><span class="badge rounded-pill bg-label-success m-1">your/path/staff</span></a>',
                    //       Koordinator:
                    //         '<a href="' + userList + '"><span class="badge rounded-pill bg-label-info m-1">your/path/koordinator</span></a>',
                    //     };
                    //     for (var i = 0; i < $assignedTo.length; i++) {
                    //       var val = $assignedTo[i];
                    //       $output += roleBadgeObj[val];
                    //     }
                    //     return '<span class="text-nowrap">' + $output + '</span>';
                    //   }
                    // },
                    // {
                    //   // remove ordering from Name
                    //   targets: 4,
                    //   orderable: false,
                    //   render: function (data, type, full, meta) {
                    //     var $date = full['created_at'];
                    //     return '<span class="text-nowrap">' + $date + '</span>';
                    //   }
                    // },
                    {
                        // Actions
                        targets: -1,
                        searchable: false,
                        title: "Actions",
                        orderable: false,
                        render: function (data, type, full, meta) {
                            return (
                                '<span class="text-nowrap"><button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2 editPermissionModal" data-id="'+full["id"]+'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>' +
                                '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon btnDelete" data-id="'+full["id"]+'"><i class="mdi mdi-delete-outline mdi-20px"></i></button></span>'
                                );
                            },
                        },
                    ],
                    order: [[0, "asc"]],
                    dom:
                    '<"row mx-1"' +
                    '<"col-sm-12 col-md-3" l>' +
                    '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
                    ">t" +
                    '<"row mx-2"' +
                    '<"col-sm-12 col-md-6"i>' +
                    '<"col-sm-12 col-md-6"p>' +
                    ">",
                    language: {
                        sLengthMenu: "Show _MENU_",
                        search: "Search",
                        searchPlaceholder: "Search..",
                    },
                    // Buttons with Dropdown
                    buttons: [
                        {
                            text: "Add Permission",
                            className: "add-new btn btn-primary mb-3 mb-md-0",
                            attr: {
                                "data-bs-toggle": "modal",
                                "data-bs-target": "#addPermissionModal",
                            },
                            init: function (api, node, config) {
                                $(node).removeClass("btn-secondary");
                            },
                        },
                    ]
                });
            }

            //show permission
            $("body").on("click", ".editPermissionModal", function () {
                var id = $(this).data("id");
                $.ajax({
                    url: `/permission/show/${id}`,
                    type: "GET",
                    cache: false,
                    success: function (response) {
                        var data = response.data
                        $("#permissionId").val(id);
                        $("#name").val(data.name);
                        $("#desc").val(data.description);
                        $("#urlName").val(data.url_name);
                        $("#urlPath").val(data.url_path);
                        $("#icons").val(data.icon);
                        $("#parentId").val(data.parent_id);
                        $("#parentName").val(data.parent_id).append('<span>'+data.parent_+'</span>');
                        $("#orderNumber").val(data.order_number);
                        //open modal
                        $("#editPermissionModal").modal("show");
                    },
                    error: function(jqXhr, json, errorThrown) {
                        $("#addRoleModal").modal('hide');
                        console.log(errorThrown);
                    }

                });
            });


            $("body").on("click", ".btnDelete", function () {
                var id = $(this).data("id");
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    preConfirm: function() {
                        return new Promise(function(resolve) {
                            $.ajax({
                                url: `/permission/destroy/${id}`,
                                type: "GET",
                                success: function(response) {
                                    if (response.type == "success") {
                                        Swal.fire({
                                            type: 'success',
                                            title: 'Deleted Success',
                                            showConfirmButton: true
                                        });
                                        location.reload();
                                    } else {
                                        Swal.fire({
                                            type: 'info',
                                            title: 'Failed!',
                                            text: "Deleted Not Success",
                                            showCancelButton: true,
                                        });
                                        location.reload();
                                    }
                                },
                            });
                        });
                    },
                })
            });


        });

