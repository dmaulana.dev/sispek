<?php

namespace App\Imports;

use App\Models\MasterKategori;
use App\Models\MasterKategoriAffiliasi;
use App\Models\Perusahaan;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Pencatatan;

use Illuminate\Support\Facades\Log;

class PencatatanImport implements ToCollection, WithHeadingRow, WithStartRow
{
    /**
    * @return int
    */
    public function startRow(): int
    {
        return 3;
    }
    
    /**
    * @param array $row
    * @param Collection $collection
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    
    
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            // rundom code
            // $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            // $pin = mt_rand(10000, 99999). $characters[rand(0, strlen($characters) - 1)];
            
            // // shuffle the result
            // $code = str_shuffle($pin);
            
            
            $last = Pencatatan::orderBy('id', 'desc')->first();
            if (!empty($last)) {
                $urut = $last->nomor_urut + 1;
            }else{
                $urut = 1;
            }
            
            // cek bentuk serikat/kategori sudah terinput atau belum by nama
            $bentukSerikat = MasterKategori::where('nama', $row['bentuk_serikat'])->first();
            if (empty($bentukSerikat)) {
                if (!empty($row['bentuk_serikat'])) {
                    $bentuk = new MasterKategori;
                    $bentuk->nama = $row['bentuk_serikat'];
                    $bentuk->status = 1;
                    $bentuk->save();
                }
            }
            
            // cek afiliasi sudah terinput atau belum by nama
            $afiliasi = MasterKategoriAffiliasi::where('nama', $row['afiliasi'])->first();
            if (empty($afiliasi)) {
                if (!empty($row['afiliasi'])) {
                    $af = new MasterKategoriAffiliasi;
                    $af->nama = $row['afiliasi'];
                    $af->status = 1;
                    $af->save();
                }
            }
            
            if (!empty($row['pengurus'])) {
                $pengurus = $row['pengurus'];
                $resPengurus = explode(":", $pengurus);
            }
            
            // Log::info($resPengurus);
            
            if (!empty($row['tgl_pencatatan'])) {
                $time = strtotime($row['tgl_pencatatan']);
                $newformat = date('Y-m-d', $time);
            }
            
            $item = Pencatatan::firstOrNew([
                'nama_serikat' =>  $row['nama_serikat']
            ]);
            $item->kategori_id = !empty($bentukSerikat->id) ? $bentukSerikat->id : 1011;
            $item->affiliasi_id = !empty($afiliasi->id) ? $afiliasi->id : null;
            $item->nomor_urut = $urut;
            $item->no_suat = $row['no_suat'];
            $item->tgl_surat = $row['tgl_surat'];
            $item->bentuk_serikat = $row['bentuk_serikat'];
            $item->jenis_serikat = $row['jenis_serikat'];
            $item->nama_serikat = $row['nama_serikat'];
            $item->nama_singkat = $row['nama_singkatan'];
            $item->no_surat_permohonan = $row['no_surat_permohonan'];
            $item->tgl_surat_permohonan = $row['tgl_surat_permohonan'];
            $item->perusahaan = null;
            $item->wilayah = $row['wilayah'];
            $item->kecamatan = $row['kecamatan'];
            $item->alamat = $row['alamat'];
            $item->kelurahan = $row['kelurahan'];
            $item->status_serikat = $row['status_serikat'];
            $item->afiliasi = $row['afiliasi'];
            $item->nomor_pencatatan = $row['nomor_bukti_pencatatan'];
            $item->tgl_pencatatan = $newformat;
            $item->pengurus = !empty($resPengurus) ? $resPengurus[0] : null;
            $item->visible = $row['visible'] === 'Aktif' ? 1 : 0;
            $item->keterangan_nonaktif = $row['keterangan_non_aktif'];
            $item->kategori_ttd = $row['kategori_ttd'];
            $item->nama_ttd = $row['nama_ttd'];
            $item->nip_ttd = $row['nip_ttd'];
            $item->keterangan = $row['keterangan'];
            $item->created_by = !empty(auth()->user()->full_name) ? auth()->user()->full_name : 'admin';
            $item->save();
            
            // cek perusahaan
            if (!empty($row['perusahaan']) && $row['perusahaan'] != '-') {
                $perusahaan = Perusahaan::firstOrNew([
                    'nama_serikat' =>  $row['nama_serikat'],
                    'nama_perusahaan' =>  $row['perusahaan'],
                ]);
                $perusahaan->pencatatan_id = $item->id;
                $perusahaan->nama_serikat = $row['nama_serikat'];
                $perusahaan->nama_perusahaan = $row['perusahaan'];
                $perusahaan->created_by = $item->created_by = !empty(auth()->user()->full_name) ? auth()->user()->full_name : 'admin';
                $perusahaan->save();
            }
        }
        
    }
}
