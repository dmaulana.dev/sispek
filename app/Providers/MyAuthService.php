<?php

namespace App\Providers;
use App\Models\User;

class MyAuthService
{
    public function getUserByToken($token)
    {
        // Implement your token validation logic here
        // For example, if you are using Eloquent and the User model
        // you can check if the token matches any user's token in the database
        $user = User::where('api_token', $token)->first();
        return $user;
    }
}
