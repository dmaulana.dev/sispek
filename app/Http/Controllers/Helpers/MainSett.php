<?php

namespace App\Http\Controllers\Helpers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;

class MainSett extends Controller
{
    public static function getUserDataSession()
    {
        return Session::get('data_user');
    }

    public static function decodeId($value)
    {
        try {
            return hex2bin(substr($value, 20));
        } catch (\Throwable $th) {
            abort(404);
        }
    }

    public static function encodeId($value)
    {
        return Self::generateRandomString(20) . bin2hex($value);
    }

    public static function generateRandomString($length = 10, $type = 'normal')
    {
        if ($type === 'normal') {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        } else {
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
