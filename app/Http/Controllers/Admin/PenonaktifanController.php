<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\LogPenonaktifan;
use App\Models\MasterKategori;
use App\Models\Pencatatan;
use App\Models\Perusahaan;
use App\Models\SusunanPengurus;
use App\Models\WilayahDKI;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PenonaktifanController extends Controller
{
    public function index(Request $request){
        $data = LogPenonaktifan::get();
        
        if ($request->ajax()) {
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-success rounded-pill btn-icon me-2 upload-record" data-id="'.$row->id.'"><i class="mdi mdi-cloud-upload mdi-20px"></i></button>';
                $btn = $btn.'<a href="'.route('penonaktifan.pdf', $row->id).'" class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2"><i class="mdi mdi-cloud-print-outline mdi-20px"></i></a>';
                $btn = $btn.'<a href="'.route('pencatatan.details', $row->pencatatan_id).'" class="btn btn-sm btn-icon btn-text-info rounded-pill btn-icon me-2 details-pencatatan"><i class="mdi mdi-view-carousel mdi-20px"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.penonaktifan.index');
    }
    
    
    public function show(Request $request){
        $pencatatan = Pencatatan::where('status', 1)->get();
        
        if ($request->ajax()) {
            return Datatables::of(json_decode($pencatatan))
            ->addIndexColumn()
            ->addColumn('action', function($row){
                if ($row->status == '1') {
                    $btn = '<a href="'.route('penonaktifan.create', $row->id).'" class="btn btn-sm btn-success rounded-pill btn-icon me-2"><i class="mdi mdi-plus mdi-10px"></i></a>';
                } else {
                    $btn = '';
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        
    }
    
    
    public function create(Request $request, $id){
        $bentukSerikat = MasterKategori::get();
        $data = Pencatatan::where('id', $id)->first();
        // return $data;
        $perusahaan = Perusahaan::where('pencatatan_id', $id)->get();
        $susunan = SusunanPengurus::where('pencatatan_id', $id)->get();
        
        $wilayah = WilayahDKI::get();
        $resWilayah = $wilayah->groupBy('wilayah')->map(function ($dt) {
            return [
                'wilayah' => $dt->first()->wilayah
            ];
        })
        ->values();
        
        return view('admin.penonaktifan.create', [
            'kategori' => $bentukSerikat,
            'data' => $data,
            'perusahaan' => $perusahaan,
            'susunan' => $susunan,
            'wilayah' => $resWilayah,
        ]);
    }
    
    public function store(Request $request){
        $pencatatan = Pencatatan::where('id', $request->pencatatan_id)->first();
        $pencatatan->visible = 0;
        
        // simpan ke log penolakan
        // cek dulu
        $tolakLast = LogPenonaktifan::orderBy('id', 'desc')->first();
        if (!empty($tolakLast)) {
            $noUrut = $tolakLast->nomor_urut + 1;
        }else{
            $noUrut = 1;
        }
        $nonaktif = new LogPenonaktifan();
        $nonaktif->pencatatan_id = $request->pencatatan_id;
        $nonaktif->nomor_urut = $noUrut;
        $nonaktif->bentuk_serikat = $pencatatan->bentuk_serikat;
        $nonaktif->nama_serikat = $pencatatan->nama_serikat;
        $nonaktif->nomor_pencatatan = $pencatatan->nomor_pencatatan;
        $nonaktif->no_surat = $request->no_surat;
        $nonaktif->tgl_surat = $request->tgl_surat;
        $nonaktif->no_surat_permohonan = $request->no_surat_permohonan;
        $nonaktif->tgl_surat_permohonan = $request->tgl_surat_permohonan;
        $nonaktif->dok_surat_permohonan = $request->dok_surat_permohonan;
        $nonaktif->status_alasan = $request->status_alasan;
        $nonaktif->keterangan = $request->keterangan;
        $nonaktif->pilih_ttd = $request->pilih_ttd;
        $nonaktif->nama_ttd = $request->nama_ttd;
        $nonaktif->nip_ttd = $request->nip_ttd;
        $nonaktif->pindah_ke = $request->pindah_ke;

        if (!empty($request->dok_surat_permohonan) && $request->dok_surat_permohonan != 'null') {
            $file = $request->file('dok_surat_permohonan');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/dokument', $filename);
            
            $file_path = "storage/app/public/dokument/".$filename;
            $file_serverpath = asset($file_path);
            $nonaktif->dok_surat_permohonan = $file_serverpath;
        }
        $nonaktif->tgl_penonaktifan = Carbon::now()->format('Y-m-d');
        $nonaktif->created_by = auth()->user()->full_name;
        $nonaktif->save();
        
        $pencatatan->save();
        
        return redirect()->route('penonaktifan.index');
    }
    
    // public function upload(Request $request){
        //     $data = LogPenonaktifan::where('id', $request->penonaktifan_id)->first();
        //     if (!empty($request->dokument) && $request->dokument != 'null') {
            //         $file = $request->file('dokument');
            //         $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            //         $file->storeAs('public/dokument', $filename);
            
            //         $file_path = "storage/app/public/dokument/".$filename;
            //         $file_serverpath = asset($file_path);
            //         $data->dokument_url = $file_serverpath;
            //         $data->dokument_nama = $filename;
            //     }
            
            //     $data->save();
            //     return redirect()->route('penonaktifan.index');
            // }
            
            public function pdf($id){
                $log = LogPenonaktifan::where('id', $id)->first();
                // return $log;
                // die();
                $data = Pencatatan::where('id', $log->pencatatan_id)->first();
                
                $result = [
                    'tgl_pencatatan' => Carbon::parse($data->tgl_pencatatan)->format('d F Y'),
                    'nama_serikat' => $data->nama_serikat,
                    'alamat' => $data->alamat,
                    'kecamatan' => $data->kecamatan,
                    'kelurahan' => $data->kelurahan,
                    'wilayah' => $data->wilayah,
                    'nomor_pencatatan' => $data->nomor_pencatatan,
                    'jenis_serikat' => $data->jenis_serikat,
                    'status_serikat' => $data->status_serikat,
                    'nama_ttd' => $data->nama_ttd,
                    'nip_ttd' => $data->nip_ttd,
                ];
                
                $pdf = Pdf::loadView('admin.penonaktifan.pdf', [
                    'result' => $result,
                    'log' => $log
                ]);
                return $pdf->stream('pengganti.pdf');
            }
        }
        