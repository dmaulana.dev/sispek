<?php

namespace App\Http\Controllers\Admin;

use App\Exports\RekapAnggota;
use App\Exports\RekapBentukSerikat;
use App\Http\Controllers\Controller;
use App\Models\MasterKategori;
use App\Models\Pencatatan;
use App\Models\Perusahaan;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DaftarLaporan;

use DB;
class LaporanController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $data = Pencatatan::select('*')->orderBy('id', 'desc');
            return Datatables::of($data)
            ->addIndexColumn()
            ->filter(function ($instance) use ($request) {
                if ($request->get('visible') == '0' || $request->get('visible') == '1') {
                    $instance->where('visible', $request->get('visible'));
                }
                if ($request->get('status') == '0' || $request->get('status') == '1' || $request->get('status') == '2' ) {
                    $instance->where('status', $request->get('status'));
                }
                if ($request->get('bentuk_serikat') == 'SP/SB' || $request->get('bentuk_serikat') == 'Federasi' || $request->get('bentuk_serikat') == 'Konfederasi' ) {
                    $instance->where('bentuk_serikat', $request->get('bentuk_serikat'));
                }
                if ($request->get('status_serikat') == 'Afiliasi' || $request->get('status_serikat') == 'Mandiri') {
                    $instance->where('status_serikat', $request->get('status_serikat'));
                }
                if (!empty($request->get('search'))) {
                    $instance->where(function($w) use($request){
                        $search = $request->get('search');
                        $w->orWhere('nomor_pencatatan', 'LIKE', "%$search%")
                        ->orWhere('nama_serikat', 'LIKE', "%$search%");
                    });
                }
            })
            ->rawColumns(['visible', 'status', 'bentuk_serikat', 'status_serikat'])
            ->make(true);
        }
        
        return view('admin.laporan.index');
    }
    
    public function export(Request $request){
        if (!empty($request->pilih_export)) {
            if ($request->pilih_export == 'pdf') {
                
                $index = Pencatatan::orderBy('id', 'desc')->where(function ($where) use ($request) {
                    
                    if ($request->get('visible') == '0' || $request->get('visible') == '1') {
                        $where->where('visible', $request->get('visible'));
                    }
                    if ($request->get('status') == '0' || $request->get('status') == '1' || $request->get('status') == '2' ) {
                        $where->where('status', $request->get('status'));
                    }
                    if ($request->get('bentuk_serikat') == 'SP/SB' || $request->get('bentuk_serikat') == 'Federasi' || $request->get('bentuk_serikat') == 'Konfederasi' ) {
                        $where->where('bentuk_serikat', $request->get('bentuk_serikat'));
                    }
                    if ($request->get('status_serikat') == 'Afiliasi' || $request->get('status_serikat') == 'Mandiri') {
                        $where->where('status_serikat', $request->get('status_serikat'));
                    }
                });
                
                $unmap = (clone $index)
                ->get();
                
                $query = $unmap->map(function ($item, $index) {
                    $perusahaan = Perusahaan::where('pencatatan_id', $item->id)->get();
                    
                    if ($item->status == 0) {
                        $status = 'Proses';
                    }elseif ($item->status == 1) {
                        $status = 'Selesai';
                    }else{
                        $status = 'Ditolak';
                    }
                    
                    $alamat = $item->wilayah.' '.$item->kecatamatan.' '.$item->alamat.' '.$item->kelurahan;
                    return [
                        'no' => $index + 1,
                        'bentuk_serikat' => $item->bentuk_serikat,
                        'jenis_serikat' => $item->jenis_serikat,
                        'nama_serikat' => $item->nama_serikat,
                        'nama_singkat' => $item->nama_singkat,
                        'perusahaan' => collect($perusahaan)->pluck('nama_perusahaan')->implode(', '),
                        'alamat' => $alamat,
                        'status_serikat' => $item->status_serikat,
                        'afiliasi' => $item->afiliasi,
                        'nomor_pencatatan' => $item->nomor_pencatatan,
                        'tgl_pencatatan' => Carbon::parse($item->tgl_pencatatan)->format('d m Y'),
                        'pengurus' => 'Ketua : '.$item->pengurus,
                        'status' => $status,
                        'visible' => $item->visible == 1 ? 'Aktif' : 'Non Aktif',
                        'keterangan_nonaktif' => $item->keterangan_nonaktif,
                        'keterangan' => $item->keterangan
                    ];
                })
                ->values();
                
                $pdf = Pdf::loadView('admin.laporan.daftar-pdf', [
                    'data' => $query,
                    'request' => $request->get('bentuk_serikat')
                    ])->setPaper('a4', 'landscape');
                    
                    return $pdf->stream('daftar-laporan.pdf');
                }else{
                    return Excel::download(new DaftarLaporan($request), 'daftar-laporan.xlsx', \Maatwebsite\Excel\Excel::XLSX);
                }
            } else {
                return redirect()->route('daftar.laporan.index');
            }
            
        }
        
        
        public function rekapIndex(Request $request){
            $bentukSerikat = MasterKategori::get();
            $firstTgl = Pencatatan::orderBy('tgl_pencatatan', 'asc')->first();
            
            $from = date($firstTgl->tgl_pencatatan);
            $to = date('2023-12-31');
            
            $data = $bentukSerikat->map(function ($query) use ($from, $to) {
                $pencatatan = Pencatatan::where('kategori_id', $query->id)->get();
                return $pencatatan->groupBy('bentuk_serikat')->map(function ($qp) use ($from, $to){
                    $tahun_2023 = $qp->whereBetween('tgl_pencatatan', [$from, $to])->count();
                    $tahun_2024 = $qp->whereBetween('tgl_pencatatan', ['2024-01-01', Carbon::now()->format('Y-m-d')])->count();
                    $afiliasi_aktif = $qp->where('status_serikat', 'Afiliasi')->where('visible', 1)->count();
                    $mandiri_aktif = $qp->where('status_serikat', 'Mandiri')->where('visible', 1)->count();
                    $afiliasi_nonaktif = $qp->where('status_serikat', 'Afiliasi')->where('visible', 0)->count();
                    $mandiri_nonaktif = $qp->where('status_serikat', 'Mandiri')->where('visible', 0)->count();
                    return [
                        'bentuk_serikat' => $qp->first()->bentuk_serikat,
                        'tahun_2023' => $tahun_2023,
                        'tahun_2024' => $tahun_2024,
                        'jumlah' => $tahun_2023 + $tahun_2024,
                        'jenis_serikat_diperusaaan' => $qp
                        ->where('jenis_serikat', 'Serikat di Perusahaan')
                        ->groupBy('jenis_serikat')
                        ->map(function ($jns){
                            $afiliasi = $jns->where('status_serikat', 'Afiliasi')->count();
                            $mandiri = $jns->where('status_serikat', 'Mandiri')->count();
                            return [
                                'afiliasi' => $afiliasi,
                                'mandiri' => $mandiri,
                                'total' => $mandiri + $afiliasi
                            ];
                            
                        })
                        ->values(),
                        'jenis_serikat_diluar_perusaaan' => $qp
                        ->where('jenis_serikat', 'Serikat di Luar Perusahaan')
                        ->groupBy('jenis_serikat')
                        ->map(function ($jns){
                            $afiliasi = $jns->where('status_serikat', 'Afiliasi')->count();
                            $mandiri = $jns->where('status_serikat', 'Mandiri')->count();
                            return [
                                'afiliasi' => $afiliasi,
                                'mandiri' => $mandiri,
                                'total' => $mandiri + $afiliasi
                            ];
                            
                        })
                        ->values(),
                        'afiliasi_aktif' => $afiliasi_aktif,
                        'mandiri_aktif' => $mandiri_aktif,
                        'jumlah_aktif' => $afiliasi_aktif + $mandiri_aktif,
                        'afiliasi_nonaktif' => $afiliasi_nonaktif,
                        'mandiri_nonaktif' => $mandiri_nonaktif,
                        'jumlah_nonaktif' => $afiliasi_nonaktif + $mandiri_nonaktif,
                    ];
                })
                ->values();
            })
            ->collapse()
            ->values();
            
            // rekap 2 bawah
            $data_rekap = Pencatatan::whereIn('bentuk_serikat', ['Federasi', 'Konfederasi'])->get();
            $data_rekapitulasi = $data_rekap->groupBy('bentuk_serikat')->map(function ($qp) use ($from, $to){
                return $qp->groupBy('afiliasi')->map(function ($af) use ($from, $to) {
                    $tahun_2023 = $af->whereBetween('tgl_pencatatan', [$from, $to])->count();
                    $tahun_2024 = $af->whereBetween('tgl_pencatatan', ['2024-01-01', Carbon::now()->format('Y-m-d')])->count();
                    return [
                        'bentuk_serikat' => $af->first()->bentuk_serikat,
                        'afiliasi' => $af->first()->afiliasi === null ? 'Tidak ada afiliasi' : $af->first()->afiliasi,
                        'tahun_2023' => $tahun_2023,
                        'tahun_2024' => $tahun_2024,
                        'jumlah' => $tahun_2023 + $tahun_2024,
                        'jenis_serikat_diperusaaan' => $af
                        ->where('jenis_serikat', 'Serikat Pekerja di Perusahaan')
                        ->groupBy('jenis_serikat')
                        ->map(function ($jns){
                            $non_aktif = $jns->where('visible', 0)->count();
                            $aktif = $jns->where('visible', 1)->count();
                            return [
                                'aktif' => $aktif,
                                'non_aktif' => $non_aktif,
                            ];
                            
                        })
                        ->values(),
                        'jenis_serikat_diluar_perusaaan' => $af
                        ->where('jenis_serikat', 'Serikat Pekerja di Luar Perusahaan')
                        ->groupBy('jenis_serikat')
                        ->map(function ($jns){
                            $aktif = $jns->where('visible', 0)->count();
                            $non_aktif = $jns->where('visible', 1)->count();
                            return [
                                'aktif' => $aktif,
                                'non_aktif' => $non_aktif,
                            ];
                            
                        })
                        ->values(),
                    ];
                })
                ->values();
            })
            ->collapse()
            ->values();
            
            
            return view('admin.laporan.rekap-index', [
                'data' => $data,
                'data_rekapitulasi' => $data_rekapitulasi
            ]);
        }
        
        public function rekapExport(Request $request){
            if (!empty($request->pilih_export)) {
                if ($request->pilih_export == 'pdf') {
                    
                    $index = Pencatatan::orderBy('id', 'desc')->where(function ($where) use ($request) {
                        
                        if ($request->get('visible') == '0' || $request->get('visible') == '1') {
                            $where->where('visible', $request->get('visible'));
                        }
                        if ($request->get('status') == '0' || $request->get('status') == '1' || $request->get('status') == '2' ) {
                            $where->where('status', $request->get('status'));
                        }
                        if ($request->get('bentuk_serikat') == 'SP/SB' || $request->get('bentuk_serikat') == 'Federasi' || $request->get('bentuk_serikat') == 'Konfederasi' ) {
                            $where->where('bentuk_serikat', $request->get('bentuk_serikat'));
                        }
                        if ($request->get('status_serikat') == 'Afiliasi' || $request->get('status_serikat') == 'Mandiri') {
                            $where->where('status_serikat', $request->get('status_serikat'));
                        }
                    });
                    
                    $unmap = (clone $index)
                    ->get();
                    
                    $query = $unmap->map(function ($item, $index) {
                        $perusahaan = Perusahaan::where('pencatatan_id', $item->id)->get();
                        
                        if ($item->status == 0) {
                            $status = 'Proses';
                        }elseif ($item->status == 1) {
                            $status = 'Selesai';
                        }else{
                            $status = 'Ditolak';
                        }
                        
                        $alamat = $item->wilayah.' '.$item->kecatamatan.' '.$item->alamat.' '.$item->kelurahan;
                        return [
                            'no' => $index + 1,
                            'bentuk_serikat' => $item->bentuk_serikat,
                            'jenis_serikat' => $item->jenis_serikat,
                            'nama_serikat' => $item->nama_serikat,
                            'nama_singkat' => $item->nama_singkat,
                            'perusahaan' => collect($perusahaan)->pluck('nama_perusahaan')->implode(', '),
                            'alamat' => $alamat,
                            'status_serikat' => $item->status_serikat,
                            'afiliasi' => $item->afiliasi,
                            'nomor_pencatatan' => $item->nomor_pencatatan,
                            'tgl_pencatatan' => Carbon::parse($item->tgl_pencatatan)->format('d m Y'),
                            'pengurus' => 'Ketua : '.$item->pengurus,
                            'status' => $status,
                            'visible' => $item->visible == 1 ? 'Aktif' : 'Non Aktif',
                            'keterangan_nonaktif' => $item->keterangan_nonaktif,
                            'keterangan' => $item->keterangan
                        ];
                    })
                    ->values();
                    
                    $pdf = Pdf::loadView('admin.laporan.daftar-pdf', [
                        'data' => $query,
                        'request' => $request->get('bentuk_serikat')
                        ])->setPaper('a4', 'landscape');
                        
                        return $pdf->stream('daftar-laporan.pdf');
                    }else{
                        return Excel::download(new DaftarLaporan($request), 'daftar-laporan.xlsx', \Maatwebsite\Excel\Excel::XLSX);
                    }
                } else {
                    return redirect()->route('daftar.laporan.index');
                }
                
            }
            
            
            public function rekapAnggotaPdf(){
                $firstTgl = Pencatatan::orderBy('tgl_pencatatan', 'asc')->first();
                
                $from = date($firstTgl->tgl_pencatatan);
                $to = date('2023-12-31');
                
                $data_rekap = Pencatatan::whereIn('bentuk_serikat', ['Federasi', 'Konfederasi'])->get();
                $data_rekapitulasi = $data_rekap->groupBy('bentuk_serikat')->map(function ($qp) use ($from, $to){
                    return $qp->groupBy('afiliasi')->map(function ($af) use ($from, $to) {
                        $tahun_2023 = $af->whereBetween('tgl_pencatatan', [$from, $to])->count();
                        $tahun_2024 = $af->whereBetween('tgl_pencatatan', ['2024-01-01', Carbon::now()->format('Y-m-d')])->count();
                        return [
                            'bentuk_serikat' => $af->first()->bentuk_serikat,
                            'afiliasi' => $af->first()->afiliasi === null ? 'Tidak ada afiliasi' : $af->first()->afiliasi,
                            'tahun_2023' => $tahun_2023,
                            'tahun_2024' => $tahun_2024,
                            'jumlah' => $tahun_2023 + $tahun_2024,
                            'jenis_serikat_diperusaaan' => $af
                            ->where('jenis_serikat', 'Serikat Pekerja di Perusahaan')
                            ->groupBy('jenis_serikat')
                            ->map(function ($jns){
                                $non_aktif = $jns->where('visible', 0)->count();
                                $aktif = $jns->where('visible', 1)->count();
                                return [
                                    'aktif' => $aktif,
                                    'non_aktif' => $non_aktif,
                                ];
                                
                            })
                            ->values(),
                            'jenis_serikat_diluar_perusaaan' => $af
                            ->where('jenis_serikat', 'Serikat Pekerja di Luar Perusahaan')
                            ->groupBy('jenis_serikat')
                            ->map(function ($jns){
                                $aktif = $jns->where('visible', 0)->count();
                                $non_aktif = $jns->where('visible', 1)->count();
                                return [
                                    'aktif' => $aktif,
                                    'non_aktif' => $non_aktif,
                                ];
                                
                            })
                            ->values(),
                        ];
                    })
                    ->values();
                })
                ->collapse()
                ->values();
                
                
                $pdf = Pdf::loadView('admin.laporan.rekap-pdf-anggota', [
                    'data_rekapitulasi' => $data_rekapitulasi
                    ])
                    ->setPaper('a4', 'landscape');
                    
                    return $pdf->stream('daftar-rekap-anggota.pdf');
                }
                
                
                public function rekapBentukSerikatPdf(){
                    $bentukSerikat = MasterKategori::get();
                    $firstTgl = Pencatatan::orderBy('tgl_pencatatan', 'asc')->first();
                    
                    $from = date($firstTgl->tgl_pencatatan);
                    $to = date('2023-12-31');
                    
                    $data = $bentukSerikat->map(function ($query) use ($from, $to) {
                        $pencatatan = Pencatatan::where('kategori_id', $query->id)->get();
                        return $pencatatan->groupBy('bentuk_serikat')->map(function ($qp) use ($from, $to){
                            $tahun_2023 = $qp->whereBetween('tgl_pencatatan', [$from, $to])->count();
                            $tahun_2024 = $qp->whereBetween('tgl_pencatatan', ['2024-01-01', Carbon::now()->format('Y-m-d')])->count();
                            $afiliasi_aktif = $qp->where('status_serikat', 'Afiliasi')->where('visible', 1)->count();
                            $mandiri_aktif = $qp->where('status_serikat', 'Mandiri')->where('visible', 1)->count();
                            $afiliasi_nonaktif = $qp->where('status_serikat', 'Afiliasi')->where('visible', 0)->count();
                            $mandiri_nonaktif = $qp->where('status_serikat', 'Mandiri')->where('visible', 0)->count();
                            return [
                                'bentuk_serikat' => $qp->first()->bentuk_serikat,
                                'tahun_2023' => $tahun_2023,
                                'tahun_2024' => $tahun_2024,
                                'jumlah' => $tahun_2023 + $tahun_2024,
                                'jenis_serikat_diperusaaan' => $qp
                                ->where('jenis_serikat', 'Serikat Pekerja di Perusahaan')
                                ->groupBy('jenis_serikat')
                                ->map(function ($jns){
                                    $afiliasi = $jns->where('status_serikat', 'Afiliasi')->count();
                                    $mandiri = $jns->where('status_serikat', 'Mandiri')->count();
                                    return [
                                        'afiliasi' => $afiliasi,
                                        'mandiri' => $mandiri,
                                        'total' => $mandiri + $afiliasi
                                    ];
                                    
                                })
                                ->values(),
                                'jenis_serikat_diluar_perusaaan' => $qp
                                ->where('jenis_serikat', 'Serikat Pekerja di Luar Perusahaan')
                                ->groupBy('jenis_serikat')
                                ->map(function ($jns){
                                    $afiliasi = $jns->where('status_serikat', 'Afiliasi')->count();
                                    $mandiri = $jns->where('status_serikat', 'Mandiri')->count();
                                    return [
                                        'afiliasi' => $afiliasi,
                                        'mandiri' => $mandiri,
                                        'total' => $mandiri + $afiliasi
                                    ];
                                    
                                })
                                ->values(),
                                'afiliasi_aktif' => $afiliasi_aktif,
                                'mandiri_aktif' => $mandiri_aktif,
                                'jumlah_aktif' => $afiliasi_aktif + $mandiri_aktif,
                                'afiliasi_nonaktif' => $afiliasi_nonaktif,
                                'mandiri_nonaktif' => $mandiri_nonaktif,
                                'jumlah_nonaktif' => $afiliasi_nonaktif + $mandiri_nonaktif,
                            ];
                        })
                        ->values();
                    })
                    ->collapse()
                    ->values();
                    
                    $pdf = Pdf::loadView('admin.laporan.rekap-pdf-bentuk-serikat', [
                        'data' => $data
                        ])
                        ->setPaper('a4', 'landscape');
                        
                        return $pdf->stream('daftar-rekap-bentuk-serikat.pdf');
                    }
                    
                    public function rekapBentukSerikatExcell(Request $request){
                        return Excel::download(new RekapBentukSerikat($request), 'rekap-bentuk-serikat.xlsx', \Maatwebsite\Excel\Excel::XLSX);
                    }
                    
                    public function rekapAnggotaExcell(Request $request){
                        return Excel::download(new RekapAnggota($request), 'rekap-anggota-fed.xlsx', \Maatwebsite\Excel\Excel::XLSX);
                    }
                    
                }
                