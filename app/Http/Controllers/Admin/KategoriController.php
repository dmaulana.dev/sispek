<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MasterKategori;
use Illuminate\Http\Request;

use DataTables;

class KategoriController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = MasterKategori::get();
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2 edit-kategori" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                $btn = $btn.'<button class="btn btn-sm btn-icon btn-text-danger rounded-pill btn-icon me-2 delete-record" data-id="'.$row->id.'"><i class="mdi mdi-delete-outline mdi-20px"></i></button>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        
        return view('admin.master.kategori.index');
    }
    
    public function getData(){
        $data = MasterKategori::orderBy('id', 'desc')->get();
        return response()->json([
            'data' => $data,
            'type' => 'success',
            'message' => 'get data berhasil',
        ], 200);
    }
    
    public function store(Request $request){
        $data = new MasterKategori;
        $data->nama = $request->nama;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();
        
        return redirect()->route('master-kategori.index');
    }
    
    public function edit(Request $request, $id){
        $data = MasterKategori::where('id', $id)->first();
        
        return view('admin.master.kategori.edit', [
            'data' => $data
        ]);
    }
    
    public function update(Request $request, $id){
        $data = MasterKategori::find($id);
        $data->nama = $request->nama;
        $data->status = $request->status;
        $data->keterangan = $request->keterangan;
        $data->save();
        
        return redirect()->route('master-kategori.index');
    }
    
    public function destroy($id){
        $data = MasterKategori::find($id);
        $data->delete();
        
        return response()->json([
            'type' => 'success',
            'message' => 'Deleted success'
        ]);
    }
}
