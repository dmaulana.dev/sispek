<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\WilayahDKI;
use Illuminate\Http\Request;

class WilayahDKIController extends Controller
{
    public function index(Request $request){
        $data = WilayahDKI::where('wilayah', $request->wilayah)->get();
        
        return response()->json([
            'data' => $data
        ], 200);
    }
    
    public function kabupaten(Request $request){
        $data = WilayahDKI::where('wilayah', $request->wilayah)
        ->get();
        $data = $data->groupBy('kecamatan')->map(function ($dt) {
            return [
                'kecamatan' => $dt->first()->kecamatan
            ];
        })
        ->values();
        return response()->json([
            'data' => $data
        ], 200);
    }
    
    public function kecamatan(Request $request){
        $data = WilayahDKI::where('kecamatan', $request->kecamatan)
        ->get();
        $data = $data->groupBy('kelurahan')->map(function ($dt) {
            return [
                'kelurahan' => $dt->first()->kelurahan
            ];
        })
        ->values();

        return response()->json([
            'data' => $data
        ], 200);
    }
}
