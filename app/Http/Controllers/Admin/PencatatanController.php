<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TemplatePencatatan;
use App\Http\Controllers\Controller;
use App\Imports\PencatatanImport;
use App\Models\LogPenonaktifan;
use App\Models\LogPergantian;
use App\Models\LogPerubahan;
use App\Models\MasterKategori;
use App\Models\MasterKategoriAffiliasi;
use App\Models\Pencatatan;
use App\Models\PencatatanDokument;
use App\Models\Perusahaan;
use App\Models\SusunanPengurus;
use App\Models\WilayahDKI;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use Barryvdh\DomPDF\Facade\Pdf;

class PencatatanController extends Controller
{
    
    public function index(Request $request){
        $data = Pencatatan::select('pencatatan.*', 'dokument_pencatatan.dokument_url')
        ->leftJoin('dokument_pencatatan', 'dokument_pencatatan.pencatatan_id', '=', 'pencatatan.id')
        ->orderBy('pencatatan.id', 'desc')
        ->get();
        
        if ($request->ajax()) {
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-danger rounded-pill btn-icon me-2 delete-record" data-id="'.$row->id.'"><i class="mdi mdi-delete-outline mdi-20px"></i></button>';
                if ($row->status === 1) {
                    $btn = $btn.'<a href="'.route('pencatatan.pdf', $row->id).'" class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2"><i class="mdi mdi-cloud-print-outline mdi-20px"></i></a>';
                    $btn = $btn.'<button class="btn btn-sm btn-icon btn-text-success rounded-pill btn-icon me-2 upload-record" data-id="'.$row->id.'"><i class="mdi mdi-cloud-upload mdi-20px"></i></button>';
                }
                if ($row->status === 0) {
                    $btn = $btn.'<a href="'.route('pencatatan.edit', $row->id).'" class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2 edit-pencatatan"><i class="mdi mdi-pencil-outline mdi-20px"></i></a>';
                }
                $btn = $btn.'<a href="'.route('pencatatan.details', $row->id).'" class="btn btn-sm btn-icon btn-text-info rounded-pill btn-icon me-2 details-pencatatan"><i class="mdi mdi-view-carousel mdi-20px"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.pencatatan.index');
    }
    
    
    public function create(Request $request){
        $bentukSerikat = MasterKategori::get();
        $wilayah = WilayahDKI::get();
        $data = $wilayah->groupBy('wilayah')->map(function ($dt) {
            return [
                'kabupaten' => $dt->first()->wilayah
            ];
        })
        ->values();
        
        return view('admin.pencatatan.create', [
            'kategori' => $bentukSerikat,
            'wilayah' => $data
        ]);
    }
    
    public function getData(Request $request){
        $pencatatan = Pencatatan::where('kategori_id', $request->ids)->get();
        
        $data = $pencatatan->map(function ($query) {
            return [
                'nama_serikat' => $query->nama_serikat
            ];
        })
        ->groupBy('nama_serikat')
        ->collapse()
        ->values();
        
        return response()->json([
            'data' => $data,
            'type' => 'success'
        ], 200);
        
    }
    
    public function store(Request $request){
        
        $bentukSerikat = MasterKategori::where('id', $request->bentuk_serikat)->first();
        $afiliasi = MasterKategoriAffiliasi::where('nama', $request->afiliasi)->first();
        
        $pencatatan = Pencatatan::where('kategori_id', $request->bentuk_serikat)
        ->orderBy('nomor_urut', 'desc')
        ->first();
        
        
        if (!empty($pencatatan)) {
            $nomorUrut = str_pad($pencatatan->nomor_urut + 1, 3, '0', STR_PAD_LEFT);
        } else {
            $nomorUrut = '001';
        }
        
        $bulan = Carbon::now()->format('m');
        $tahun = Carbon::now()->format('Y');
        
        if ($bentukSerikat->nama == 'SP/SB') {
            $nomor_pencatatan = $nomorUrut.'/SP/JP/'.$bulan.'/'.$tahun;
        } elseif ($bentukSerikat->nama == 'Federasi') {
            $nomor_pencatatan = $nomorUrut.'/FSP/JP/'.$bulan.'/'.$tahun;
        } elseif ($bentukSerikat->nama == 'Konfederasi') {
            $nomor_pencatatan = $nomorUrut.'/KSP/JP/'.$bulan.'/'.$tahun;
        } else{
            $nomor_pencatatan = $nomorUrut.'/FC/JP/'.$bulan.'/'.$tahun;
        }
        
        
        // simpan ke pencataatan
        $data = new Pencatatan;
        $data->nomor_urut = $nomorUrut;
        $data->kategori_id = $request->bentuk_serikat;
        $data->affiliasi_id = !empty($afiliasi) ? $afiliasi->id : null;
        $data->bentuk_serikat = $bentukSerikat->nama;
        $data->jenis_serikat = $request->jenis_serikat;
        $data->nama_serikat = $request->nama_panjang;
        $data->nama_singkat = $request->nama_singkatan;
        $data->status_serikat = $request->status_serikat;
        $data->afiliasi = $request->afiliasi;
        $data->nomor_pencatatan = $nomor_pencatatan;
        $data->tgl_pencatatan = $request->tgl_pencatatan;
        $data->nama_ttd = $request->nama_ttd;
        $data->nip_ttd = $request->nip_ttd;
        $data->pilih_ttd = $request->pilih_ttd;
        $data->provinsi = 'DKI Jakarta';
        $data->wilayah = $request->wilayah;
        $data->kecamatan = $request->kecamatan;
        $data->kelurahan = $request->kelurahan;
        $data->alamat = $request->alamat;
        $data->no_surat = $request->no_surat;
        $data->tgl_surat = $request->tgl_surat;
        $data->judul_surat = $request->judul_surat;
        $data->no_surat_permohonan = $request->no_surat_permohonan;
        $data->tgl_surat_permohonan = $request->tgl_surat_permohonan;
        $data->permohonan_berkas_url = $request->permohonan_berkas_url;
        $data->pengurus = $request->nama[0];
        
        // upload logo
        if (!empty($request->logo_image) && $request->logo_image != 'null') {
            $file = $request->file('logo_image');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/images', $filename);
            
            $file_path = "storage/app/public/images/".$filename;
            $file_serverpath = asset($file_path);
            $data->logo_url = $file_serverpath;
            $data->logo_berkas = $filename;
        }
        $data->save();
        
        // perusahaan
        if (!empty($request['perusahaan'])) {
            foreach ($request['perusahaan'] as $key => $value) {
                $perusahaan = new Perusahaan;
                $perusahaan->pencatatan_id = $data->id;
                $perusahaan->nama_serikat = $data->nama_serikat;
                $perusahaan->nama_perusahaan = $request->perusahaan[$key];
                $perusahaan->created_by = auth()->user()->nama;
                $perusahaan->save();
            }
        }
        
        //pengurus
        if (!empty($request['jabatan'])) {
            foreach ($request['jabatan'] as $key => $value) {
                $susunan = new SusunanPengurus;
                $susunan->pencatatan_id = $data->id;
                $susunan->nama = $request->nama[$key];
                $susunan->jabatan = $request->jabatan[$key];
                $susunan->save();
            }
        }
        
        // return $data;
        
        return redirect()->route('pencatatan.index');
    }
    
    public function edit(Request $request, $id){
        $bentukSerikat = MasterKategori::get();
        $list_afiliasi  = MasterKategoriAffiliasi::get();
        $data = Pencatatan::where('id', $id)->first();
        
        $perusahaan = Perusahaan::where('pencatatan_id', $data->id)->get();
        $susunan = SusunanPengurus::where('pencatatan_id', $data->id)->get();
        
        $wilayah = WilayahDKI::get();
        $dataWilayah = $wilayah->groupBy('wilayah')->map(function ($dt) {
            return [
                'kabupaten' => $dt->first()->wilayah
            ];
        })
        ->values();
        return view('admin.pencatatan.edit', [
            'data' => $data,
            'kategori' => $bentukSerikat,
            'list_afiliasi' => $list_afiliasi,
            'perusahaan' => $perusahaan,
            'susunan' => $susunan,
            'wilayah' => $dataWilayah
        ]);
    }
    
    public function update(Request $request, $id){
        $bentukSerikat = MasterKategori::where('id', $request->bentuk_serikat)->first();
        
        // update ke pencataatan
        $data = Pencatatan::where('id', $id)->first();
        $data->kategori_id = $request->bentuk_serikat;
        
        $afiliasi = MasterKategoriAffiliasi::where('nama', $request->afiliasi)->first();
        if (!empty($afiliasi)) {
            $data->affiliasi_id = !empty($afiliasi) ? $afiliasi->id : null;
            $data->afiliasi = $request->afiliasi;
        }      
        
        $pengurusPertama = SusunanPengurus::where('pencatatan_id', $id)->where('nama', 'ketua')->first();
        if (!empty($pengurusPertama)) {
            $data->perusahaan = $pengurusPertama->nama;
        }
        
        $data->bentuk_serikat = $bentukSerikat->nama;
        $data->jenis_serikat = $request->jenis_serikat;
        $data->nama_serikat = $request->nama_panjang;
        $data->nama_singkat = $request->nama_singkatan;
        $data->status_serikat = $request->status_serikat;
        $data->tgl_pencatatan = $request->tgl_pencatatan;
        $data->nama_ttd = $request->nama_ttd;
        // $data->wilayah = $request->wilayah;
        // $data->kelurahan = $request->kelurahan;
        // $data->kecamatan = $request->kecamatan;
        $data->alamat = $request->alamat;
        $data->nip_ttd = $request->nip_ttd;
        $data->pilih_ttd = $request->pilih_ttd;
        $data->no_surat = $request->no_surat;
        $data->tgl_surat = $request->tgl_surat;
        $data->judul_surat = $request->judul_surat;
        $data->no_surat_permohonan = $request->no_permohonan;
        $data->tgl_surat_permohonan = $request->tgl_permohonan;
        
        // upload logo
        if (!empty($request->logo_image) && $request->logo_image != 'null') {
            $file = $request->file('logo_image');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/images', $filename);
            
            $file_path = "storage/app/public/images/".$filename;
            $file_serverpath = asset($file_path);
            $data->logo_url = $file_serverpath;
            $data->logo_berkas = $filename;
        }
        
        if (!empty($request->permohonan_berkas_url) && $request->permohonan_berkas_url != 'null') {
            $file = $request->file('permohonan_berkas_url');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/dokument', $filename);
            
            $file_path = "storage/app/public/dokument/".$filename;
            $file_serverpath = asset($file_path);
            $data->permohonan_berkas_url = $file_serverpath;
        }
        
        $data->status = 1;
        $data->save();
        
        // perusahaan
        if ($request['perusahaan'][0] != null) {
            foreach ($request['perusahaan'] as $key => $value) {
                $perusahaan = new Perusahaan;
                $perusahaan->pencatatan_id = $data->id;
                $perusahaan->nama_serikat = $data->nama_serikat;
                $perusahaan->nama_perusahaan = $request->perusahaan[$key];
                $perusahaan->created_by = auth()->user()->nama;
                $perusahaan->save();
            }
        }
        
        //pengurus
        if ($request['jabatan'][0] != null) {
            foreach ($request['jabatan'] as $key => $value) {
                $susunan = new SusunanPengurus;
                $susunan->pencatatan_id = $data->id;
                $susunan->nama = $request->nama[$key];
                $susunan->jabatan = $request->jabatan[$key];
                $susunan->save();
            }
        }
        
        
        $result = [
            'tgl_pencatatan' => $data->tgl_pencatatan,
            'nama_serikat' => $data->nama_serikat,
            'alamat' => $data->alamat,
            'wilayah' => $data->wilayah,
            'kecamatan' => $data->kecamatan,
            'kelurahan' => $data->kelurahan,
            'nomor_pencatatan' => $data->nomor_pencatatan,
            'jenis_serikat' => $data->jenis_serikat,
            'status_serikat' => $data->status_serikat,
            'nama_ttd' => $data->nama_ttd,
            'nip_ttd' => $data->nip_ttd,
            'pilih_ttd' => $data->pilih_ttd,
            'no_surat' => $data->no_surat,
            'tgl_surat' => $data->tgl_surat,
            'judul_surat' => $data->judul_surat,
            'no_surat_permohonan' => $data->no_surat_permohonan,
            'tgl_surat_permohonan' => $data->tgl_surat_permohonan
        ];
        
        if ($data->bentuk_serikat === 'SP/SB') {
            $pdf = Pdf::loadView('admin.pencatatan.pdf-spsb', [
                'result' => $result
            ]);
        }elseif ($data->bentuk_serikat === 'Federasi') {
            $pdf = Pdf::loadView('admin.pencatatan.pdf-federasi', [
                'result' => $result
            ]);
        }elseif ($data->bentuk_serikat === 'Konfederasi') {
            $pdf = Pdf::loadView('admin.pencatatan.pdf-konfederasi', [
                'result' => $result
            ]);
        }else{
            $pdf = Pdf::loadView('admin.pencatatan.pdf-dpcdpd', [
                'result' => $result
            ]);
        }
        return $pdf->stream('pencatatan-.'.$data->bentuk_serikat.'-.pdf');
    }
    
    public function pdf(Request $request, $id){
        $data = Pencatatan::where('id', $id)->first();
        
        $result = [
            'tgl_pencatatan' => $data->tgl_pencatatan,
            'nama_serikat' => $data->nama_serikat,
            'alamat' => $data->alamat,
            'wilayah' => $data->wilayah,
            'kecamatan' => $data->kecamatan,
            'kelurahan' => $data->kelurahan,
            'nomor_pencatatan' => $data->nomor_pencatatan,
            'jenis_serikat' => $data->jenis_serikat,
            'status_serikat' => $data->status_serikat,
            'nama_ttd' => $data->nama_ttd,
            'nip_ttd' => $data->nip_ttd,
            'pilih_ttd' => $data->pilih_ttd,
            'no_surat' => $data->no_surat,
            'tgl_surat' => $data->tgl_surat,
            'judul_surat' => $data->judul_surat,
            'no_surat_permohonan' => $data->no_surat_permohonan,
            'tgl_surat_permohonan' => $data->tgl_surat_permohonan
        ];
        
        $pdf = Pdf::loadView('admin.pencatatan.pdf-spsb', [
            'result' => $result
        ]);
        return $pdf->stream('pencatatan.pdf');
    }
    
    public function upload(Request $request){
        $data = new PencatatanDokument;
        if (!empty($request->dokument) && $request->dokument != 'null') {
            $file = $request->file('dokument');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/dokument', $filename);
            
            $file_path = "storage/app/public/dokument/".$filename;
            $file_serverpath = asset($file_path);
            $data->dokument_url = $file_serverpath;
            $data->dokument_nama = $filename;
        }
        
        $data->pencatatan_id = $request->pencatatan_id;
        $data->save();
        return redirect()->route('pencatatan.index');
    }
    
    
    public function details($id){
        $bentukSerikat = MasterKategori::get();
        $data = Pencatatan::where('id', $id)->first();
        // return $data;
        $perusahaan = Perusahaan::where('pencatatan_id', $id)->get();
        $susunan = SusunanPengurus::where('pencatatan_id', $id)->get();
        
        $dokument = PencatatanDokument::where('pencatatan_id', $data->id)->first();

        $perubahan = LogPerubahan::where('pencatatan_id', $data->id)->orderBy('perubahan_ke', 'asc')->get();
        $pengganti = LogPergantian::where('pencatatan_id', $data->id)->get();
        $penonAktifan = LogPenonaktifan::where('pencatatan_id', $data->id)->get();
        
        return view('admin.pencatatan.details', [
            'kategori' => $bentukSerikat,
            'data' => $data,
            'perusahaan' => $perusahaan,
            'susunan' => $susunan,
            'dokument' => $dokument,
            'perubahan' => $perubahan,
            'pengganti' => $pengganti,
            'penonaktifan' => $penonAktifan
        ]);
    }
    
    public function template(Request $request){
        return Excel::download(new TemplatePencatatan($request), 'template-pencatatan.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
    
    public function import(Request $request){
        
        Excel::import(new PencatatanImport, $request->import);
        
        return redirect()->route('pencatatan.index');
    }
    
    public function destroy(Request $request, $id){
        $data = Pencatatan::where('id', $id)->first();
        
        $perusahaan = Perusahaan::where('pencatatan_id', $data->id)->get();
        foreach ($perusahaan as $key => $value) {
            $datas = Perusahaan::where('id', $value['id'])->first();
            $datas->delete();
        }
        
        $susunan = SusunanPengurus::where('id', $value['id'])->get();
        foreach ($susunan as $key => $value) {
            $item = SusunanPengurus::where('id', $value['id'])->first();
            $item->delete();
        }
        
        $data->delete();
        
        return response()->json([
            'type' => 'success',
            'message' => 'delete data berhasil'
        ]);
    }
}
