<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pencatatan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardAdminController extends Controller
{
    public function index(){
        $spsb = Pencatatan::orderBy('id', 'desc')->where('bentuk_serikat', 'SP/SB');
        $federasi = Pencatatan::orderBy('id', 'desc')->where('bentuk_serikat', 'Federasi');
        $konfederasi = Pencatatan::orderBy('id', 'desc')->where('bentuk_serikat', 'Konfederasi');
        
        $data_spsb = $spsb->limit(10)->get();
        $data_federasi = $federasi->limit(10)->get();
        $data_konfederasi = $konfederasi->limit(10)->get();
        
        $resultCard = [
            'total_spsb' => $spsb->count(),
            'total_spsb_proses' => $spsb->where('status', 0)->count(),
            'total_spsb_selesai' => $spsb->where('status', 1)->count(),
            'total_spsb_tolak' => $spsb->where('status', 2)->count(),
            //
            'total_federasi' => $federasi->count(),
            'total_federasi_proses' => $federasi->where('status', 0)->count(),
            'total_federasi_selesai' => $federasi->where('status', 1)->count(),
            'total_federasi_tolak' => $federasi->where('status', 2)->count(),
            //
            'total_konfederasi' => $konfederasi->count(),
            'total_konfederasi_proses' => $konfederasi->where('status', 0)->count(),
            'total_konfederasi_selesai' => $konfederasi->where('status', 1)->count(),
            'total_konfederasi_tolak' => $konfederasi->where('status', 2)->count(),
        ];
        
        
        $reports = Pencatatan::select('*')
        ->orderBy('tgl_pencatatan', 'asc')
        ->get()
        ->groupBy(function ($val) {
            return Carbon::parse($val->tgl_pencatatan)->format('Y');
        });
        
        $report = $reports->map(function ($values) {
            $spsb = Pencatatan::whereYear('tgl_pencatatan', $values->first()->tgl_pencatatan)
            ->where('bentuk_serikat', 'SP/SB')->count();
            $federasi = Pencatatan::whereYear('tgl_pencatatan', $values->first()->tgl_pencatatan)
            ->where('bentuk_serikat', 'Federasi')->count();
            $konfederasi = Pencatatan::whereYear('tgl_pencatatan', $values->first()->tgl_pencatatan)
            ->where('bentuk_serikat', 'Konfederasi')->count();
            return [
                'tahun' => Carbon::parse($values->first()->tgl_pencatatan)->format('Y'),
                'spsb' => $spsb,
                'federasi' => $federasi,
                'konfederasi' => $konfederasi
            ];
        })
        ->values();
        
        $tahun = [];
        $spsb = [];
        $federasi = [];
        $konfederasi = [];
        foreach ($report as $key => $value) {
            $tahun[] = $value['tahun'];
            $spsb[] = $value['spsb'];
            $federasi[] = $value['federasi'];
            $konfederasi[] = $value['konfederasi'];
        }
        
        $result = [
            'tahun' => $tahun,
            'spsb' => $spsb,
            'federasi' => $federasi,
            'konfederasi' => $konfederasi
        ];


        // diagram per kecamatan
        $tahuns = Pencatatan::selectRaw('kecamatan, count(kecamatan) jumlah')
        ->groupBy('kecamatan')
        ->orderBy('kecamatan', 'asc')
        ->get();
        
        $nama = [];
        $jumlah = [];
        foreach ($tahuns as $key => $value) {
            $kecamatan[] = $value['kecamatan'];
            $jumlah[] = $value['jumlah'];
        }
        
        $barKecamatan = [
            'kecamatan' => $kecamatan,
            'jumlah' => $jumlah
        ];
        
        return view('admin.dashboard-utama', [
            'card_total' => $resultCard,
            'spsb' => $data_spsb,
            'federasi' => $data_federasi,
            'konfederasi' => $data_konfederasi,
            'result' => $result,
            'bar_kecamatan' => $barKecamatan
        ]);
    }
}
