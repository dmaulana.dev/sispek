<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TemplatePencatatan;
use App\Http\Controllers\Controller;
use App\Imports\PencatatanImport;
use App\Models\LogPerubahan;
use App\Models\MasterKategori;
use App\Models\MasterKategoriAffiliasi;
use App\Models\Pencatatan;
use App\Models\Perusahaan;
use App\Models\SusunanPengurus;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;

class PerubahanController extends Controller
{
    public function index(Request $request){
        $data = LogPerubahan::get();
        if ($request->ajax()) {
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-success rounded-pill btn-icon me-2 upload-record" data-id="'.$row->id.'"><i class="mdi mdi-cloud-upload mdi-20px"></i></button>';
                $btn = $btn.'<a href="'.route('perubahan.pdf', $row->id).'" class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2"><i class="mdi mdi-cloud-print-outline mdi-20px"></i></a>';
                $btn = $btn.'<a href="'.route('pencatatan.details', $row->pencatatan_id).'" class="btn btn-sm btn-icon btn-text-info rounded-pill btn-icon me-2 details-pencatatan"><i class="mdi mdi-view-carousel mdi-20px"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.perubahan.index');
    }
    
    
    public function show(Request $request){
        $pencatatan = Pencatatan::where('status', 1)->get();
        
        if ($request->ajax()) {
            return Datatables::of(json_decode($pencatatan))
            ->addIndexColumn()
            ->addColumn('action', function($row){
                if ($row->status == '1') {
                    $btn = '<a href="'.route('perubahan.create', $row->id).'" class="btn btn-sm btn-success rounded-pill btn-icon me-2"><i class="mdi mdi-plus mdi-10px"></i></a>';
                } else {
                    $btn = '';
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        
    }
    
    public function create($pencatatan_id){
        $bentukSerikat = MasterKategori::get();
        
        $data = Pencatatan::where('id', $pencatatan_id)->first();
        
        $susunan = SusunanPengurus::where('pencatatan_id', $pencatatan_id)->get();
        return view('admin.perubahan.create', [
            'kategori' => $bentukSerikat,
            'data' => $data,
            'susunan' => $susunan
        ]);
    }
    
    public function getData(Request $request){
        $pencatatan = Pencatatan::where('kategori_id', $request->ids)->get();
        
        $data = $pencatatan->map(function ($query) {
            return [
                'nama_serikat' => $query->nama_serikat
            ];
        })
        ->groupBy('nama_serikat')
        ->collapse()
        ->values();
        
        return response()->json([
            'data' => $data,
            'type' => 'success'
        ], 200);
        
    }
    
    public function store(Request $request){
        $id = $request->pencatatan_id;
        $tipePerubahan = $request->tipe_perubahan;
        
        // cek perubahan
        $perubahan = LogPerubahan::where('pencatatan_id', $id)->orderBy('id', 'desc')->first();
        
        if (!empty($perubahan)) {
            $perubahanKe = $perubahan->perubahan_ke + 1;
            $noUrut = $perubahan->nomor_urut + 1;
        }else{
            $perubahanKe = 1;
            $noUrut = 1;
        }
        
        // update ke pencatatan
        $pencatatanUpdate = Pencatatan::where('id', $id)->first();
        
        if ($tipePerubahan == 'Nama dan Lambang') {
            $pencatatanUpdate->nama_serikat = $request->nama_serikat;
            $pencatatanUpdate->nama_singkat = $request->nama_singkatan;
            
            if (!empty($request->logo) && $request->logo != 'null') {
                $file = $request->file('logo');
                $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
                $file->storeAs('public/images', $filename);
                
                $file_path = "storage/app/public/images/".$filename;
                $file_serverpath = asset($file_path);
                $pencatatanUpdate->logo_url = $file_serverpath;
                $pencatatanUpdate->logo_berkas = $filename;
            }
        }elseif ($tipePerubahan == 'Pengurus SP/SB') {
            foreach ($request['jabatan'] as $key => $value) {
                if ($request['jabatan'][$key] == 'Ketua') {
                    $pencatatanUpdate->pengurus = $request->pengurus;
                }else{
                    $susunan = SusunanPengurus::where('pencatatan_id', $id)->first();
                    $susunan->nama = $request['nama'][$key];
                    $susunan->jabatan = $request['jabatan'][$key];
                    $susunan->save();
                } 
            }
        }else{
            
        }
        
        // simpan ke log perubahan
        $pencatatan = Pencatatan::where('id', $id)->first();
        $perubahan = new LogPerubahan;
        $perubahan->nomor_urut = $noUrut;
        $perubahan->pencatatan_id = $id;
        $perubahan->bentuk_serikat = $pencatatan->bentuk_serikat;
        $perubahan->jenis_serikat = $pencatatan->jenis_serikat;
        $perubahan->nama_serikat = $pencatatan->nama_serikat;
        $perubahan->nama_singkat = !empty($pencatatan->nama_singkat) ? $pencatatan->nama_singkat : '';
        $perubahan->status_serikat = $pencatatan->status_serikat;
        $perubahan->alamat = !empty($pencatatan->alamat) ? $pencatatan->alamat : '';
        $perubahan->afiliasi = !empty($pencatatan->afiliasi) ? $pencatatan->afiliasi : '';
        $perubahan->nomor_pencatatan = $pencatatan->nomor_pencatatan;
        $perubahan->tgl_pencatatan = $pencatatan->tgl_pencatatan;
        $perubahan->logo_url = $pencatatan->logo_url;
        $perubahan->pengurus = $pencatatan->pengurus;
        $perubahan->perubahan_ke = $perubahanKe;
        $perubahan->perubahan_jenis = $tipePerubahan;
        $perubahan->no_permohonan = '00'.$noUrut;
        $perubahan->no_surat = $request->no_surat;
        $perubahan->tgl_surat = $request->tgl_surat;
        $perubahan->no_surat_permohonan = $request->no_surat_permohonan;
        $perubahan->tgl_surat_permohonan = $request->tgl_surat_permohonan;
        
        if (!empty($request->dok_surat_permohonan) && $request->dok_surat_permohonan != 'null') {
            $file = $request->file('dok_surat_permohonan');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/dokument', $filename);
            
            $file_path = "storage/app/public/dokument/".$filename;
            $file_serverpath = asset($file_path);
            $perubahan->dok_surat_permohonan = $file_serverpath;
        }
        
        $perubahan->pilih_ttd = $pencatatan->pilih_ttd;
        $perubahan->nama_ttd = $pencatatan->nama_ttd;
        $perubahan->nip_ttd = $pencatatan->nip_ttd;
        $perubahan->tgl_perubahan = Carbon::now()->format('Y-m-d');
        $perubahan->save();
        
        $pencatatanUpdate->save();
        
        return redirect()->route('perubahan.index');
    }
    
    public function upload(Request $request){
        $data = LogPerubahan::where('id', $request->perubahan_id)->first();
        if (!empty($request->dokument) && $request->dokument != 'null') {
            $file = $request->file('dokument');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/dokument', $filename);
            
            $file_path = "storage/app/public/dokument/".$filename;
            $file_serverpath = asset($file_path);
            $data->dokument_url = $file_serverpath;
            $data->dokument_nama = $filename;
        }
        
        $data->save();
        return redirect()->route('perubahan.index');
    }
    
    public function pdf($id){
        $log = LogPerubahan::where('id', $id)->first();
        $data = Pencatatan::where('id', $log->pencatatan_id)->first();
        
        $result = [
            'tgl_pencatatan' => $data->tgl_pencatatan,
            'nama_serikat' => $data->nama_serikat,
            'alamat' => $data->alamat,
            'nomor_pencatatan' => $data->nomor_pencatatan,
            'jenis_serikat' => $data->jenis_serikat,
            'status_serikat' => $data->status_serikat,
            'nama_ttd' => $data->nama_ttd,
            'nip_ttd' => $data->nip_ttd,
            'pilih_ttd' => $data->pilih_ttd,
            'wilayah' => $data->wilayah,
            'kecamatan' => $data->kecamatan,
            'kelurahan' => $data->kelurahan,
            'tgl_surat' => $data->tgl_surat,
            'no_surat' => $data->no_surat,
            'judul_surat' => $data->judul_surat,
        ];
        
        if ($log->perubahan_jenis == 'Nama dan Lambang') {
            $pdf = Pdf::loadView('admin.perubahan.pdf-lambang', [
                'result' => $result,
                'log' => $log
            ]);
        } else {
            $pdf = Pdf::loadView('admin.perubahan.pdf-pengurus', [
                'result' => $result,
                'log' => $log
            ]);
        }
        return $pdf->stream('perubahaan.pdf');
    }
    
    public function simpanPengurus(Request $request){
        $data = new SusunanPengurus;
        $data->pencatatan_id = $request->pencatatan_id;
        $data->nama = $request->nama_baru;
        $data->jabatan = $request->jabatan_baru;
        $data->save();
        
        return response()->json([
            'type' => 'success',
            'message' => 'data barhasil disimpan'
        ], 200);
    }
    
    public function destroy($id){
        $data = SusunanPengurus::where('id', $id)->first();
        $data->delete();
        
        return response()->json([
            'type' => 'success',
            'message' => 'hapus berhasil'
        ]);
    }
    
    
}
