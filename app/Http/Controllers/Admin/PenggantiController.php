<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TemplatePencatatan;
use App\Http\Controllers\Controller;
use App\Imports\PencatatanImport;
use App\Models\LogPergantian;
use App\Models\MasterKategori;
use App\Models\MasterKategoriAffiliasi;
use App\Models\Pencatatan;
use App\Models\Perusahaan;
use App\Models\SusunanPengurus;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;

class PenggantiController extends Controller
{
    
    public function index(Request $request){
        $data = LogPergantian::get();
        
        if ($request->ajax()) {
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-success rounded-pill btn-icon me-2 upload-record" data-id="'.$row->id.'"><i class="mdi mdi-cloud-upload mdi-20px"></i></button>';
                $btn = $btn.'<a href="'.route('pengganti.pdf', $row->id).'" class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2"><i class="mdi mdi-cloud-print-outline mdi-20px"></i></a>';
                $btn = $btn.'<a href="'.route('pencatatan.details', $row->pencatatan_id).'" class="btn btn-sm btn-icon btn-text-info rounded-pill btn-icon me-2 details-pencatatan"><i class="mdi mdi-view-carousel mdi-20px"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.pergantian.index');
    }
    
    
    public function create(Request $request, $id){
        $bentukSerikat = MasterKategori::get();
        
        $data = Pencatatan::where('id', $id)->first();
        
        return view('admin.pergantian.create', [
            'kategori' => $bentukSerikat,
            'data' => $data
        ]);
    }
    
    public function show(Request $request){
        $pencatatan = Pencatatan::where('status', 1)->get();
        
        if ($request->ajax()) {
            return Datatables::of(json_decode($pencatatan))
            ->addIndexColumn()
            ->addColumn('action', function($row){
                if ($row->status == '1') {
                    $btn = '<a href="'.route('pengganti.create', $row->id).'" class="btn btn-sm btn-success rounded-pill btn-icon me-2"><i class="mdi mdi-plus mdi-10px"></i></a>';
                } else {
                    $btn = '';
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        
    }
    
    public function store(Request $request){
        $data = new LogPergantian;
        $data->pencatatan_id = $request->pencatatan_id;
        $data->bentuk_serikat = $request->bentuk_serikat;
        $data->nama_serikat = $request->nama_serikat;
        $data->nomor_pencatatan = $request->nomor_pencatatan;
        $data->no_surat = $request->no_surat;
        $data->tgl_surat = $request->tgl_surat;
        $data->no_surat_permohonan = $request->no_surat_permohonan;
        $data->tgl_surat_permohonan = $request->tgl_surat_permohonan;
        $data->perihal = $request->perihal;
        $data->surat_ket_hilang = $request->surat_kehilangan_dari;
        $data->no_surat_ket_hilang = $request->no_surat_ket_hilang;
        $data->keterangan_hilang = $request->keterangan_hilang;
        $data->pilih_ttd = $request->pilih_ttd;
        $data->nama_ttd = $request->nama_ttd;
        $data->nip_ttd = $request->nip_ttd;
        $data->tgl_kehilangan = $request->tgl_kehilangan;
        $data->pindah_ke = $request->pindah_ke;
        
        if (!empty($request->dokument_kehilangan) && $request->dokument_kehilangan != 'null') {
            $file = $request->file('dokument_kehilangan');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/dokument', $filename);
            
            $file_path = "storage/app/public/dokument/".$filename;
            $file_serverpath = asset($file_path);
            $data->dokument_kehilangan = $file_serverpath;
        }
        
        $data->created_by = auth()->user()->full_name;
        $data->save();
        
        return redirect()->route('pengganti.index');
    }
    
    public function upload(Request $request){
        $data = LogPergantian::where('id', $request->pengganti_id)->first();
        if (!empty($request->dokument) && $request->dokument != 'null') {
            $file = $request->file('dokument');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/dokument', $filename);
            
            $file_path = "storage/app/public/dokument/".$filename;
            $file_serverpath = asset($file_path);
            $data->dokument_url = $file_serverpath;
            $data->dokument_nama = $filename;
        }
        
        $data->save();
        return redirect()->route('pengganti.index');
    }
    
    public function pdf($id){
        $log = LogPergantian::where('id', $id)->first();
        // return $log;
        // die();
        $data = Pencatatan::where('id', $log->pencatatan_id)->first();
        
        $result = [
            'tgl_pencatatan' => Carbon::parse($data->tgl_pencatatan)->format('d F Y'),
            'nama_serikat' => $data->nama_serikat,
            'alamat' => $data->alamat,
            'kecamatan' => $data->kecamatan,
            'kelurahan' => $data->kelurahan,
            'wilayah' => $data->wilayah,
            'nomor_pencatatan' => $data->nomor_pencatatan,
            'jenis_serikat' => $data->jenis_serikat,
            'status_serikat' => $data->status_serikat,
            'pilih_ttd' => $data->nama_ttd,
            'nama_ttd' => $data->nama_ttd,
            'nip_ttd' => $data->nip_ttd,
        ];
        
        $pdf = Pdf::loadView('admin.pergantian.pdf', [
            'result' => $result,
            'log' => $log
        ]);
        return $pdf->stream('pengganti.pdf');
    }
}
