<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MasterKategori;
use App\Models\MasterKategoriAffiliasi;
use Illuminate\Http\Request;

use DataTables;

class KategoriAffiliasiController extends Controller
{
    public function index(Request $request)
    {
        $data = MasterKategoriAffiliasi::get();
            
            if ($request->ajax()) {
                return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2 edit-kategori" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                    $btn = $btn.'<button class="btn btn-sm btn-icon btn-text-danger rounded-pill btn-icon me-2 delete-record" data-id="'.$row->id.'"><i class="mdi mdi-delete-outline mdi-20px"></i></button>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
            }
            
            $kategori = MasterKategori::orderBy('id', 'asc')->get();
            
            return view('admin.master.kategori-affiliasi.index', [
                'kategori' => $kategori
            ]);
        }
        
        public function getData(Request $request){
            if ($request->afiliasi) {
                $data = MasterKategoriAffiliasi::where('id', $request->afiliasi)
                ->orderBy('id', 'desc')
                ->get();
            }else{
                $data = MasterKategoriAffiliasi::orderBy('id', 'desc')->get();
            }

            $result = $data->map(function ($dt) {
                return '<option value="'.$dt->nama.'">'.$dt->nama.'</option>';
            })
            ->values();

            return response()->json([
                'data' => $data,
                'option' => $result,
                'type' => 'success',
                'message' => 'get data berhasil',
            ], 200);
        }
        
        
        public function store(Request $request){
            $data = new MasterKategoriAffiliasi;
            $data->kategori_id = $request->kategori;
            $data->nama = $request->nama;
            $data->status = $request->status;
            $data->keterangan = $request->keterangan;
            $data->save();
            
            return redirect()->route('master-kategori-affiliasi.index');
        }
        
        public function edit(Request $request, $id){
            $data = MasterKategoriAffiliasi::where('id', $id)->first();
            
            return view('admin.master.kategori-affiliasi.edit', [
                'data' => $data
            ]);
        }
        
        public function update(Request $request, $id){
            $data = MasterKategoriAffiliasi::find($id);
            $data->kategori_id = $request->kategori;
            $data->nama = $request->nama;
            $data->status = $request->status;
            $data->keterangan = $request->keterangan;
            $data->save();
            
            return redirect()->route('master-kategori-affiliasi.index');
        }
        
        public function destroy($id){
            $data = MasterKategoriAffiliasi::find($id);
            $data->delete();
            
            return response()->json([
                'type' => 'success',
                'message' => 'Deleted success'
            ]);
        }
        
    }
    