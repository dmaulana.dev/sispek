<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TemplatePencatatan;
use App\Http\Controllers\Controller;
use App\Imports\PencatatanImport;
use App\Models\MasterKategori;
use App\Models\MasterKategoriAffiliasi;
use App\Models\Pencatatan;
use App\Models\PenolakanPencatatan;
use App\Models\Perusahaan;
use App\Models\SusunanPengurus;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;

class PenolakanController extends Controller
{
    
    public function index(Request $request){
        $data = PenolakanPencatatan::get();
        
        if ($request->ajax()) {
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-success rounded-pill btn-icon me-2 upload-record" data-id="'.$row->id.'"><i class="mdi mdi-cloud-upload mdi-20px"></i></button>';
                // $btn = $btn.'<button class="btn btn-sm btn-icon btn-text-danger rounded-pill btn-icon me-2 delete-record" data-id="'.$row->id.'"><i class="mdi mdi-delete-outline mdi-20px"></i></button>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.penolakan.index');
    }
    
    
    public function show(Request $request){
        $pencatatan = Pencatatan::where('status', 1)->get();
        
        if ($request->ajax()) {
            return Datatables::of(json_decode($pencatatan))
            ->addIndexColumn()
            ->addColumn('action', function($row){
                if ($row->status == '1') {
                    $btn = '<a href="'.route('pencatatan.penolakan.create', $row->id).'" class="btn btn-sm btn-success rounded-pill btn-icon me-2"><i class="mdi mdi-plus mdi-10px"></i></a>';
                } else {
                    $btn = '';
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        
    }
    
    
    public function create(Request $request){
        return view('admin.penolakan.create');
    }
    
    public function store(Request $request){
        
        // simpan ke log penolakan
        // cek dulu
        $tolakLast = PenolakanPencatatan::orderBy('id', 'desc')->first();
        if (!empty($tolakLast)) {
            $noUrut = $tolakLast->nomor_urut + 1;
            $noPermohonan = $tolakLast->nomor_urut + 1;
        }else{
            $noUrut = 1;
            $noPermohonan = 1;
        }
        $penolakan = new PenolakanPencatatan;
        $penolakan->nomor_urut = $noUrut;
        $penolakan->no_permohonan = '00'.$noPermohonan;
        $penolakan->no_surat = $request->no_surat;
        $penolakan->tgl_surat = $request->tgl_surat;
        $penolakan->bentuk_serikat = $request->bentuk_serikat;
        $penolakan->nama_serikat = $request->nama_serikat;
        $penolakan->alasan_penolakan = $request->alasan_penolakan;
        $penolakan->tgl_penolakan = Carbon::now()->format('Y-m-d');
        $penolakan->created_by = auth()->user()->full_name;
        $penolakan->save();
        
        return redirect()->route('pencatatan.penolakan.index');
    }
    
    public function upload(Request $request){
        $data = PenolakanPencatatan::where('id', $request->penolakan_id)->first();
        if (!empty($request->dokument) && $request->dokument != 'null') {
            $file = $request->file('dokument');
            $filename = rand(0, 99).time(). '_' . $file->getClientOriginalName();
            $file->storeAs('public/dokument', $filename);
            
            $file_path = "storage/app/public/dokument/".$filename;
            $file_serverpath = asset($file_path);
            $data->dokument_url = $file_serverpath;
            $data->dokument_nama = $filename;
        }
        
        $data->save();
        return redirect()->route('pencatatan.penolakan.index');
    }
    
}

