<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterKategoriAffiliasi extends Model
{
    // use HasFactory;
    protected $table = 'master_kategori_affiliasi';
}
