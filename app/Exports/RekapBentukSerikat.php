<?php

namespace App\Exports;

use App\Models\MasterKategori;
use App\Models\Pencatatan;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;



class RekapBentukSerikat implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    
    use Exportable, SerializesModels;
    
    public $request;
    
    function __construct($request)
    {
        $this->request = $request;
    }
    
    
    public function view(): View
    {
        $request = $this->request;
        //
        $bentukSerikat = MasterKategori::get();
        $firstTgl = Pencatatan::orderBy('tgl_pencatatan', 'asc')->first();
        
        $from = date($firstTgl->tgl_pencatatan);
        $to = date('2023-12-31');
        
        $data = $bentukSerikat->map(function ($query) use ($from, $to) {
            $pencatatan = Pencatatan::where('kategori_id', $query->id)->get();
            return $pencatatan->groupBy('bentuk_serikat')->map(function ($qp) use ($from, $to){
                $tahun_2023 = $qp->whereBetween('tgl_pencatatan', [$from, $to])->count();
                $tahun_2024 = $qp->whereBetween('tgl_pencatatan', ['2024-01-01', Carbon::now()->format('Y-m-d')])->count();
                $afiliasi_aktif = $qp->where('status_serikat', 'Afiliasi')->where('visible', 1)->count();
                $mandiri_aktif = $qp->where('status_serikat', 'Mandiri')->where('visible', 1)->count();
                $afiliasi_nonaktif = $qp->where('status_serikat', 'Afiliasi')->where('visible', 0)->count();
                $mandiri_nonaktif = $qp->where('status_serikat', 'Mandiri')->where('visible', 0)->count();
                return [
                    'bentuk_serikat' => $qp->first()->bentuk_serikat,
                    'tahun_2023' => $tahun_2023,
                    'tahun_2024' => $tahun_2024,
                    'jumlah' => $tahun_2023 + $tahun_2024,
                    'jenis_serikat_diperusaaan' => $qp
                    ->where('jenis_serikat', 'Serikat Pekerja di Perusahaan')
                    ->groupBy('jenis_serikat')
                    ->map(function ($jns){
                        $afiliasi = $jns->where('status_serikat', 'Afiliasi')->count();
                        $mandiri = $jns->where('status_serikat', 'Mandiri')->count();
                        return [
                            'afiliasi' => $afiliasi,
                            'mandiri' => $mandiri,
                            'total' => $mandiri + $afiliasi
                        ];
                        
                    })
                    ->values(),
                    'jenis_serikat_diluar_perusaaan' => $qp
                    ->where('jenis_serikat', 'Serikat Pekerja di Luar Perusahaan')
                    ->groupBy('jenis_serikat')
                    ->map(function ($jns){
                        $afiliasi = $jns->where('status_serikat', 'Afiliasi')->count();
                        $mandiri = $jns->where('status_serikat', 'Mandiri')->count();
                        return [
                            'afiliasi' => $afiliasi,
                            'mandiri' => $mandiri,
                            'total' => $mandiri + $afiliasi
                        ];
                        
                    })
                    ->values(),
                    'afiliasi_aktif' => $afiliasi_aktif,
                    'mandiri_aktif' => $mandiri_aktif,
                    'jumlah_aktif' => $afiliasi_aktif + $mandiri_aktif,
                    'afiliasi_nonaktif' => $afiliasi_nonaktif,
                    'mandiri_nonaktif' => $mandiri_nonaktif,
                    'jumlah_nonaktif' => $afiliasi_nonaktif + $mandiri_nonaktif,
                ];
            })
            ->values();
        })
        ->collapse()
        ->values();
        
        return view('admin.laporan.rekap-excell-bentuk-serikat', [
            'data' => $data
        ]);
    }
    
    
    
}
