<?php

namespace App\Exports;

use App\Models\Pencatatan;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Queue\SerializesModels;



class RekapAnggota implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    
    use Exportable, SerializesModels;
    
    public $request;
    
    function __construct($request)
    {
        $this->request = $request;
    }
    
    
    public function view(): View
    {
        $request = $this->request;
        //
        $firstTgl = Pencatatan::orderBy('tgl_pencatatan', 'asc')->first();
        
        $from = date($firstTgl->tgl_pencatatan);
        $to = date('2023-12-31');
        
        $data_rekap = Pencatatan::whereIn('bentuk_serikat', ['Federasi', 'Konfederasi'])->get();
        $data_rekapitulasi = $data_rekap->groupBy('bentuk_serikat')->map(function ($qp) use ($from, $to){
            return $qp->groupBy('afiliasi')->map(function ($af) use ($from, $to) {
                $tahun_2023 = $af->whereBetween('tgl_pencatatan', [$from, $to])->count();
                $tahun_2024 = $af->whereBetween('tgl_pencatatan', ['2024-01-01', Carbon::now()->format('Y-m-d')])->count();
                return [
                    'bentuk_serikat' => $af->first()->bentuk_serikat,
                    'afiliasi' => $af->first()->afiliasi === null ? 'Tidak ada afiliasi' : $af->first()->afiliasi,
                    'tahun_2023' => $tahun_2023,
                    'tahun_2024' => $tahun_2024,
                    'jumlah' => $tahun_2023 + $tahun_2024,
                    'jenis_serikat_diperusaaan' => $af
                    ->where('jenis_serikat', 'Serikat Pekerja di Perusahaan')
                    ->groupBy('jenis_serikat')
                    ->map(function ($jns){
                        $non_aktif = $jns->where('visible', 0)->count();
                        $aktif = $jns->where('visible', 1)->count();
                        return [
                            'aktif' => $aktif,
                            'non_aktif' => $non_aktif,
                        ];
                        
                    })
                    ->values(),
                    'jenis_serikat_diluar_perusaaan' => $af
                    ->where('jenis_serikat', 'Serikat Pekerja di Luar Perusahaan')
                    ->groupBy('jenis_serikat')
                    ->map(function ($jns){
                        $aktif = $jns->where('visible', 0)->count();
                        $non_aktif = $jns->where('visible', 1)->count();
                        return [
                            'aktif' => $aktif,
                            'non_aktif' => $non_aktif,
                        ];
                        
                    })
                    ->values(),
                ];
            })
            ->values();
        })
        ->collapse()
        ->values();
        
        return view('admin.laporan.rekap-excell-anggota', [
            'data_rekapitulasi' => $data_rekapitulasi
        ]);
        
    }
    
    
}
