<?php

namespace App\Exports;

use App\Models\Pencatatan;
use App\Models\Perusahaan;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Queue\SerializesModels;



class DaftarLaporan extends  DefaultValueBinder implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    
    use Exportable, SerializesModels;
    
    public $request;
    
    function __construct($request)
    {
        $this->request = $request;
    }
    
    public function headings(): array
    {
        return [
            'NO',
            'BENTUK_SERIKAT',
            'JENIS_SERIKAT',
            'NAMA_SERIKAT',
            'NAMA_SINGKATAN',
            'PERUSAHAAN',
            'ALAMAT',
            'STATUS_SERIKAT',
            'AFILIASI',
            'NOMOR BUKTI PENCATATAN',
            'TGL PENCATATAN',
            'PENGURUS',
            'STATUS',
            'VISIBLE',
            'KETERANGAN NON AKTIF', 
            'KETERANGAN',
            ''
        ];
        
    }
    
    
    public function collection()
    {
        $request = $this->request;
        //
        $index = Pencatatan::where(function ($where) use ($request) {
            
            if ($request->get('visible') == '0' || $request->get('visible') == '1') {
                $where->where('visible', $request->get('visible'));
            }
            if ($request->get('status') == '0' || $request->get('status') == '1' || $request->get('status') == '2' ) {
                $where->where('status', $request->get('status'));
            }
            if ($request->get('bentuk_serikat') == 'SP/SB' || $request->get('bentuk_serikat') == 'Federasi' || $request->get('bentuk_serikat') == 'Konfederasi' ) {
                $where->where('bentuk_serikat', $request->get('bentuk_serikat'));
            }
            if ($request->get('status_serikat') == 'Afiliasi' || $request->get('status_serikat') == 'Mandiri') {
                $where->where('status_serikat', $request->get('status_serikat'));
            }
        });
        
        $unmap = (clone $index)
        ->get();
        
        $query = $unmap->map(function ($item, $index) {
            $perusahaan = Perusahaan::where('pencatatan_id', $item->id)->get();
            
            if ($item->status == 0) {
                $status = 'Proses';
            }elseif ($item->status == 1) {
                $status = 'Selesai';
            }else{
                $status = 'Ditolak';
            }
            
            $alamat = $item->wilayah.' '.$item->kecatamatan.' '.$item->alamat.' '.$item->kelurahan;
            return [
                'no' => $index + 1,
                'bentuk_serikat' => $item->bentuk_serikat,
                'jenis_serikat' => $item->jenis_serikat,
                'nama_serikat' => $item->nama_serikat,
                'nama_singkat' => $item->nama_singkat,
                'perusahaan' => collect($perusahaan)->pluck('nama_perusahaan')->implode(', '),
                'alamat' => $alamat,
                'status_serikat' => $item->status_serikat,
                'afiliasi' => $item->afiliasi,
                'nomor_pencatatan' => $item->nomor_pencatatan,
                'tgl_pencatatan' => Carbon::parse($item->tgl_pencatatan)->format('d m Y'),
                'pengurus' => 'Ketua : '.$item->pengurus,
                'status' => $status,
                'visible' => $item->visible == 1 ? 'Aktif' : 'Non Aktif',
                'keterangan_nonaktif' => $item->keterangan_nonaktif,
                'keterangan' => $item->keterangan
            ];
        })
        ->values();
        return $query;
    }
    
    
}
