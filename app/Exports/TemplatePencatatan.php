<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class TemplatePencatatan implements FromCollection, WithHeadings, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $request;
    
    function __construct($request)
    {
        $this->request = $request;
    }
    
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A2:Z2')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('F2D7D9');
            },
        ];
    }
    
    public function headings(): array
    {
        return [
            'NO',
            'TGL_SURAT',
            'NO_SURAT',
            'BENTUK_SERIKAT',
            'JENIS_SERIKAT',
            'NAMA_SERIKAT',
            'NAMA_SINGKATAN',
            'NO_SURAT_PERMOHOANAN',
            'TGL_SURAT_PERMOHOANAN',
            'PERUSAHAAN',
            'WILAYAH',
            'KECAMATAN',
            'ALAMAT',
            'KELURAHAN',
            'STATUS_SERIKAT',
            'AFILIASI',
            'NOMOR BUKTI PENCATATAN',
            'TGL PENCATATAN',
            'PENGURUS',
            'VISIBLE',
            'KETERANGAN_NON_AKTIF', 
            'KATEGORI_TTD', 
            'NAMA_TTD', 
            'NIP_TTD',
            'KETERANGAN',
            ''
        ];
        
    }
    
    
    public function collection()
    {
        return collect([
            [
                'no' => '1',
                'tgl_surat' => '10-11-2022',
                'no_surat' => '001/xxx/xxx/xx',
                'bentuk_serikat' => 'Federasi',
                'jenis_serikat' => 'Serikat Pekerja di Perusahaan',
                'nama_serikat' => 'SP Provis Garuda Services',
                'nama_singkatan' => 'SP Provis Garuda Services',
                'no_surat_permohonan' => '001/xxx/xxx/xx',
                'tgl_surat_permohonan' => '10-11-2022',
                'perusahaan' => 'PT. Provis Garuda',
                'wilayah' => 'Jakarta Pusat',
                'kecamatan' => 'Gambir',
                'alamat' => 'Jl. Gunung Sahari Raya No. 52',
                'kelurahan' => 'Gambir',
                'status_serikat' => 'Affiliasi',
                'afiliasi' => 'Federasi Pertamina Bersatu',
                'nomor_bukti_pencatatan' => '594/I/P/XI/2012',
                'tgl_pencatatan' => '2022-11-22',
                'pengurus' => 'Julius K',
                'visible' => 'Aktif',
                'keterangan_non_aktif' => 'SP Pindah Domisili',
                'kategori_ttd' => 'PLT',
                'nama_ttd' => 'Sudrajad, S.E., M.M.',
                'nip_ttd' => '19809xxxx90',
                'keterangan' => 'Pindah ke Kota Tangerang',
                '' => '<- Contoh data jangan dihapus - isi di bawah contoh'
                ]
            ]);
        }
        
    }
    