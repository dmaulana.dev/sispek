<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogPenonaktifanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_penonaktifan', function (Blueprint $table) {
            $table->id();
            $table->integer('pencatatan_id');
            $table->integer('nomor_urut')->default(0);
            $table->text('nomor_pencatatan');
            $table->text('no_surat_permohonan')->nullable();
            $table->string('bentuk_serikat');
            $table->string('nama_serikat');
            $table->string('pindah_domisili')->nullable();
            $table->date('tanggal');
            $table->text('alasan');
            $table->text('keterangan');
            $table->text('status');
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_penonaktifan');
    }
}
