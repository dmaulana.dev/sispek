<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogPerubahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_perubahan', function (Blueprint $table) {
            $table->id();
            $table->integer('pencatatan_id');
            $table->integer('nomor_urut')->default(0);
            $table->string('bentuk_serikat');
            $table->string('jenis_serikat');
            $table->string('nama_serikat');
            $table->string('nama_singkat');
            $table->string('perusahaan')->nullable();
            $table->string('alamat');
            $table->string('status_serikat')->nullable();
            $table->string('afiliasi');
            $table->text('nomor_pencatatan')->nullable();
            $table->date('tgl_pencatatan');
            $table->string('pengurus')->nullable();
            $table->integer('perubahan_ke')->default(0);
            $table->string('perubahan_jenis')->nullable();
            $table->text('no_permohonan')->nullable();
            $table->date('tgl_perubahan');
            $table->text('logo_url')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perubahan');
    }
}
