<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDokumentPencatatanTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('dokument_pencatatan', function (Blueprint $table) {
            $table->id();
            $table->integer('pencatatan_id');
            $table->string('dokument_nama');
            $table->string('dokument_url');
            $table->timestamps();
        });
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('dokument_pencatatan');
    }
}
