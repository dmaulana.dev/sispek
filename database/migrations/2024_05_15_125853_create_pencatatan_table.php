<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePencatatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('pencatatan', function (Blueprint $table) {
            $table->id();
            $table->integer('nomor_urut')->default(0);
            $table->integer('kategori_id');
            $table->integer('affiliasi_id');
            $table->string('bentuk_serikat');
            $table->string('jenis_serikat');
            $table->string('nama_serikat');
            $table->string('nama_singkat');
            $table->string('perusahaan')->nullable();
            $table->string('alamat');
            $table->string('status_serikat')->nullable();
            $table->string('afiliasi');
            $table->text('nomor_pencatatan')->nullable();
            $table->date('tgl_pencatatan');
            $table->string('pengurus')->nullable();
            $table->integer('visible')->default(1);
            $table->string('keterangan');
            $table->string('keterangan_nonaktif')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pencatatan');
    }
}
