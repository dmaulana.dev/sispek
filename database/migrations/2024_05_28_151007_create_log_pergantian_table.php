<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogPergantianTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('log_pergantian', function (Blueprint $table) {
            $table->id();
            $table->integer('pencatatan_id');
            $table->integer('nomor_urut')->default(0);
            $table->text('no_permohonan')->nullable();
            $table->string('bentuk_serikat');
            $table->string('nama_serikat');
            $table->date('tgl_kehilangan');
            $table->string('surat_ket_hilang');
            $table->text('no_surat_ket_hilang');
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('log_pergantian');
    }
}
