<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogPenolakanTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('log_penolakan', function (Blueprint $table) {
            $table->id();
            $table->integer('pencatatan_id');
            $table->integer('nomor_urut')->default(0);
            $table->text('no_permohonan')->nullable();
            $table->string('bentuk_serikat');
            $table->string('nama_serikat');
            $table->date('tgl_penolakan');
            $table->text('alasan_penolakan');
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('log_penolakan');
    }
}
