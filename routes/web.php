<?php

use App\Http\Controllers\Admin\KategoriAffiliasiController;
use App\Http\Controllers\Admin\KategoriController;
use App\Http\Controllers\Admin\LaporanController;
use App\Http\Controllers\Admin\PenggantiController;
use App\Http\Controllers\Admin\PenolakanController;
use App\Http\Controllers\Admin\PenonaktifanController;
use App\Http\Controllers\Admin\PerubahanController;
use App\Http\Controllers\Admin\PerubahanNamaLambangController;
use App\Http\Controllers\Admin\WilayahDKIController;
use Illuminate\Support\Facades\Route;

//admin
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\ConfigurationController;
use App\Http\Controllers\Admin\MAdminController;
use App\Http\Controllers\Admin\MUserController;
use App\Http\Controllers\Admin\DashboardAdminController;
use App\Http\Controllers\Admin\PencatatanController;

// user controller
use App\Http\Controllers\User\DashboardUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|5
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// admin route
Route::get('/', [AuthController::class, 'index'])->name('auth.login');
Route::get('/login', [AuthController::class, 'index'])->name('auth.login');

Route::middleware(['has_login'])->group(function () {
    // Dashboard Admin
    Route::get('/dashboard-admin', [DashboardAdminController::class, 'index'])->name('dashboard.admin');
    
    // master Data
    Route::get('/master-admin', [MAdminController::class, 'index'])->name('master-admin.index');
    Route::get('/master-admin/get-data', [MAdminController::class, 'getDataAdmin'])->name('master-admin.get-data');
    Route::post('/master-admin/store', [MAdminController::class, 'store'])->name('master-admin.store');
    Route::get('/master-admin/details/{id}', [MAdminController::class, 'details'])->name('master-admin.details');
    Route::get('/master-admin/show/{id}', [MAdminController::class, 'show'])->name('master-admin.show');
    Route::post('/master-admin/update', [MAdminController::class, 'update'])->name('master-admin.update');
    Route::get('/master-admin/destroy/{id}', [MAdminController::class, 'destroy'])->name('master-admin.destroy');
    
    //user or pengguna
    Route::get('/dashboard-user', [DashboardUserController::class, 'index'])->name('dashboard.user');
    
    // master user
    Route::get('/master-user', [MUserController::class, 'index'])->name('master-user.index');
    Route::get('/master-user/get-data', [MUserController::class, 'getDataUser'])->name('master-user.get-data');
    Route::post('/master-user/store', [MUserController::class, 'store'])->name('master-user.store');
    Route::get('/master-user/show/{id}', [MUserController::class, 'show'])->name('master-user.show');
    Route::post('/master-user/update/{id}', [MUserController::class, 'update'])->name('master-user.update');
    Route::get('/master-user/destroy/{id}', [MUserController::class, 'destroy'])->name('master-user.destroy');
    
    // config
    Route::get('/configuration', [ConfigurationController::class, 'index'])->name('configuration.index');
    
    // Permission
    Route::get('/permission', [PermissionController::class, 'index'])->name('permission.index');
    Route::get('/permission/get-data', [PermissionController::class, 'getData'])->name('permission.get-data');
    Route::post('/permission/store', [PermissionController::class, 'store'])->name('permission.store');
    Route::get('/permission/show/{id}', [PermissionController::class, 'show'])->name('permission.show');
    Route::post('/permission/updated', [PermissionController::class, 'update'])->name('permission.update');
    Route::get('/permission/destroy/{id}', [PermissionController::class, 'destroy'])->name('permission.destroy');
    
    // role
    Route::get('/role', [RoleController::class, 'index'])->name('role.index');
    Route::get('/role/get-role', [RoleController::class, 'getData'])->name('role.get-data');
    Route::post('/role/store', [RoleController::class, 'store'])->name('role.store');
    Route::get('/role/show/{id}', [RoleController::class, 'show'])->name('role.show');
    Route::get('/role/show-edit/{id}', [RoleController::class, 'showedit'])->name('role.show-edit');
    Route::post('/role/update/{id}', [RoleController::class, 'update'])->name('role.update');
    Route::get('/role/destroy/{id}', [RoleController::class, 'destroy'])->name('role.destroy');
    
    
    // master kategori
    Route::get('/master-kategori', [KategoriController::class, 'index'])->name('master-kategori.index');
    Route::post('/master-kategori/store', [KategoriController::class, 'store'])->name('master-kategori.store');
    Route::get('/master-kategori/edit/{id}', [KategoriController::class, 'edit'])->name('master-kategori.edit');
    Route::post('/master-kategori/update/{id}', [KategoriController::class, 'update'])->name('master-kategori.update');
    Route::get('/master-kategori/destroy/{id}', [KategoriController::class, 'destroy'])->name('master-kategori.sp-sb.destroy');
    
    // master kategori affiliasi
    Route::get('/master-kategori-affiliasi', [KategoriAffiliasiController::class, 'index'])->name('master-kategori-affiliasi.index');
    Route::get('/master-kategori-affiliasi/get-data', [KategoriAffiliasiController::class, 'getData'])->name('master-kategori-affiliasi.get-data');
    Route::post('/master-kategori-affiliasi/store', [KategoriAffiliasiController::class, 'store'])->name('master-kategori-affiliasi.store');
    Route::get('/master-kategori-affiliasi/edit/{id}', [KategoriAffiliasiController::class, 'show'])->name('master-kategori-affiliasi.show');
    Route::post('/master-kategori-affiliasi/update', [KategoriAffiliasiController::class, 'update'])->name('master-kategori-affiliasi.update');
    Route::get('/master-kategori-affiliasi/destroy/{id}', [KategoriAffiliasiController::class, 'destroy'])->name('master-kategori-affiliasi.sp-sb.destroy');
    
    // pencatatan sp-sb
    Route::get('/pencatatan', [PencatatanController::class, 'index'])->name('pencatatan.index');
    Route::get('/pencatatan/create', [PencatatanController::class, 'create'])->name('pencatatan.create');
    Route::post('/pencatatan/get-data', [PencatatanController::class, 'getData'])->name('pencatatan.get-data');
    Route::post('/pencatatan/store', [PencatatanController::class, 'store'])->name('pencatatan.store');
    Route::get('/pencatatan/template', [PencatatanController::class, 'template'])->name('pencatatan.template');
    Route::post('/pencatatan/import', [PencatatanController::class, 'import'])->name('pencatatan.import');
    Route::get('/pencatatan/show/{id}', [PencatatanController::class, 'show'])->name('pencatatan.show');
    Route::get('/pencatatan/edit/{id}', [PencatatanController::class, 'edit'])->name('pencatatan.edit');
    Route::post('/pencatatan/updated/{id}', [PencatatanController::class, 'update'])->name('pencatatan.update');
    Route::get('/pencatatan/destroy/{id}', [PencatatanController::class, 'destroy'])->name('pencatatan.destroy');
    Route::get('/pencatatan/pdf/{id}', [PencatatanController::class, 'pdf'])->name('pencatatan.pdf');
    Route::post('/pencatatan/upload', [PencatatanController::class, 'upload'])->name('pencatatan.upload');
    Route::get('/pencatatan/details/{id}', [PencatatanController::class, 'details'])->name('pencatatan.details');
    
    // perubahan sp-sb
    Route::get('/perubahan', [PerubahanController::class, 'index'])->name('perubahan.index');
    Route::get('/perubahan/create/{id}', [PerubahanController::class, 'create'])->name('perubahan.create');
    Route::post('/perubahan/store', [PerubahanController::class, 'store'])->name('perubahan.store');
    Route::post('/simpan-pengurus', [PerubahanController::class, 'simpanPengurus'])->name('simpan-pengurus');
    Route::get('/perubahan/show', [PerubahanController::class, 'show'])->name('perubahan.show');
    Route::post('/perubahan/updated', [PerubahanController::class, 'update'])->name('perubahan.update');
    Route::post('/perubahan/upload', [PerubahanController::class, 'upload'])->name('perubahan.upload');
    Route::get('/perubahan/pdf/{id}', [PerubahanController::class, 'pdf'])->name('perubahan.pdf');
    Route::get('/hapus-pengurus/{id}', [PerubahanController::class, 'destroy'])->name('hapus-pengurus');
    
    
    // penolakan sp-sb
    Route::get('/pencatatan/penolakan', [PenolakanController::class, 'index'])->name('pencatatan.penolakan.index');
    Route::get('/pencatatan/penolakan/show', [PenolakanController::class, 'show'])->name('pencatatan.penolakan.show');
    Route::get('/pencatatan/penolakan/create', [PenolakanController::class, 'create'])->name('pencatatan.penolakan.create');
    Route::post('/pencatatan/penolakan/store', [PenolakanController::class, 'store'])->name('pencatatan.penolakan.store');
    Route::post('/pencatatan/penolakan/upload', [PenolakanController::class, 'upload'])->name('pencatatan.penolakan.upload');
    Route::get('/pencatatan/penolakan/destroy/{id}', [PenolakanController::class, 'destroy'])->name('pencatatan.penolakan.sp-sb.destroy');
    
    
    // penonaktifan sp-sb
    Route::get('/penonaktifan', [PenonaktifanController::class, 'index'])->name('penonaktifan.index');
    Route::get('/pencatatan/penonaktifan/show', [PenonaktifanController::class, 'show'])->name('pencatatan.penonaktifan.show');
    Route::get('/penonaktifan/create/{id}', [PenonaktifanController::class, 'create'])->name('penonaktifan.create');
    Route::post('/penonaktifan/store', [PenonaktifanController::class, 'store'])->name('penonaktifan.store');
    Route::post('/penonaktifan/upload', [PenonaktifanController::class, 'upload'])->name('penonaktifan.upload');
    Route::get('/penonaktifan/pdf/{id}', [PenonaktifanController::class, 'pdf'])->name('penonaktifan.pdf');
    
    
    // pengganti
    Route::get('/pengganti', [PenggantiController::class, 'index'])->name('pengganti.index');
    Route::post('/pengganti/store', [PenggantiController::class, 'store'])->name('pengganti.store');
    Route::get('/pengganti/show', [PenggantiController::class, 'show'])->name('pengganti.show');
    Route::get('/pengganti/create/{id}', [PenggantiController::class, 'create'])->name('pengganti.create');
    Route::post('/pengganti/upload', [PenggantiController::class, 'upload'])->name('pengganti.upload');
    Route::get('/pengganti/pdf/{id}', [PenggantiController::class, 'pdf'])->name('pengganti.pdf');
    
    
    
    Route::get('/wilayah/kabupaten', [WilayahDKIController::class, 'kabupaten'])->name('wilayah.kabupaten');
    Route::get('/wilayah/kecamatan', [WilayahDKIController::class, 'kecamatan'])->name('wilayah.kecamatan');
    
    
    // daftar
    Route::get('/daftar/laporan', [LaporanController::class, 'index'])->name('daftar.laporan.index');
    Route::get('/daftar/laporan/export', [LaporanController::class, 'export'])->name('daftar.laporan.export');
    
    // rekap laporan
    Route::get('/rekap/laporan', [LaporanController::class, 'rekapIndex'])->name('rekap.laporan.rekap_index');
    Route::post('/rekap/laporan/export', [LaporanController::class, 'rekapExport'])->name('rekap.laporan.rekap_export');
    Route::get('/rekap/laporan/bentuk-serikat', [LaporanController::class, 'rekapBentukSerikatPdf'])->name('rekap.laporan.bentuk-serikat.pdf');
    Route::get('/rekap/laporan/anggota', [LaporanController::class, 'rekapAnggotaPdf'])->name('rekap.laporan.anggota.pdf');
    Route::get('/rekap/laporan/bentuk-serikat/excell', [LaporanController::class, 'rekapBentukSerikatExcell'])->name('rekap.laporan.bentuk-serikat.excell');
    Route::get('/rekap/laporan/anggota/excell', [LaporanController::class, 'rekapAnggotaExcell'])->name('rekap.laporan.anggota.excell');
    
    
    
    // Logout
    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
});

Route::get('/404', function(){
    abort(404);
})->name('not_found');

