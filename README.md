How to Run This Project


```bash
    Using Native Composer
    1. run `composer install`
    2. Copy .env.example to .env
    4. run `php artisan key:generate`

    5. if the project database is empty then run
    `php artisan migrate and php artisan db:seed`

    6. to run with artisan `php -S localhost:8000`
```